<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Sites;
use App\Audit;
use App\Models\Core\Language;
use App\Models\Core\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class SitesController extends Controller
{

    public function __construct(Sites $sites)
    {
        $setting = new Setting();
        $this->Setting = $setting;
        $this->Sites = $sites;
    }

    public function index() 
    {
      $title = ['pageTitle' => 'Sites'];

      $data['sites'] = $this->Sites->paginate(15);
      $data['commonContent'] = $this->Setting->commonContent();
      return view('admin.sites.index', $title)->with('result',$data);
    }

    public function show(Request $request) 
    {
      $title = ['pageTitle' => 'Sites'];

      $data['site'] = $this->Sites->find($request->id);
      $data['audits'] = Audit::paginate(15);
      $data['commonContent'] = $this->Setting->commonContent();

      return view('admin.sites.show', $title)->with('result',$data);
    }

}
