<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditStat extends Model
{
    protected $fillable = [
        'audit_id', 'audit_site_id', 'user_id', 'audit_date', 'total_valid', 'total_failed', 'total_points', 'total_percentage' 
    ];

    
}
