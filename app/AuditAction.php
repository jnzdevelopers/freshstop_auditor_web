<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditAction extends Model
{
    protected $fillable = [
         'site_audit_id', 'question_id', 'action', 'action_date', 'user_id', 'site_id'
    ];
    
}
