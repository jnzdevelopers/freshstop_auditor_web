@extends('admin.layout')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> {{ trans('labels.admins') }} <small>{{ trans('labels.admins') }}...</small> </h1>
    <ol class="breadcrumb">
      <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
      <li class="active">{{ trans('labels.admins') }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->

    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ trans('labels.admins') }} </h3>
            <div class="box-tools pull-right">
              <a href="{{ URL::to('admin/addadmins')}}" type="button" class="btn btn-block btn-primary">{{ trans('labels.addadmins') }}</a>
            </div>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">

          @if (count($errors) > 0)
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{$errors->first()}}
            </div>
            @endif
          @endif
          
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{ session()->get('success') }}
            </div>
            @endif
          
              </div>

            </div>
            <div class="row">
              <div class="col-xs-12">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Audit</th>                
                      <th>Site</th>                
                      <th>Pdf</th>                
                      <th>Status</th>                
                      <th>Created</th>                
                      <th>Updated</th>                
                      <th>Action</th>                
                      <!-- <th>Deleted</th> -->
                    </tr>
                  </thead>
                  <tbody>
              @if (count($result['audits']) > 0)
                @foreach ($result['audits']  as $audit)
                  <tr>
                  <td>{{ $audit->heading }}</td>
                  <td>{{ $audit->site_name }}</td>
                  <td>
                      @if($audit->document && $audit->status == 1) 
                      <a href="{{ URL::to($audit->document) }}" target="_blank" class="label label-info"><i class="fa fa-file"></i> View Report</a>
                      @elseif(!$audit->document && $audit->status == 1)
                      <a href="{{ URL::to('admin/admin/sendAuditReport/'. $audit->id) }}" class="label label-danger">Generate & Mail Report</a>
                      @else
                      <label class="label label-warning">Pending</label>
                      @endif
                  </td>
                  <td>{{ $audit->created_at }}</td>
                  <td>{{ $audit->updated_at }}</td>
                    <td>
                      @if($audit->status == 0)
                        <strong class="badge bg-yellow">Started</strong>
                      @elseif($audit->status == 1)
                      <strong class="badge bg-green">Completed</strong>
                      @elseif($audit->status == 2)
                      <strong class="badge bg-blue">Saved</strong>
                      @endif

                    </td>
                      <td>
                        <ul class="nav table-nav">
                          <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ URL::to('admin/admin/audit_log/' . $audit->id) }}">Log</a></li> -->
                          <li role="presentation"><a role="menuitem" tabindex="-1" onclick="view_log('{{ $audit->id }}')" class="label label-default"><i class="fa fa-file"></i> Log</a></li>
                        </ul>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                </tr>
              @endif
              </tbody>
            </table>
            {{--  @if (count($result['audits']) > 0)
              <div class="col-xs-12 text-right">
                {{$result['audits']->links()}}
              </div>
              @endif
              --}}
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->

    <!-- deleteCustomerModal -->
  <div class="modal fade" id="deleteCustomerModal" tabindex="-1" role="dialog" aria-labelledby="deleteCustomerModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="deleteCustomerModalLabel">{{ trans('labels.deleteAdmin') }}</h4>
          </div>
        
        </div>
      </div>
    </div>
  </div>

    <!-- Main row -->

    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<div class="modal" id="myModal" aria-hidden="true"></div>
<script>
  function view_log(id){
      $('#myModal').load('/admin/admin/audit_log/'+id, function(response) {
        $('#myModal').modal( {backdrop:'static'} );
      });
  }
</script>

@endsection
