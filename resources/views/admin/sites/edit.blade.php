@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1> {{ trans('labels.Sites') }} <small>{{ trans('labels.ListingAllSites') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Sites') }}</li>
        </ol>
    </section>

  
    <section class="content">
         <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="container-fluid">
                            <div class="row">


                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (count($errors) > 0)
                                  @if($errors->any())
                                  <div class="alert alert-success alert-dismissible" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      {{$errors->first()}}
                                  </div>
                                  @endif
                                @endif
                            </div>
                        </div>
                        <div class="box-body">
                        {!! Form::open(array('url' =>'admin/sites/update/'.$result['site']->id, 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                          @method('PUT')
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Site Name</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('site_name',  $result['site']->site_name, array('class'=>'form-control field-validate', 'id'=>'customers_firstname')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">BC Abbr</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('bc',  $result['site']->bc, array('class'=>'form-control field-validate', 'id'=>'bc')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">BC Name</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('bc_name',  $result['site']->bc_name, array('class'=>'form-control field-validate', 'id'=>'bc_name')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">BC Email</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('bc_email', $result['site']->bc_email, array('class'=>'form-control field-validate', 'id'=>'bc_email')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Retailer</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('retailer',  $result['site']->retailer, array('class'=>'form-control field-validate', 'id'=>'retailer')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Area</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('area',  $result['site']->area, array('class'=>'form-control field-validate', 'id'=>'area')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Address</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('address',  $result['site']->address, array('class'=>'form-control field-validate', 'id'=>'address')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Landline</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('landline',  $result['site']->landline, array('class'=>'form-control field-validate', 'id'=>'landline')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Cell No</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('cell_no',  $result['site']->cell_no, array('class'=>'form-control field-validate', 'id'=>'cell_no')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Email</label>
                                <div class="col-sm-10 col-md-6">
                                {!! Form::text('email',  $result['site']->email, array('class'=>'form-control field-validate', 'id'=>'email')) !!}
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-md-3 control-label">Status</label>
                                <div class="col-sm-10 col-md-6">
                                <select class="form-control" name="status">
                                    <option value="1" {{ $result['site']->status == 1 ? 'selected':'' }}>{{ trans('labels.Active') }}</option>
                                    <option value="0" {{ $result['site']->status == 0 ? 'selected':'' }}>{{ trans('labels.Inactive') }}</option>
                                </select>
                                </div>
                            </div>

                            <div class="box-footer text-center">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    
                    
                </div>
            
            </div>
            
        </div>

    </section>
    
</div>

@endsection
