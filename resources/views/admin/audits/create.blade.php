@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Audits') }} <small>{{ trans('labels.ListingAllAudits') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Audits') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
              <div class="box">

              </div>

              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        @if (count($errors) > 0)
                          @if($errors->any())
                          <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{$errors->first()}}
                          </div>
                          @endif
                        @endif
                    </div>
                </div>
                
                {!! Form::open(array('url' => 'admin/audits/generate', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                <div class="row">
                  <div class="col-sm-5">
                    <label>Audit Heading: </label><input type="text" class="form-control" name="audit_heading" />
                  </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <div class="col-sm-5">
                          Question
                        </div>
                        <div class="col-sm-2">
                          Data Type
                        </div>
                        <div class="col-sm-3">
                          Options (comma "," seperated)
                        </div>
                        <div class="col-sm-2">
                          Required
                        </div>
                      </div>
                <?php for($i = 1; $i <= 20; $i++){
                //	if(isset($questions[$i-1]->id)){
                ?>
                <div class="form-group row">
                    <div class="col-sm-5">
                      <?php if(!empty($questions)){ ?>
                        <input type="hidden" name="id_<?php echo $i;?>" value="<?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->id; } ?>" />
                        <input type="hidden" name="number_<?php echo $i;?>" value="<?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->question_number; } ?>" />
                                  <?php } ?>
                      <textarea name="question_<?php echo $i;?>" class="form-control"><?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->question_question; } ?></textarea>
                              </div>
                              <div class="col-sm-2">
                                  <select name="type_<?php echo $i;?>" class="form-control">
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'short text'){  echo 'selected'; }} ?>>Short Text</option>
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'long text'){  echo 'selected'; }} ?>>Long Text</option>
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'radio'){  echo 'selected'; }} ?>>Radio</option>
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'checkbox'){  echo 'selected'; }} ?>>checkbox</option>
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'select box'){  echo 'selected'; }} ?>>select box</option>
                                  </select>
                              </div>
                              <div class="col-sm-3">
                                  <input name="options_<?php echo $i;?>" class="form-control" value="<?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->question_options; } ?>" />
                              </div>
                              <div class="col-sm-2">
                                  <select name="required_<?php echo $i;?>" class="form-control">
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_required) == 'no'){  echo 'selected'; }} ?>>no</option>
                                      <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_required) == 'yes'){  echo 'selected'; }} ?>>yes</option>
                                  </select>
                              </div>
                          </div>
                          <?php } //} ?>
                        
                    </div>
                    <?php if(!empty($questions)){ ?> 
                    <button type="submit" value="submit" name="submit" class="btn btn-sm btn-primary"></i> <?= 'Update'; ?></button>
                    <?php }else{ ?>
                                <button type="submit" value="submit" name="submit" class="btn btn-sm btn-primary"></i> <?= 'Submit'; ?></button>
                    <?php } ?>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </section>
</div>
@endsection