<div style="width: 100%; display:block;">
 
  <h2>Audit Report</h2>
  
  <p>
    Hi! A new Audit Report has been completed.  Please find attached report.
  </p>

  <table width="450px" cellpadding="10px">
    <tr>
      <td width="450px; ">
      <div style="background-color: #EFF7EF; width: 450px">
        <h2>Details:</h2>
        <p><strong>Audit</strong>: {{ $data['audit']->heading }}</p>
        <p><strong>Site</strong>: {{ $data['site']->site_name }}</p>
        <p><strong>Date</strong>: {{ $data['audit']->created_at->todatestring() }}</p>
        <p><strong>BC</strong>: {{ $data['user']->first_name }} </p>
        <p><strong>Opts Manager</strong>: {{ $data['user']->opts_manager }}</p>
      </div>
    </td>
    </tr>
  </table>
  
</div>