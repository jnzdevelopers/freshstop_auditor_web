<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('audit_id');
            $table->string('question');
            $table->string('question_type');
            $table->string('question_options')->nullable();
            $table->string('question_required')->nullable();
            $table->string('question_number')->nullable();
            $table->string('question_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_questions');
    }
}
