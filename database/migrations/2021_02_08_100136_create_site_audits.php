<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteAudits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('audit_id');
            $table->bigInteger('site_id');
            $table->bigInteger('user_id');
            $table->tinyInteger('status')->comment('0 = not started, 1 = completed, 2 = saved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_sites');
    }
}
