-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2021 at 08:52 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freshstop_audit`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_book`
--

CREATE TABLE `address_book` (
  `address_book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `entry_gender` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_id` int(11) DEFAULT NULL,
  `entry_company` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_firstname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `entry_lastname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `entry_street_address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `entry_suburb` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_postcode` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `entry_city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `entry_state` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  `entry_latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alert_settings`
--

CREATE TABLE `alert_settings` (
  `alert_id` int(11) NOT NULL,
  `create_customer_email` tinyint(1) NOT NULL DEFAULT '0',
  `create_customer_notification` tinyint(1) NOT NULL DEFAULT '0',
  `order_status_email` tinyint(1) NOT NULL DEFAULT '0',
  `order_status_notification` tinyint(1) NOT NULL DEFAULT '0',
  `new_product_email` tinyint(1) NOT NULL DEFAULT '0',
  `new_product_notification` tinyint(1) NOT NULL DEFAULT '0',
  `forgot_email` tinyint(1) NOT NULL DEFAULT '0',
  `forgot_notification` tinyint(1) NOT NULL DEFAULT '0',
  `news_email` tinyint(1) NOT NULL DEFAULT '0',
  `news_notification` tinyint(1) NOT NULL DEFAULT '0',
  `contact_us_email` tinyint(1) NOT NULL DEFAULT '0',
  `contact_us_notification` tinyint(1) NOT NULL DEFAULT '0',
  `order_email` tinyint(1) NOT NULL DEFAULT '0',
  `order_notification` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `alert_settings`
--

INSERT INTO `alert_settings` (`alert_id`, `create_customer_email`, `create_customer_notification`, `order_status_email`, `order_status_notification`, `new_product_email`, `new_product_notification`, `forgot_email`, `forgot_notification`, `news_email`, `news_notification`, `contact_us_email`, `contact_us_notification`, `order_email`, `order_notification`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `api_calls_list`
--

CREATE TABLE `api_calls_list` (
  `id` int(11) NOT NULL,
  `nonce` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `device_id` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE `audits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `heading` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `heading`, `description`, `created_at`, `updated_at`) VALUES
(5, 'weekday audit', 'Weekday audit to be done', '2020-11-17 09:11:31', '2020-11-17 09:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `audit_answers`
--

CREATE TABLE `audit_answers` (
  `id` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `value` text,
  `site_audit_id` int(11) NOT NULL,
  `audit_id` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `site_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_answers`
--

INSERT INTO `audit_answers` (`id`, `field`, `value`, `site_audit_id`, `audit_id`, `question_id`, `user_id`, `site_id`, `created_at`, `updated_at`) VALUES
(1, '', NULL, 0, 5, 25, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(2, '', '1', 0, 5, 26, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(3, '', '1', 0, 5, 27, 1, 0, '2020-11-19 09:24:56', '2020-11-19 15:03:17'),
(4, '', '1', 0, 5, 28, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(5, '', '1', 0, 5, 29, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(6, '', NULL, 0, 5, 30, 1, 0, '2020-11-19 09:24:56', '2020-11-19 15:04:00'),
(7, '', NULL, 0, 5, 31, 1, 0, '2020-11-19 09:24:56', '2020-11-19 15:03:57'),
(8, '', NULL, 0, 5, 32, 1, 0, '2020-11-19 09:24:56', '2020-11-19 15:03:54'),
(9, '', NULL, 0, 5, 33, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(10, '', NULL, 0, 5, 34, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(11, '', '1', 0, 5, 35, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(12, '', NULL, 0, 5, 36, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(13, '', NULL, 0, 5, 37, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(14, '', NULL, 0, 5, 38, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(15, '', '1', 0, 5, 39, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(16, '', '1', 0, 5, 40, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(17, '', '1', 0, 5, 41, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:27:14'),
(18, '', NULL, 0, 5, 42, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(19, '', NULL, 0, 5, 43, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(20, '', NULL, 0, 5, 44, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(21, '', NULL, 0, 5, 45, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(22, '', NULL, 0, 5, 46, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(23, '', NULL, 0, 5, 47, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(24, '', NULL, 0, 5, 48, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(25, '', NULL, 0, 5, 49, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(26, '', NULL, 0, 5, 50, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(27, '', NULL, 0, 5, 51, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(28, '', NULL, 0, 5, 52, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(29, '', NULL, 0, 5, 53, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(30, '', NULL, 0, 5, 54, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(31, '', NULL, 0, 5, 55, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(32, '', NULL, 0, 5, 56, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(33, '', NULL, 0, 5, 57, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(34, '', NULL, 0, 5, 58, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(35, '', NULL, 0, 5, 59, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(36, '', NULL, 0, 5, 60, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(37, '', NULL, 0, 5, 61, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(38, '', NULL, 0, 5, 62, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(39, '', NULL, 0, 5, 63, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(40, '', NULL, 0, 5, 64, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(41, '', NULL, 0, 5, 65, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(42, '', NULL, 0, 5, 66, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(43, '', NULL, 0, 5, 67, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(44, '', NULL, 0, 5, 68, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(45, '', NULL, 0, 5, 69, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(46, '', NULL, 0, 5, 70, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(47, '', NULL, 0, 5, 71, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(48, '', NULL, 0, 5, 72, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(49, '', NULL, 0, 5, 73, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(50, '', NULL, 0, 5, 74, 1, 0, '2020-11-19 09:24:56', '2020-11-19 09:24:56'),
(51, '', NULL, 0, 5, 75, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(52, '', NULL, 0, 5, 76, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(53, '', NULL, 0, 5, 77, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(54, '', NULL, 0, 5, 78, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(55, '', NULL, 0, 5, 79, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(56, '', NULL, 0, 5, 80, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(57, '', NULL, 0, 5, 81, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(58, '', NULL, 0, 5, 82, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(59, '', NULL, 0, 5, 83, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(60, '', NULL, 0, 5, 84, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:24:57'),
(61, '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAB9CAYAAAAY2F6TAAAMiElEQVR4Xu2de6h1RRnGHwsiEkmNyuxuRFiSYpJdsUiiorCiojCokAwzTcWShLDonwrsYnTFKLELlZmFhkVQkVFQgkL6h91vWmZmWRYYGU/N1Hzj2mfv2XutmT1r/wYO53z7zJr3nd+7zvPNvGtm1n6iQAACEOiEwH6d+ImbEIAABIRgcRNAAALdEECwugkVjkIAAggW9wAEINANAQSrm1DhKAQggGBxD0AAAt0QQLC6CRWOQgACCBb3AAQg0A0BBKubUOEoBCCAYHEPQAAC3RBAsLoJVVeOHi/psPD1QEmPkHS9pGsl/UzSjeHfXXUKZ9sTQLDax6B3D14k6XRJR0k6sKAzt0n6q6SfBPH6g6Rvha+CZqi6SwQQrF2K9nh9PVHSsZJOG6/JfVp6i6RLJd0wUfs02ykBBKvTwDV0+4OSXl/B/nWS3sCIqwLpjkwgWB0Fq7GrzkNdIukJC/xwbsrlSkme3jlP5c/+GT6/PVx7eJg6PkPSAyTdJWn/BW1eLemYxv3G/BYRQLC2KBhb7MqrJV0o6Z6Zj7dK+oqkizYYCR0QhMwCdpwkf0/L+yWdscVscK0iAQSrIuxOTX1e0ksHfH+7pI9Kumnkflmwvpm06fYPHdkGzXVKAMHqNHCV3P5+SK6n5vx072RJX5jQh19Jemho37msIya0RdMdEUCwOgpWRVc9ynFiPR9ZeeRzgiTno6YqeVLfwviyqYzRbl8EEKy+4lXDW4uUp4F5+Zikd4VE+pR+3Czp/okBr/O6bEqDtN0PAQSrn1jV8jTPWf1G0lkTTwFj3yyIb046+o0wyvM0lAIBznTnHtiHgJPozk+l5ZkbPAEswetRlUdXsfxJ0osr2S7xk7oNCTDCagh/y0y/U9I5mU8fknRqJT8vlvTKxNZ7w8iuknnM9EAAweohStP76FGVR1dp8bKFt01v+j8W8qUMd0jypmnvNaRA4H8EECxuBifZLVYHJSi8CdlTwVrlE5K8ODWW8yWdXcs4dvohgGD1E6spPB1avlB7GYG3+vww69zDJXktFgUC+xBAsHb7hrBQpHsDvQfQOaspF4XmxN8j6czkww9X2ly925HvtPcIVqeBG8HtfPlCC7Fyjuy8pC+/k/RaSZeP0D+amCEBBGuGQV2hS7lQ+JKaSXbb8zIGb/3xyaSx8GRwheDtchUEa/eiPyRWLYQiX1Hvo2getXvhoMclBBCsElr9132QpKuyUU3tJLspHizpjxlO7xesmTvrP5o72AMEa7eC/m5Jb0q6/AtJj2yAwGdovYCpYAPynZtEsDoPYKH7v5b0kOSa2nkrm2aRaGHQqP5/AgjW7twN35H0tKS7NbfdpJR9RE16quhrJH1yd8JATzchgGBtQq+va/8u6d7B5X9I8tnqnhLWLPno6geSnljTAWz1TQDB6jt+q3qfr7m6QtLzV714xHr5ItEWU9IRu0NTtQkgWLWJ17eXb2z2+Vbx+OGa3thmut3m95IOqekAtvongGD1H8NlPci335wi6SPLLprg9/no6n3ZlpwJTNLk3AggWHOL6L79sTg5uR5Lq5zRcyV9NUPN0cfzvvcm6R2CNQnWrWn0i+HUzuhQrdNDcwA/l+QXscbi8+FftzWUcKQbAghWN6Fay9FUKFosEvXI6q2Snpx438KPteBx0fYRQLC2LyZjevTb5CWkt2RvoxnTzlBbp0m6YOAXPBmcmvyM20ewZhzcsG/wqUkXa4jFgZI+Lel5A2jZLzjv+23y3iFYkyNuauDlkj6beHCrpA9MeFb7CyX5uGOLVlriWVt+bZffhkOBwFoEEKy1sHV1UX6cjNdhWViuHrEXFigfUZOeyx6bb/VkcsTu0dS2EECwtiUS0/qRr3R34vuikUZaR4VRlb/nxTaGRGza3tL6bAkgWLMN7d069hdJB2SfelTkRaQ3rInB158xcO2fw+dsal4TLJcNE0CwdufOyDcepz13Mv5rkr63Ig6vqfqSpKFR1bfDqKr2xuoVXadazwQQrJ6jV+6710N9JlvEmbZiwfp6qLNo1PXGMJXME+tup8ZTyPJec8VsCCBYswllUUf84lRvit6r+K3Ll0jyS1U9anLxE8D0LKt4/S9DIv+aIi/6rhxX7vu7GVEqEECwKkDeUhPHSHqHpOes6N+/JN1joK4T685j3RZ+F0de8d8rNr+V1fxmn8clIv0sSY+RdHP4PDrt88Wcr/tcePp6+1b2ZgZOIVgzCOKGXfCLKZ4k6RWS/CabsUsUrr9J+nE4NNCfxc89KvPPvhfjz2P74PYspM653ZUIqz+LIyX/7K/Hh2OkvXbsUEkHFTrT4g1EhS72Wx3B6jd2U3juEcWxkvz25fTs9ylsrdJmFLYobv5ugfFIz68FG8qjxc+iAK1iZ+w610k6YuxGae+//6tRIJAS2CupDinpTknfDUtB7iXJ08ShAxG9j9N5wvxYHRhuQADB2gDezC59lSQfqjc0anFX/YfnkZdzX3F6tQxBbOvRku6zrPIW/f4OST7pYv/w1PSmkLfy1qKhp6cPlvQUSV6gmxamhyMHFcEaGWhnzVlQTgjLFNLzqtJu+AmgV6uP8SQsTtNsK+aP4mfpmi7/fN8lLL04dZU6biZ9eulppdeIxWmmf45rxvLvpeH0WrZnZxdx9lcpxT3qI1gjwuyoKQuGp34WokUjKguC9yF61EVZncDQ6v/Tw6bz1Vuh5iABBGu3bgyPpixS3vy8qFioLFL+msPShNoR9n8Gl2fLHuyDl0dcX9uZudlDsOYW0bv3x39Azk9ZqBZN+3wVQjXeveDFtc5n+alrLF6n5ZfGUjYggGBtAG/LL40iNbQyPXX92jCaYqPyuAH1mrY8Cd/qTP1xe9awNQSrIfwJTFucLFSe8i3KTUWzXqFukRojmT5BV2bR5I+yqeGNkvxEkbImAQRrTXBbdJmfqJ0n6WhJD1vil5/4OZF+GfmpKhH0SzgulnS/xNqp2avXqjgyFyMIVp+RPC6MojyS2isv5d45N+WRlL92aXPytkT2fElnZc5wqsWa0UGw1gRX8TJP7Y4MG3A95VuWk4qufTmMpMhNVQzWAlM+5SI/eZUXcqwRFwRrDWgjXBIXTsaFj+mCSjfvaV5cTb4sF5W645zJx4NQcYDeCIEaqYmDJfmltul/Nt5cbdEih1gAGcEqgFVQ1YLkUVEuPEMndBY0O1jVUz7npLxuiinfpjSnu96byf3UMH2prMXKTw4pKxJAsFYEtUc155MsUBYjf606ZdvEchQpC5W/KP0Q+Kmkw4K7CFZh3BCsQmCSPLz3BuBzJT19waF25a0uv8LrpXyDW6CYRiznta01/JT2JZJ86N/ZxLIsTAjW6rycbzonrFY+ZPXL1qppcfK2GAuTv6Y82G4tB7kIAi0IIFjLqftoFC/GPHODI1I8hYsnBMTv+Ymb9oSR0/J4UGOHCSBYi4PvqZ/P6D6+4P7wwkwLkoXH3z0ySo8yKWiKqhCAQE4AwVp8T5wo6VN73DK3BGHy4W6XMm3jjwsC0xNAsBYzPknShQO/viK8t8/v96NAAAIVCSBYi2E/VpI3CPuJoF/bdKWkCyRdVTE+mIIABBICCNby2+HwcL63H0NTIACBhgQQrIbwMQ0BCJQRQLDKeFEbAhBoSADBaggf0xCAQBkBBKuMF7UhAIGGBBCshvAxDQEIlBFAsMp4URsCEGhIAMFqCB/TEIBAGQEEq4wXtSEAgYYEEKyG8DENAQiUEUCwynhRGwIQaEgAwWoIH9MQgEAZAQSrjBe1IQCBhgQQrIbwMQ0BCJQRQLDKeFEbAhBoSADBaggf0xCAQBkBBKuMF7UhAIGGBBCshvAxDQEIlBFAsMp4URsCEGhIAMFqCB/TEIBAGQEEq4wXtSEAgYYEEKyG8DENAQiUEUCwynhRGwIQaEgAwWoIH9MQgEAZAQSrjBe1IQCBhgQQrIbwMQ0BCJQRQLDKeFEbAhBoSADBaggf0xCAQBkBBKuMF7UhAIGGBBCshvAxDQEIlBFAsMp4URsCEGhIAMFqCB/TEIBAGQEEq4wXtSEAgYYEEKyG8DENAQiUEUCwynhRGwIQaEgAwWoIH9MQgEAZAQSrjBe1IQCBhgQQrIbwMQ0BCJQRQLDKeFEbAhBoSADBaggf0xCAQBkBBKuMF7UhAIGGBBCshvAxDQEIlBFAsMp4URsCEGhIAMFqCB/TEIBAGQEEq4wXtSEAgYYEEKyG8DENAQiUEUCwynhRGwIQaEgAwWoIH9MQgEAZAQSrjBe1IQCBhgQQrIbwMQ0BCJQR+DcIUXGNarJrRQAAAABJRU5ErkJggg==', 0, 5, 85, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:27:14'),
(62, '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAB9CAYAAAAY2F6TAAAMtElEQVR4Xu2de6h1RRnGHxMiEkmNyuxqEWFJSUl2xSKJisKKisKgIkrUNJVSCsKif7pgF8NuGCV2oTKz0LAILDIKSkhQ/9DonpaZWZYFRcZTMzXfuPbZa/Zea82atX8Dh3O+fWbmfef3ru85M7Pmsp9IEIAABBohsF8jfuImBCAAASFYPAQQgEAzBBCsZkKFoxCAAILFMwABCDRDAMFqJlQ4CgEIIFg8AxCAQDMEEKxmQoWjEIAAgsUzAAEINEMAwWomVDgKAQggWDwDEIBAMwQQrGZC1ZSjx0l6RPh6gKSHS7pe0jWSfirppvDvphqFs/UJIFj1Y9C6By+WdJqkoyQdVNCY2yX9RdJPgnj9XtK3w1dBNWTdJQII1i5Fe7i2niDpGEmnDlflPjW9VdIlkm4YqX6qbZQAgtVo4Cq6fb6kkyewf52kN9LjmoB0QyYQrIaCVdlVz0NdLOmJK/zw3JTTFZI8vPM8lT/7Z/j8jlD2iDB0fKak+0u6S9IBK+q8WtLRlduN+RkRQLBmFIwZu/IaSRdI2j/z8TZJX5N04RY9oQODkFnAjpXk72n6kKTTZ8wG1yYkgGBNCLtRU1+U9LIO398p6eOSbh64XRasK5M6Xf9hA9ugukYJIFiNBm4it38QJtdTc3679wZJXxrRh19Kekio33NZR45oi6obIoBgNRSsCV11L8cT63nPyj2f4yV5PmqslE/qWxhfPpYx6m2LAILVVrym8NYi5WFgnj4h6T1hIn1MP26RdL/EgNd5XTqmQepuhwCC1U6spvI0n7P6taQzRx4CxrZZEM9KGvqt0MvzMJQEAc505xnYh4An0T0/laZnbfEGsASve1XuXcX0R0kvmch2iZ/krUiAHlZF+DMz/W5JZ2c+fUTSKRP5eZGkVyW2PhB6dhOZx0wLBBCsFqI0vo/uVbl3lSYvW3jH+Kb/YyFfynCnJG+a9l5DEgT+RwDB4mHwJLvF6uAEhTcheyg4VfqUJC9OjelcSW+eyjh22iGAYLUTqzE87Vq+MPUyAm/1+VHWuIdJ8losEgT2IYBg7fYDYaFI9wZ6D6DnrMZcFJoTf7+kM5IPPzrR5urdjnyjrUewGg3cAG7nyxdqiJXnyM5J2vJbSa+XdNkA7aOKBRJAsBYY1B5NyoXCRaacZLc9L2Pw1h+fTBoTbwZ7BG+XsyBYuxf9LrGqIRT5inofRfPI3QsHLS4hgGCV0Go/7wMlXZX1aqaeZDfFQyT9IcPp/YJTzp21H80dbAGCtVtBf6+ktyRN/rmkwysg8BlaL2QoWIF84yYRrMYDWOj+ryQ9OCkz9byVTbNItDBoZP8/AQRrd56G70p6etLcKbfdpJR9RE16quhrJX16d8JAS7chgGBtQ6+tsn+TdK/g8t8l+Wx1DwmnTHnv6oeSnjSlA9hqmwCC1Xb8+nqfr7m6XNIL+hYeMF++SLTGkHTA5lDV1AQQrKmJT28v39js863i8cNTemOb6Xab30k6dEoHsNU+AQSr/Riua0G+/eYkSR9bV2iE3+e9qw9mW3JGMEmVSyOAYC0tovu2x+LkyfWYas0ZPU/S1zPUHH287GdvlNYhWKNgnU2lXw6ndkaHpjo9NAfwM0m+iDUmnw9/4mwo4UgzBBCsZkK1kaOpUNRYJOqe1dslPSXxvoYfG8Gj0PwIIFjzi8mQHv0muYT01uw2miHtdNV1qqTzOn7Bm8GxyS+4fgRrwcEN+wafljRxCrE4SNJnJT2/Ay37BZf9vI3eOgRrdMRVDbxC0ucTD26T9OERz2p/kSQfd2zRSlM8a8vXdvk2HBIENiKAYG2EralC+XEyXodlYbl6wFZYoHxETXoue6y+1pvJAZtHVXMhgGDNJRLj+pGvdPfE94UD9bSOCr0qf8+TbXSJ2LitpfbFEkCwFhvauzXsz5IOzD51r8iLSG/YEIPLn95R9k/hczY1bwiWYt0EEKzdeTLyjcdpyz0Z/w1J3++Jw2uqviKpq1f1ndCrmnpjdU/XydYyAQSr5eiV++71UJ/LFnGmtViwvhnyrOp1vSkMJfOJddczxVvI8lZTYjEEEKzFhLKoIb441Zui90q+dfliSb5U1b0mJ78BTM+yiuV/ESbyf1zkRduZ48p9fzcj0gQEEKwJIM/UxNGS3iXpuT39+5eke3Tk9cS657FuD7+LPa/4757VzzKbb/Z5bCLSz5b0aEm3hM+j0z5fzPN1XwhvX++YZWsW4BSCtYAgbtkEX0zxZEmvlOSbbIZOUbj+KunGcGigP4ufu1fmn/0sxp+H9sH1WUg953ZXIqz+LPaU/LO/HheOkfbascMkHVzoTI0biApdbDc7gtVu7Mbw3D2KYyT59uX07PcxbPWpMwpbFDd/t8C4p+drwbrm0eJnUYD62Bk6z3WSjhy6Uur77181EgRSAntNqkNK+oek74WlIPeU5GFi14GI3sfpecL8WB0YbkEAwdoC3sKKvlqSD9Xr6rW4qf6P556X577i8GodgljXoyTde13mGf3+Tkk+6eKA8Nb05jBv5a1FXW9PHyTpqZK8QDdNDA8HDiqCNTDQxqqzoBwflimk51WlzfAbQK9WH+JNWBym2VacP4qfpWu6/PN91rD04tQ+eVxN+vbSw0qvEYvDTP8c14zl30vD6bVsz8kKcfZXKcU98iNYA8JsqCoLhod+FqJVPSoLgvchutdF6k+ga/X/aWHTef9ayNlJAMHarQfDvSmLlDc/r0oWKouUv5awNGHqCPuPwWXZsgf74OUR10/tzNLsIVhLi+jd2+P/QJ6fslCtGva5FEI13LPgxbWez/Jb15i8TsuXxpK2IIBgbQFv5kWjSHWtTE9dvyb0ptioPGxAvaYtn4Svdab+sC2rWBuCVRH+CKYtThYqD/lWzU1Fs16hbpEaYjJ9hKYsosprs6HhTZL8RpG0IQEEa0NwMyrmN2rnSHqCpIeu8ctv/DyRfinzU5NE0JdwXCTpvom1U7Kr1yZxZClGEKw2I3ls6EW5J7XXvJRb57kp96T8tUubk+cS2XMlnZk5w6kWG0YHwdoQ3ITFPLR7fNiA6yHfujmp6NpXQ0+KuakJg7XClE+5yE9e5UKODeKCYG0AbYAiceFkXPiYLqh09R7mxdXk6+aiUnc8Z/LJIFQcoDdAoAaq4hBJvtQ2/WPjzdUWLeYQCyAjWAWwCrJakNwryoWn64TOgmo7s3rI5zkpr5tiyLctzfHKezO53xpyqewWjBGsLeCFop5PskBZjPzVd8i2jeUoUhYqf5HaIZDexm2v3yfprHbcr+spglXO3917bwB+m6RnrDjUrrzW9SW8XsrDBwsUw4j1vOaaI5+E99D98Lk6Oze/EKz+EfF809lhtfKh/YttlNPi5G0xFiZ/jXmw3UYOUmgrAvm1aywo7YkTwVoPykejeDHmGVsckeIhXDwhIH7PT9y0J/Sc1sdjCTlOlnR+0pATJflUB9IaAgjWakAe+vmM7uMKniIvzLQgWXj83T2j9CiTgqrIumACj5HkU0lj4oTSnsFGsFaDOkHSZ/bgeGsQJh/udgnDtp5PHNkigSuTFzT+A+dhIYke1sbPwOskXdBR+vJwb5/v9yNBYFMC3iL10lDY16n53yQEa+NnwN12bxD2G0Ff23SFpPMkXbVxjRSEAAS2IsCQcD2+I8L53r57jgQBCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjMC/AdlXeo0HsHoQAAAAAElFTkSuQmCC', 0, 5, 86, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:27:14'),
(63, '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAB9CAYAAAAY2F6TAAAMtElEQVR4Xu2de6h1RRnGHxMiEkmNyuxqEWFJSUl2xSKJisKKisKgIkrUNJVSCsKif7pgF8NuGCV2oTKz0LAILDIKSkhQ/9DonpaZWZYFRcZTMzXfuPbZa/Zea82atX8Dh3O+fWbmfef3ru85M7Pmsp9IEIAABBohsF8jfuImBCAAASFYPAQQgEAzBBCsZkKFoxCAAILFMwABCDRDAMFqJlQ4CgEIIFg8AxCAQDMEEKxmQoWjEIAAgsUzAAEINEMAwWomVDgKAQggWDwDEIBAMwQQrGZC1ZSjx0l6RPh6gKSHS7pe0jWSfirppvDvphqFs/UJIFj1Y9C6By+WdJqkoyQdVNCY2yX9RdJPgnj9XtK3w1dBNWTdJQII1i5Fe7i2niDpGEmnDlflPjW9VdIlkm4YqX6qbZQAgtVo4Cq6fb6kkyewf52kN9LjmoB0QyYQrIaCVdlVz0NdLOmJK/zw3JTTFZI8vPM8lT/7Z/j8jlD2iDB0fKak+0u6S9IBK+q8WtLRlduN+RkRQLBmFIwZu/IaSRdI2j/z8TZJX5N04RY9oQODkFnAjpXk72n6kKTTZ8wG1yYkgGBNCLtRU1+U9LIO398p6eOSbh64XRasK5M6Xf9hA9ugukYJIFiNBm4it38QJtdTc3679wZJXxrRh19Kekio33NZR45oi6obIoBgNRSsCV11L8cT63nPyj2f4yV5PmqslE/qWxhfPpYx6m2LAILVVrym8NYi5WFgnj4h6T1hIn1MP26RdL/EgNd5XTqmQepuhwCC1U6spvI0n7P6taQzRx4CxrZZEM9KGvqt0MvzMJQEAc505xnYh4An0T0/laZnbfEGsASve1XuXcX0R0kvmch2iZ/krUiAHlZF+DMz/W5JZ2c+fUTSKRP5eZGkVyW2PhB6dhOZx0wLBBCsFqI0vo/uVbl3lSYvW3jH+Kb/YyFfynCnJG+a9l5DEgT+RwDB4mHwJLvF6uAEhTcheyg4VfqUJC9OjelcSW+eyjh22iGAYLUTqzE87Vq+MPUyAm/1+VHWuIdJ8losEgT2IYBg7fYDYaFI9wZ6D6DnrMZcFJoTf7+kM5IPPzrR5urdjnyjrUewGg3cAG7nyxdqiJXnyM5J2vJbSa+XdNkA7aOKBRJAsBYY1B5NyoXCRaacZLc9L2Pw1h+fTBoTbwZ7BG+XsyBYuxf9LrGqIRT5inofRfPI3QsHLS4hgGCV0Go/7wMlXZX1aqaeZDfFQyT9IcPp/YJTzp21H80dbAGCtVtBf6+ktyRN/rmkwysg8BlaL2QoWIF84yYRrMYDWOj+ryQ9OCkz9byVTbNItDBoZP8/AQRrd56G70p6etLcKbfdpJR9RE16quhrJX16d8JAS7chgGBtQ6+tsn+TdK/g8t8l+Wx1DwmnTHnv6oeSnjSlA9hqmwCC1Xb8+nqfr7m6XNIL+hYeMF++SLTGkHTA5lDV1AQQrKmJT28v39js863i8cNTemOb6Xab30k6dEoHsNU+AQSr/Riua0G+/eYkSR9bV2iE3+e9qw9mW3JGMEmVSyOAYC0tovu2x+LkyfWYas0ZPU/S1zPUHH287GdvlNYhWKNgnU2lXw6ndkaHpjo9NAfwM0m+iDUmnw9/4mwo4UgzBBCsZkK1kaOpUNRYJOqe1dslPSXxvoYfG8Gj0PwIIFjzi8mQHv0muYT01uw2miHtdNV1qqTzOn7Bm8GxyS+4fgRrwcEN+wafljRxCrE4SNJnJT2/Ay37BZf9vI3eOgRrdMRVDbxC0ucTD26T9OERz2p/kSQfd2zRSlM8a8vXdvk2HBIENiKAYG2EralC+XEyXodlYbl6wFZYoHxETXoue6y+1pvJAZtHVXMhgGDNJRLj+pGvdPfE94UD9bSOCr0qf8+TbXSJ2LitpfbFEkCwFhvauzXsz5IOzD51r8iLSG/YEIPLn95R9k/hczY1bwiWYt0EEKzdeTLyjcdpyz0Z/w1J3++Jw2uqviKpq1f1ndCrmnpjdU/XydYyAQSr5eiV++71UJ/LFnGmtViwvhnyrOp1vSkMJfOJddczxVvI8lZTYjEEEKzFhLKoIb441Zui90q+dfliSb5U1b0mJ78BTM+yiuV/ESbyf1zkRduZ48p9fzcj0gQEEKwJIM/UxNGS3iXpuT39+5eke3Tk9cS657FuD7+LPa/4757VzzKbb/Z5bCLSz5b0aEm3hM+j0z5fzPN1XwhvX++YZWsW4BSCtYAgbtkEX0zxZEmvlOSbbIZOUbj+KunGcGigP4ufu1fmn/0sxp+H9sH1WUg953ZXIqz+LPaU/LO/HheOkfbascMkHVzoTI0biApdbDc7gtVu7Mbw3D2KYyT59uX07PcxbPWpMwpbFDd/t8C4p+drwbrm0eJnUYD62Bk6z3WSjhy6Uur77181EgRSAntNqkNK+oek74WlIPeU5GFi14GI3sfpecL8WB0YbkEAwdoC3sKKvlqSD9Xr6rW4qf6P556X577i8GodgljXoyTde13mGf3+Tkk+6eKA8Nb05jBv5a1FXW9PHyTpqZK8QDdNDA8HDiqCNTDQxqqzoBwflimk51WlzfAbQK9WH+JNWBym2VacP4qfpWu6/PN91rD04tQ+eVxN+vbSw0qvEYvDTP8c14zl30vD6bVsz8kKcfZXKcU98iNYA8JsqCoLhod+FqJVPSoLgvchutdF6k+ga/X/aWHTef9ayNlJAMHarQfDvSmLlDc/r0oWKouUv5awNGHqCPuPwWXZsgf74OUR10/tzNLsIVhLi+jd2+P/QJ6fslCtGva5FEI13LPgxbWez/Jb15i8TsuXxpK2IIBgbQFv5kWjSHWtTE9dvyb0ptioPGxAvaYtn4Svdab+sC2rWBuCVRH+CKYtThYqD/lWzU1Fs16hbpEaYjJ9hKYsosprs6HhTZL8RpG0IQEEa0NwMyrmN2rnSHqCpIeu8ctv/DyRfinzU5NE0JdwXCTpvom1U7Kr1yZxZClGEKw2I3ls6EW5J7XXvJRb57kp96T8tUubk+cS2XMlnZk5w6kWG0YHwdoQ3ITFPLR7fNiA6yHfujmp6NpXQ0+KuakJg7XClE+5yE9e5UKODeKCYG0AbYAiceFkXPiYLqh09R7mxdXk6+aiUnc8Z/LJIFQcoDdAoAaq4hBJvtQ2/WPjzdUWLeYQCyAjWAWwCrJakNwryoWn64TOgmo7s3rI5zkpr5tiyLctzfHKezO53xpyqewWjBGsLeCFop5PskBZjPzVd8i2jeUoUhYqf5HaIZDexm2v3yfprHbcr+spglXO3917bwB+m6RnrDjUrrzW9SW8XsrDBwsUw4j1vOaaI5+E99D98Lk6Oze/EKz+EfF809lhtfKh/YttlNPi5G0xFiZ/jXmw3UYOUmgrAvm1aywo7YkTwVoPykejeDHmGVsckeIhXDwhIH7PT9y0J/Sc1sdjCTlOlnR+0pATJflUB9IaAgjWakAe+vmM7uMKniIvzLQgWXj83T2j9CiTgqrIumACj5HkU0lj4oTSnsFGsFaDOkHSZ/bgeGsQJh/udgnDtp5PHNkigSuTFzT+A+dhIYke1sbPwOskXdBR+vJwb5/v9yNBYFMC3iL10lDY16n53yQEa+NnwN12bxD2G0Ff23SFpPMkXbVxjRSEAAS2IsCQcD2+I8L53r57jgQBCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjACCVcaL3BCAQEUCCFZF+JiGAATKCCBYZbzIDQEIVCSAYFWEj2kIQKCMAIJVxovcEIBARQIIVkX4mIYABMoIIFhlvMgNAQhUJIBgVYSPaQhAoIwAglXGi9wQgEBFAghWRfiYhgAEygggWGW8yA0BCFQkgGBVhI9pCECgjMC/AdlXeo0HsHoQAAAAAElFTkSuQmCC', 0, 5, 87, 1, 0, '2020-11-19 09:24:57', '2020-11-19 09:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `audit_images`
--

CREATE TABLE `audit_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audit_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `image_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `audit_questions`
--

CREATE TABLE `audit_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audit_id` bigint(20) NOT NULL,
  `question_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `question_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `question_type_two` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_options` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_required` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_points` int(11) NOT NULL DEFAULT '0',
  `question_compliance` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_path` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `audit_questions`
--

INSERT INTO `audit_questions` (`id`, `audit_id`, `question_name`, `question`, `question_type`, `question_type_two`, `question_options`, `question_required`, `question_number`, `question_points`, `question_compliance`, `question_path`, `created_at`, `updated_at`) VALUES
(25, 5, 'EXTERIOR VISUAL APPEARANCE', 'EXTERIOR VISUAL APPEARANCE', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(26, 5, 'Toilets clean, have relevant consumables and bathroom in overall good condition', 'Toilets clean, have relevant consumables and bathroom in overall good condition', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(27, 5, 'Are all exterior signage & decals clean, un-obstructed, relevant lights in good working condition', 'Are all exterior signage & decals clean, un-obstructed, relevant lights in good working condition?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(28, 5, 'Are all exterior signage & decals clean, un-obstructed, relevant lights in good working condition? ', 'Are all exterior signage & decals clean, un-obstructed, relevant lights in good working condition? ', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(29, 5, 'Entrance and isles free of obstructions? ', 'Entrance and isles free of obstructions? ', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(30, 5, 'Are all interior signage clear, clean, un-obstructed and relevant to the section in-store?', 'Are all interior signage clear, clean, un-obstructed and relevant to the section in-store?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(31, 5, 'All in-store ceiling lights in working condition, clean and fully covered?', 'All in-store ceiling lights in working condition, clean and fully covered?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(32, 5, 'All fridge lights in working condition?', 'All fridge lights in working condition?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(33, 5, 'CUSTOMER SERVICE & STAFF APPEARANCE', 'CUSTOMER SERVICE & STAFF APPEARANCE', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(34, 5, 'All staff wearing uniform & visible name badges?', 'All staff wearing uniform & visible name badges?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(35, 5, 'Freshstop carrier / paper bags available?', 'Freshstop carrier / paper bags available?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(36, 5, 'Are in-store shop credit card machines in working condition ', 'Are in-store shop credit card machines in working condition ', 'checkbox', '', '', '', '0', 5, 'N', NULL, NULL, NULL),
(37, 5, 'MAINTENANCE & HOUSEKEEPING', 'MAINTENANCE & HOUSEKEEPING', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(38, 5, 'Shopping Baskets available and clean?', 'Shopping Baskets available and clean?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(39, 5, 'Till points neat, clean, uncluttered and adequate space on counters for customers to be served?', 'Till points neat, clean, uncluttered and adequate space on counters for customers to be served?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(40, 5, 'Are shop shelves and display areas (example bread, magazine stands and bulk displays) clean?', 'Are shop shelves and display areas (example bread, magazine stands and bulk displays) clean?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(41, 5, 'Floors clean?', 'Floors clean?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(42, 5, 'No loose wires or potentially dangerous hazard throughout trading area of store?', 'No loose wires or potentially dangerous hazard throughout trading area of store?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(43, 5, 'aution boards available in-store, clean and used when floor being cleaned?', 'aution boards available in-store, clean and used when floor being cleaned?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(44, 5, 'PROMOTIONS', 'PROMOTIONS', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(45, 5, 'Are all shelf strips, ticket holders and fridge strips in good condition?', 'Are all shelf strips, ticket holders and fridge strips in good condition?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(46, 5, 'Price labels not older than 3 months ', 'Price labels not older than 3 months ', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(47, 5, 'Shelves fully stocked and no out of stocks?', 'Shelves fully stocked and no out of stocks?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(48, 5, 'No expired products on shelves?', 'No expired products on shelves?', 'checkbox', '', '', '', '0', 5, 'N', NULL, NULL, NULL),
(49, 5, 'Category Must Have range sufficient (not more than 2 missing)?', 'Category Must Have range sufficient (not more than 2 missing)?', 'checkbox', '', '', '', '0', 5, 'N', NULL, NULL, NULL),
(50, 5, 'Are open gaps being left open and not stocked with other products?', 'Are open gaps being left open and not stocked with other products?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(51, 5, 'GROCERY CONFECTIONARY', 'GROCERY CONFECTIONARY', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(52, 5, 'Are all shelf strips, ticket holders and fridge strips in good condition? ', 'Are all shelf strips, ticket holders and fridge strips in good condition? ', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(53, 5, 'Does products correspond with price labels and does all products have price labels in place?', 'Does products correspond with price labels and does all products have price labels in place?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(54, 5, 'Price labels not older than 3 months', 'Price labels not older than 3 months', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(55, 5, 'No expired products on shelves?', 'No expired products on shelves?', 'checkbox', '', '', '', '0', 5, 'N', NULL, NULL, NULL),
(56, 5, 'PERISHABLES & DAIRY', 'PERISHABLES & DAIRY', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(57, 5, 'Does products correspond with price labels and does all products have price labels in place?', 'Does products correspond with price labels and does all products have price labels in place?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(58, 5, 'No expired products on shelves?', 'No expired products on shelves?', 'checkbox', '', '', '', '0', 5, 'N', NULL, NULL, NULL),
(59, 5, 'Price labels not older than 3 months ', 'Price labels not older than 3 months ', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(60, 5, 'Shelves fully stocked and no out of stocks?', 'Shelves fully stocked and no out of stocks?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(61, 5, 'FRUIT & VEG', 'FRUIT & VEG', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(62, 5, 'No overripe and unsaleable stock on counters?', 'No overripe and unsaleable stock on counters?', 'checkbox', '', '', '', '0', 5, 'N', NULL, NULL, NULL),
(63, 5, 'Shelves fully stocked and no out of stocks?', 'Shelves fully stocked and no out of stocks?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(64, 5, 'Price labels not older than 3 months', 'Price labels not older than 3 months', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(65, 5, 'Does products correspond with price labels and does all products have price labels in place?', 'Does products correspond with price labels and does all products have price labels in place?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(66, 5, 'COLDMEATS', 'COLDMEATS', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(67, 5, 'Are all shelf strips, ticket holders and fridge strips in good condition?', 'Are all shelf strips, ticket holders and fridge strips in good condition?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(68, 5, 'Does products correspond with price labels and does all products have price labels in place?', 'Does products correspond with price labels and does all products have price labels in place?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(69, 5, 'Price labels not older than 3 months', 'Price labels not older than 3 months', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(70, 5, 'Shelves fully stocked and no out of stocks? ', 'Shelves fully stocked and no out of stocks? ', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(71, 5, 'No blown or discoloured products on sale?', 'No blown or discoloured products on sale?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(72, 5, 'PREPARATION AREAS (Behind counters)', 'PREPARATION AREAS (Behind counters)', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(73, 5, 'Floors , basins and surfaces clean? ', 'Floors , basins and surfaces clean? ', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(74, 5, 'Equipment cleaned daily?', 'Equipment cleaned daily?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(75, 5, 'Food safe Soap, Sanitizer and hand towels available?', 'Food safe Soap, Sanitizer and hand towels available?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(76, 5, ' Waste bins cleared out regularly and not overflowing? ', 'Waste bins cleared out regularly and not overflowing? ', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(77, 5, 'BACK OF HOUSE (Kitchen areas, Store Rooms)', 'BACK OF HOUSE (Kitchen areas, Store Rooms)', 'hidden', 'header', '', '', '0', 0, '', NULL, NULL, NULL),
(78, 5, 'Fridges, Freezers and store rooms secure (not unattended to unlocked)?', 'Fridges, Freezers and store rooms secure (not unattended to unlocked)?', 'checkbox', '', '', '', '0', 0, 'N', NULL, NULL, NULL),
(79, 5, 'Fridges, Freezers and store rooms clean & stored according to Q-Pro compliance?', 'Fridges, Freezers and store rooms clean & stored according to Q-Pro compliance?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(80, 5, 'Are dedicated Waste area in place with clearly visible signs?', 'Are dedicated Waste area in place with clearly visible signs?', 'checkbox', '', '', '', '0', 1, 'N', NULL, NULL, NULL),
(81, 5, 'Are the dedicated Returns area in place with clearly marked signs?', 'Are the dedicated Returns area in place with clearly marked signs?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(82, 5, 'Are the FIFO (First In, First Out) being rule adhered to and stock properly rotated?', 'Are the FIFO (First In, First Out) being rule adhered to and stock properly rotated?', 'checkbox', '', '', '', '0', 3, 'N', NULL, NULL, NULL),
(83, 5, '', 'file', 'file', '', '', '', '0', 0, '', NULL, NULL, NULL),
(84, 5, '', '', 'text', '', '', '', '0', 0, '', NULL, NULL, NULL),
(85, 5, 'N. Crafford', 'N. Crafford', 'hidden', 'signature', '', '', '0', 0, '', NULL, NULL, NULL),
(86, 5, 'N. Crafford', 'N. Crafford', 'hidden', 'signature', '', '', '0', 0, '', NULL, NULL, NULL),
(87, 5, 'C. Bouwer', 'C. Bouwer', 'hidden', 'signature', '', '', '0', 0, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `audit_questions2`
--

CREATE TABLE `audit_questions2` (
  `id` int(11) NOT NULL,
  `audit_id` int(11) NOT NULL,
  `form_data` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_questions2`
--

INSERT INTO `audit_questions2` (`id`, `audit_id`, `form_data`, `created_at`, `updated_at`) VALUES
(1, 1, '<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Grocery Shelves - no gaps\" data-form-builder=\"checkbox\" value=\"\" caption=\"Grocery Shelves - no gaps\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Grocery Shelves - no gaps (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container active_element\">\r\n    <label class=\"caption\">SignaGrocery Shelves - no gapsture</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"signaGrocery Shelves - no gapsture\"></canvas>\r\n        <input type=\"hidden\" name=\"signaGrocery Shelves - no gapsture\" value=\"signaGrocery Shelves - no gapsture\" caption=\"SignaGrocery Shelves - no gapsture\" type_two=\"signature\">\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">Signature</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"signature\"></canvas>\r\n        <input type=\"hidden\" name=\"fields[signature]\" value=\"signature:signature\">\r\n    </div>\r\n</div>', '2020-11-10 13:56:32', '2020-11-10 13:56:32'),
(2, 2, '<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Grocery Shelves - no gaps\" data-form-builder=\"checkbox\" value=\"\" caption=\"Grocery Shelves - no gaps\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Grocery Shelves - no gaps (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container active_element\">\r\n    <label class=\"caption\">SignaGrocery Shelves - no gapsture</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"signaGrocery Shelves - no gapsture\"></canvas>\r\n        <input type=\"hidden\" name=\"signaGrocery Shelves - no gapsture\" value=\"signaGrocery Shelves - no gapsture\" caption=\"SignaGrocery Shelves - no gapsture\" type_two=\"signature\">\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">Signature</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"signature\"></canvas>\r\n        <input type=\"hidden\" name=\"fields[signature]\" value=\"signature:signature\">\r\n    </div>\r\n</div>', '2020-11-10 13:58:21', '2020-11-10 13:58:21'),
(3, 3, '<div class=\"input-container\">\r\n    <div class=\"caption header\" data-level=\"header\" data-form-builder=\"header\">Header</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"\" value=\"\">\r\n</div>\r\n<div class=\"input-container active_element\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Entrance and aisles free of obstructions?kbox\" data-form-builder=\"checkbox\" value=\"\" caption=\" Entrance and aisles free of obstructions?heckbox (1 points)\" points=\"0\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\"> Entrance and aisles free of obstructions?heckbox (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">Signature</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"signature\"></canvas>\r\n        <input type=\"hidden\" name=\"fields[signature]\" value=\"signature:signature\">\r\n    </div>\r\n</div>', '2020-11-10 14:21:02', '2020-11-10 14:21:02'),
(4, 4, '<div class=\"input-container\">\r\n    <div class=\"caption header\" data-level=\"header\" data-form-builder=\"header\">Header</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"\" value=\"\">\r\n</div>\r\n<div class=\"input-container active_element\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Entrance and aisles free of obstructions?kbox\" data-form-builder=\"checkbox\" value=\"\" caption=\" Entrance and aisles free of obstructions?heckbox (1 points)\" points=\"0\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\"> Entrance and aisles free of obstructions?heckbox (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">Signature</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"signature\"></canvas>\r\n        <input type=\"hidden\" name=\"fields[signature]\" value=\"signature:signature\">\r\n    </div>\r\n</div>', '2020-11-10 14:28:14', '2020-11-10 14:28:14'),
(5, 5, '<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">EXTERIOR VISUAL APPEARANCE</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"EXTERIOR VISUAL APPEARANCE\" value=\"\" class=\"caption sub-header\" caption=\"EXTERIOR VISUAL APPEARANCE\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\" style=\"\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Toilets clean, have relevant consumables and bathroom in overall good condition\" data-form-builder=\"checkbox\" value=\"\" caption=\"Toilets clean, have relevant consumables and bathroom in overall good condition\" points=\"0\"\r\n        flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Toilets clean, have relevant consumables and bathroom in overall good condition</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are all exterior signage &amp; decals clean, un-obstructed, relevant lights in good working condition?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are all exterior signage &amp; decals clean, un-obstructed, relevant lights in good working condition\"\r\n        points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are all exterior signage &amp; decals clean, un-obstructed, relevant lights in good working condition (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are all exterior signage &amp; decals clean, un-obstructed, relevant lights in good working condition? \" data-form-builder=\"checkbox\" value=\"\" caption=\"Are all exterior signage &amp; decals clean, un-obstructed, relevant lights in good working condition? \"\r\n        points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are all exterior signage &amp; decals clean, un-obstructed, relevant lights in good working condition?  (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Entrance and isles free of obstructions? \" data-form-builder=\"checkbox\" value=\"\" caption=\"Entrance and isles free of obstructions? \" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Entrance and isles free of obstructions?  (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are all interior signage clear, clean, un-obstructed and relevant to the section in-store?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are all interior signage clear, clean, un-obstructed and relevant to the section in-store?\"\r\n        points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are all interior signage clear, clean, un-obstructed and relevant to the section in-store? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"All in-store ceiling lights in working condition, clean and fully covered?\" data-form-builder=\"checkbox\" value=\"\" caption=\"All in-store ceiling lights in working condition, clean and fully covered?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">All in-store ceiling lights in working condition, clean and fully covered? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"All fridge lights in working condition?\" data-form-builder=\"checkbox\" value=\"\" caption=\"All fridge lights in working condition?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">All fridge lights in working condition? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">CUSTOMER SERVICE &amp; STAFF APPEARANCE</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"CUSTOMER SERVICE &amp; STAFF APPEARANCE\" value=\"\" class=\"caption sub-header\" caption=\"CUSTOMER SERVICE &amp; STAFF APPEARANCE\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"All staff wearing uniform &amp; visible name badges?\" data-form-builder=\"checkbox\" value=\"\" caption=\"All staff wearing uniform &amp; visible name badges?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">All staff wearing uniform &amp; visible name badges? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Freshstop carrier / paper bags available?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Freshstop carrier / paper bags available?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Freshstop carrier / paper bags available? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are in-store shop credit card machines in working condition \" data-form-builder=\"checkbox\" value=\"\" caption=\"Are in-store shop credit card machines in working condition \" points=\"5\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are in-store shop credit card machines in working condition  (5 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">MAINTENANCE &amp; HOUSEKEEPING</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"MAINTENANCE &amp; HOUSEKEEPING\" value=\"\" class=\"caption sub-header\" caption=\"MAINTENANCE &amp; HOUSEKEEPING\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Shopping Baskets available and clean?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Shopping Baskets available and clean?\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Shopping Baskets available and clean? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Till points neat, clean, uncluttered and adequate space on counters for customers to be served?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Till points neat, clean, uncluttered and adequate space on counters for customers to be served?\"\r\n        points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Till points neat, clean, uncluttered and adequate space on counters for customers to be served? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are shop shelves and display areas (example bread, magazine stands and bulk displays) clean?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are shop shelves and display areas (example bread, magazine stands and bulk displays) clean?\"\r\n        points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are shop shelves and display areas (example bread, magazine stands and bulk displays) clean? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Floors clean?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Floors clean?\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Floors clean? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"No loose wires or potentially dangerous hazard throughout trading area of store?\" data-form-builder=\"checkbox\" value=\"\" caption=\"No loose wires or potentially dangerous hazard throughout trading area of store?\" points=\"1\"\r\n        flag=\"N\"><span class=\"check\"></span><span class=\"caption\">No loose wires or potentially dangerous hazard throughout trading area of store? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"aution boards available in-store, clean and used when floor being cleaned?\" data-form-builder=\"checkbox\" value=\"\" caption=\"aution boards available in-store, clean and used when floor being cleaned?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">aution boards available in-store, clean and used when floor being cleaned? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">PROMOTIONS</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"PROMOTIONS\" value=\"\" class=\"caption sub-header\" caption=\"PROMOTIONS\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are all shelf strips, ticket holders and fridge strips in good condition?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are all shelf strips, ticket holders and fridge strips in good condition?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are all shelf strips, ticket holders and fridge strips in good condition? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Price labels not older than 3 months \" data-form-builder=\"checkbox\" value=\"\" caption=\"Price labels not older than 3 months \" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Price labels not older than 3 months  (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Shelves fully stocked and no out of stocks?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Shelves fully stocked and no out of stocks?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Shelves fully stocked and no out of stocks? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"No expired products on shelves?\" data-form-builder=\"checkbox\" value=\"\" caption=\"No expired products on shelves?\" points=\"5\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">No expired products on shelves? (5 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Category Must Have range sufficient (not more than 2 missing)?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Category Must Have range sufficient (not more than 2 missing)?\" points=\"5\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Category Must Have range sufficient (not more than 2 missing)? (5 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are open gaps being left open and not stocked with other products?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are open gaps being left open and not stocked with other products?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are open gaps being left open and not stocked with other products? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">GROCERY CONFECTIONARY</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"GROCERY CONFECTIONARY\" value=\"\" class=\"caption sub-header\" caption=\"GROCERY CONFECTIONARY\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are all shelf strips, ticket holders and fridge strips in good condition? \" data-form-builder=\"checkbox\" value=\"\" caption=\"Are all shelf strips, ticket holders and fridge strips in good condition? \" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are all shelf strips, ticket holders and fridge strips in good condition?  (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Does products correspond with price labels and does all products have price labels in place?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Does products correspond with price labels and does all products have price labels in place?\"\r\n        points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Does products correspond with price labels and does all products have price labels in place? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Price labels not older than 3 months\" data-form-builder=\"checkbox\" value=\"\" caption=\"Price labels not older than 3 months\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Price labels not older than 3 months (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"No expired products on shelves?\" data-form-builder=\"checkbox\" value=\"\" caption=\"No expired products on shelves?\" points=\"5\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">No expired products on shelves? (5 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">PERISHABLES &amp; DAIRY</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"PERISHABLES &amp; DAIRY\" value=\"\" class=\"caption sub-header\" caption=\"PERISHABLES &amp; DAIRY\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Does products correspond with price labels and does all products have price labels in place?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Does products correspond with price labels and does all products have price labels in place?\"\r\n        points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Does products correspond with price labels and does all products have price labels in place? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"No expired products on shelves?\" data-form-builder=\"checkbox\" value=\"\" caption=\"No expired products on shelves?\" points=\"5\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">No expired products on shelves? (5 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Price labels not older than 3 months \" data-form-builder=\"checkbox\" value=\"\" caption=\"Price labels not older than 3 months \" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Price labels not older than 3 months  (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Shelves fully stocked and no out of stocks?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Shelves fully stocked and no out of stocks?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Shelves fully stocked and no out of stocks? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">FRUIT &amp; VEG</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"FRUIT &amp; VEG\" value=\"\" class=\"caption sub-header\" caption=\"FRUIT &amp; VEG\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"No overripe and unsaleable stock on counters?\" data-form-builder=\"checkbox\" value=\"\" caption=\"No overripe and unsaleable stock on counters?\" points=\"5\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">No overripe and unsaleable stock on counters? (5 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Shelves fully stocked and no out of stocks?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Shelves fully stocked and no out of stocks?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Shelves fully stocked and no out of stocks? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Price labels not older than 3 months\" data-form-builder=\"checkbox\" value=\"\" caption=\"Price labels not older than 3 months\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Price labels not older than 3 months (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Does products correspond with price labels and does all products have price labels in place?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Does products correspond with price labels and does all products have price labels in place?\"\r\n        points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Does products correspond with price labels and does all products have price labels in place? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">COLDMEATS</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"COLDMEATS\" value=\"\" class=\"caption sub-header\" caption=\"COLDMEATS\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are all shelf strips, ticket holders and fridge strips in good condition?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are all shelf strips, ticket holders and fridge strips in good condition?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are all shelf strips, ticket holders and fridge strips in good condition? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Does products correspond with price labels and does all products have price labels in place?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Does products correspond with price labels and does all products have price labels in place?\"\r\n        points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Does products correspond with price labels and does all products have price labels in place? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\" style=\"\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Price labels not older than 3 months\" data-form-builder=\"checkbox\" value=\"\" caption=\"Price labels not older than 3 months\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Price labels not older than 3 months (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Shelves fully stocked and no out of stocks? \" data-form-builder=\"checkbox\" value=\"\" caption=\"Shelves fully stocked and no out of stocks? \" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Shelves fully stocked and no out of stocks?  (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"No blown or discoloured products on sale?\" data-form-builder=\"checkbox\" value=\"\" caption=\"No blown or discoloured products on sale?\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">No blown or discoloured products on sale? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">PREPARATION AREAS (Behind counters)</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"PREPARATION AREAS (Behind counters)\" value=\"\" class=\"caption sub-header\" caption=\"PREPARATION AREAS (Behind counters)\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Floors , basins and surfaces clean? \" data-form-builder=\"checkbox\" value=\"\" caption=\"Floors , basins and surfaces clean? \" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Floors , basins and surfaces clean?  (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Equipment cleaned daily?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Equipment cleaned daily?\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Equipment cleaned daily? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Food safe Soap, Sanitizer and hand towels available?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Food safe Soap, Sanitizer and hand towels available?\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Food safe Soap, Sanitizer and hand towels available? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Waste bins cleared out regularly and not overflowing? \" data-form-builder=\"checkbox\" value=\"\" caption=\" Waste bins cleared out regularly and not overflowing? \" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\"> Waste bins cleared out regularly and not overflowing?  (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <div class=\"caption sub-header\" data-level=\"header\" data-form-builder=\"header\">BACK OF HOUSE (Kitchen areas, Store Rooms)</div>\r\n    <input type=\"hidden\" data-form-builder=\"header\" name=\"BACK OF HOUSE (Kitchen areas, Store Rooms)\" value=\"\" class=\"caption sub-header\" caption=\"BACK OF HOUSE (Kitchen areas, Store Rooms)\" points=\"0\" type_two=\"header\">\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Fridges, Freezers and store rooms secure (not unattended to unlocked)?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Fridges, Freezers and store rooms secure (not unattended to unlocked)?\" points=\"0\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Fridges, Freezers and store rooms secure (not unattended to unlocked)?</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Fridges, Freezers and store rooms clean &amp; stored according to Q-Pro compliance?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Fridges, Freezers and store rooms clean &amp; stored according to Q-Pro compliance?\" points=\"3\"\r\n        flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Fridges, Freezers and store rooms clean &amp; stored according to Q-Pro compliance? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are dedicated Waste area in place with clearly visible signs?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are dedicated Waste area in place with clearly visible signs?\" points=\"1\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are dedicated Waste area in place with clearly visible signs? (1 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are the dedicated Returns area in place with clearly marked signs?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are the dedicated Returns area in place with clearly marked signs?\" points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are the dedicated Returns area in place with clearly marked signs? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"input-control checkbox small-check\">\r\n        <input type=\"checkbox\" name=\"Are the FIFO (First In, First Out) being rule adhered to and stock properly rotated?\" data-form-builder=\"checkbox\" value=\"\" caption=\"Are the FIFO (First In, First Out) being rule adhered to and stock properly rotated?\"\r\n        points=\"3\" flag=\"N\"><span class=\"check\"></span><span class=\"caption\">Are the FIFO (First In, First Out) being rule adhered to and stock properly rotated? (3 points)</span></label>\r\n</div>\r\n<div class=\"input-container\" style=\"\">\r\n    <label class=\"caption\">File</label>\r\n    <div class=\"input-control file full-size\" data-role=\"input\">\r\n        <input type=\"file\" name=\"file\" tabindex=\"-1\" style=\"z-index: 0;\">\r\n        <input type=\"text\" class=\"input-file-wrapper\" readonly=\"\" style=\"z-index: 1; cursor: default;\">\r\n        <button class=\"button\" data-form-builder=\"file\" type=\"button\"><span class=\"mif-folder\"></span></button>\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">N. Crafford</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"N. Crafford\"></canvas>\r\n        <input type=\"hidden\" name=\"N. Crafford\" value=\"N. Crafford\" caption=\"N. Crafford\" type_two=\"signature\">\r\n    </div>\r\n</div>\r\n<div class=\"input-container\">\r\n    <label class=\"caption\">N. Crafford</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"N. Crafford\"></canvas>\r\n        <input type=\"hidden\" name=\"N. Crafford\" value=\"N. Crafford\" caption=\"N. Crafford\" type_two=\"signature\">\r\n    </div>\r\n</div>\r\n<div class=\"input-container active_element\">\r\n    <label class=\"caption\">C. Bouwer</label>\r\n    <div class=\"input-control text full-size\" style=\"height:100px\">\r\n        <canvas id=\"myCanvas\" style=\"background: gainsboro;\" width=\"200\" height=\"100\" data-form-builder=\"signature\" name=\"C. Bouwer\"></canvas>\r\n        <input type=\"hidden\" name=\"C. Bouwer\" value=\"C. Bouwer\" caption=\"C. Bouwer\" type_two=\"signature\">\r\n    </div>\r\n</div>', '2020-11-17 11:11:31', '2020-11-17 11:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `audit_sites`
--

CREATE TABLE `audit_sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audit_id` bigint(20) NOT NULL,
  `site_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0 = not started, 1 = completed, 2 = saved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `audit_sites`
--

INSERT INTO `audit_sites` (`id`, `audit_id`, `site_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 185, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banners_id` int(11) NOT NULL,
  `banners_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `banners_url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` text COLLATE utf8_unicode_ci NOT NULL,
  `banners_group` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `banners_html_text` text COLLATE utf8_unicode_ci,
  `expires_impressions` int(11) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `banners_slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banners_history`
--

CREATE TABLE `banners_history` (
  `banners_history_id` int(11) NOT NULL,
  `banners_id` int(11) NOT NULL,
  `banners_shown` int(11) NOT NULL DEFAULT '0',
  `banners_clicked` int(11) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `block_ips`
--

CREATE TABLE `block_ips` (
  `id` int(11) NOT NULL,
  `device_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `categories_image` text COLLATE utf8_unicode_ci,
  `categories_icon` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `categories_slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `categories_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categories_image`, `categories_icon`, `parent_id`, `sort_order`, `date_added`, `last_modified`, `categories_slug`, `categories_status`, `created_at`, `updated_at`) VALUES
(-1, '120', '120', 0, 0, NULL, NULL, 'uncategorized', 1, NULL, NULL),
(1, '121', '121', 0, NULL, NULL, NULL, 'iauditor-audits-current', 1, '2020-08-14 00:29:10', NULL),
(2, '121', '121', 0, NULL, NULL, NULL, 'iauditor-audits-historical', 1, '2020-08-14 00:29:37', NULL),
(3, '121', '121', 0, NULL, NULL, NULL, 'apc-weekly-sales', 1, '2020-08-14 00:29:57', NULL),
(4, '121', '121', 0, NULL, NULL, NULL, 'admin-tracker', 1, '2020-08-14 00:30:11', NULL),
(5, '121', '121', 0, NULL, NULL, NULL, 'weekly-sales', 1, '2020-08-14 00:30:26', NULL),
(6, '121', '121', 0, NULL, NULL, NULL, 'historical-team-dashboard', 1, '2020-08-14 00:30:45', NULL),
(7, '121', '121', 0, NULL, NULL, NULL, 'food-training', 1, '2020-08-14 00:30:59', NULL),
(8, '121', '121', 0, NULL, NULL, NULL, 'business-snapshot', 1, '2020-08-14 00:31:14', NULL),
(9, '121', '121', 0, NULL, NULL, NULL, 'meat-surveys', 1, '2020-08-14 00:31:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories_description`
--

CREATE TABLE `categories_description` (
  `categories_description_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories_description`
--

INSERT INTO `categories_description` (`categories_description_id`, `categories_id`, `language_id`, `categories_name`) VALUES
(1, -1, 1, 'Uncategorized'),
(3, 1, 1, 'iAuditor Audits (Current)'),
(4, 2, 1, 'iAuditor Audits (Historical)'),
(5, 3, 1, 'APC Weekly Sales'),
(6, 4, 1, 'Admin Tracker'),
(7, 5, 1, 'Weekly Sales'),
(8, 6, 1, 'Historical Team Dashboard'),
(9, 7, 1, 'Food Training'),
(10, 8, 1, 'Business Snapshot'),
(11, 9, 1, 'Meat Surveys');

-- --------------------------------------------------------

--
-- Table structure for table `categories_organization`
--

CREATE TABLE `categories_organization` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_organization`
--

INSERT INTO `categories_organization` (`id`, `organization_id`, `categories_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 2, 1),
(10, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `categories_role`
--

CREATE TABLE `categories_role` (
  `categories_role_id` int(11) NOT NULL,
  `categories_ids` text COLLATE utf8_unicode_ci NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `compare`
--

CREATE TABLE `compare` (
  `id` int(11) NOT NULL,
  `product_ids` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `constant_banners`
--

CREATE TABLE `constant_banners` (
  `banners_id` int(11) NOT NULL,
  `banners_title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `banners_url` text COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` text COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL,
  `type` varchar(55) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `constant_banners`
--

INSERT INTO `constant_banners` (`banners_id`, `banners_title`, `banners_url`, `banners_image`, `date_added`, `status`, `languages_id`, `type`) VALUES
(1, 'style0', '', '114', '2019-09-08 18:43:14', 1, 1, '1'),
(2, 'style0', '', '114', '2019-09-08 18:43:25', 1, 1, '2'),
(3, 'banner1', '', '83', '2019-09-08 18:43:34', 1, 1, '3'),
(4, 'banner1', '', '83', '2019-09-08 18:43:42', 1, 1, '4'),
(5, 'banner1', '', '83', '2019-09-08 18:44:15', 1, 1, '5'),
(6, 'banner2_3_4', '', '84', '2019-09-10 08:50:55', 1, 1, '6'),
(7, 'banner2_3_4', '', '85', '2019-09-10 08:54:18', 1, 1, '7'),
(8, 'banner2_3_4', '', '86', '2019-09-10 08:54:28', 1, 1, '8'),
(9, 'banner2_3_4', '', '86', '2019-09-10 08:54:38', 1, 1, '9'),
(10, 'banner5_6', '', '92', '2019-09-10 09:31:13', 1, 1, '10'),
(11, 'banner5_6', '', '92', '2019-09-10 09:31:24', 1, 1, '11'),
(12, 'banner5_6', '', '92', '2019-09-10 09:31:35', 1, 1, '12'),
(13, 'banner5_6', '', '92', '2019-09-10 09:32:18', 1, 1, '13'),
(14, 'banner5_6', '', '91', '2019-09-10 09:32:28', 1, 1, '14'),
(15, 'banner7_8', '', '95', '2019-09-10 09:52:02', 1, 1, '15'),
(16, 'banner7_8', '', '96', '2019-09-10 09:52:29', 1, 1, '16'),
(17, 'banner7_8', '', '96', '2019-09-10 09:47:56', 1, 1, '17'),
(18, 'banner7_8', '', '94', '2019-09-10 09:48:05', 1, 1, '18'),
(19, 'banner9', '', '97', '2019-09-10 10:19:03', 1, 1, '19'),
(20, 'banner9', '', '97', '2019-09-10 10:19:13', 1, 1, '20'),
(21, 'banner10_11_12', '', '98', '2019-09-10 10:26:12', 1, 1, '21'),
(22, 'banner10_11_12', '', '96', '2019-09-10 10:26:30', 1, 1, '22'),
(23, 'banner10_11_12', '', '96', '2019-09-10 10:26:41', 1, 1, '23'),
(24, 'banner10_11_12', '', '99', '2019-09-10 10:26:54', 1, 1, '24'),
(25, 'banner13_14_15', '', '100', '2019-09-10 11:01:09', 1, 1, '25'),
(26, 'banner13_14_15', '', '101', '2019-09-10 11:01:27', 1, 1, '26'),
(27, 'banner13_14_15', '', '101', '2019-09-10 11:02:12', 1, 1, '27'),
(28, 'banner13_14_15', '', '101', '2019-09-10 11:02:23', 1, 1, '28'),
(29, 'banner13_14_15', '', '101', '2019-09-10 11:02:36', 1, 1, '29'),
(30, 'banner16_17', '', '104', '2019-09-10 11:19:45', 1, 1, '30'),
(31, 'banner16_17', '', '104', '2019-09-10 11:19:58', 1, 1, '31'),
(32, 'banner16_17', '', '105', '2019-09-10 11:21:00', 1, 1, '32'),
(33, 'banner18_19', '', '116', '2019-09-10 11:30:35', 1, 1, '33'),
(34, 'banner18_19', '', '116', '2019-09-10 11:30:49', 1, 1, '34'),
(35, 'banner18_19', '', '96', '2019-09-10 11:31:04', 1, 1, '35'),
(36, 'banner18_19', '', '96', '2019-09-10 11:31:20', 1, 1, '36'),
(37, 'banner18_19', '', '115', '2019-09-10 11:31:54', 1, 1, '37'),
(38, 'banner18_19', '', '115', '2019-09-10 11:32:06', 1, 1, '38'),
(39, 'ad_banner1', '', '107', '2019-09-11 06:17:45', 1, 1, '39'),
(40, 'ad_banner2', '', '106', '2019-09-11 06:17:58', 1, 1, '40'),
(81, 'ad_banner3', '', '107', '0000-00-00 00:00:00', 1, 1, '41');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `countries_id` int(11) NOT NULL,
  `countries_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_2` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_3` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `address_format_id` int(11) NOT NULL,
  `country_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `country_code`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 1, NULL),
(2, 'Albania', 'AL', 'ALB', 1, NULL),
(3, 'Algeria', 'DZ', 'DZA', 1, NULL),
(4, 'American Samoa', 'AS', 'ASM', 1, NULL),
(5, 'Andorra', 'AD', 'AND', 1, NULL),
(6, 'Angola', 'AO', 'AGO', 1, NULL),
(7, 'Anguilla', 'AI', 'AIA', 1, NULL),
(8, 'Antarctica', 'AQ', 'ATA', 1, NULL),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 1, NULL),
(10, 'Argentina', 'AR', 'ARG', 1, NULL),
(11, 'Armenia', 'AM', 'ARM', 1, NULL),
(12, 'Aruba', 'AW', 'ABW', 1, NULL),
(13, 'Australia', 'AU', 'AUS', 1, NULL),
(14, 'Austria', 'AT', 'AUT', 5, NULL),
(15, 'Azerbaijan', 'AZ', 'AZE', 1, NULL),
(16, 'Bahamas', 'BS', 'BHS', 1, NULL),
(17, 'Bahrain', 'BH', 'BHR', 1, NULL),
(18, 'Bangladesh', 'BD', 'BGD', 1, NULL),
(19, 'Barbados', 'BB', 'BRB', 1, NULL),
(20, 'Belarus', 'BY', 'BLR', 1, NULL),
(21, 'Belgium', 'BE', 'BEL', 1, NULL),
(22, 'Belize', 'BZ', 'BLZ', 1, NULL),
(23, 'Benin', 'BJ', 'BEN', 1, NULL),
(24, 'Bermuda', 'BM', 'BMU', 1, NULL),
(25, 'Bhutan', 'BT', 'BTN', 1, NULL),
(26, 'Bolivia', 'BO', 'BOL', 1, NULL),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 1, NULL),
(28, 'Botswana', 'BW', 'BWA', 1, NULL),
(29, 'Bouvet Island', 'BV', 'BVT', 1, NULL),
(30, 'Brazil', 'BR', 'BRA', 1, NULL),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1, NULL),
(32, 'Brunei Darussalam', 'BN', 'BRN', 1, NULL),
(33, 'Bulgaria', 'BG', 'BGR', 1, NULL),
(34, 'Burkina Faso', 'BF', 'BFA', 1, NULL),
(35, 'Burundi', 'BI', 'BDI', 1, NULL),
(36, 'Cambodia', 'KH', 'KHM', 1, NULL),
(37, 'Cameroon', 'CM', 'CMR', 1, NULL),
(38, 'Canada', 'CA', 'CAN', 1, NULL),
(39, 'Cape Verde', 'CV', 'CPV', 1, NULL),
(40, 'Cayman Islands', 'KY', 'CYM', 1, NULL),
(41, 'Central African Republic', 'CF', 'CAF', 1, NULL),
(42, 'Chad', 'TD', 'TCD', 1, NULL),
(43, 'Chile', 'CL', 'CHL', 1, NULL),
(44, 'China', 'CN', 'CHN', 1, NULL),
(45, 'Christmas Island', 'CX', 'CXR', 1, NULL),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1, NULL),
(47, 'Colombia', 'CO', 'COL', 1, NULL),
(48, 'Comoros', 'KM', 'COM', 1, NULL),
(49, 'Congo', 'CG', 'COG', 1, NULL),
(50, 'Cook Islands', 'CK', 'COK', 1, NULL),
(51, 'Costa Rica', 'CR', 'CRI', 1, NULL),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', 1, NULL),
(53, 'Croatia', 'HR', 'HRV', 1, NULL),
(54, 'Cuba', 'CU', 'CUB', 1, NULL),
(55, 'Cyprus', 'CY', 'CYP', 1, NULL),
(56, 'Czech Republic', 'CZ', 'CZE', 1, NULL),
(57, 'Denmark', 'DK', 'DNK', 1, NULL),
(58, 'Djibouti', 'DJ', 'DJI', 1, NULL),
(59, 'Dominica', 'DM', 'DMA', 1, NULL),
(60, 'Dominican Republic', 'DO', 'DOM', 1, NULL),
(61, 'East Timor', 'TP', 'TMP', 1, NULL),
(62, 'Ecuador', 'EC', 'ECU', 1, NULL),
(63, 'Egypt', 'EG', 'EGY', 1, NULL),
(64, 'El Salvador', 'SV', 'SLV', 1, NULL),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1, NULL),
(66, 'Eritrea', 'ER', 'ERI', 1, NULL),
(67, 'Estonia', 'EE', 'EST', 1, NULL),
(68, 'Ethiopia', 'ET', 'ETH', 1, NULL),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1, NULL),
(70, 'Faroe Islands', 'FO', 'FRO', 1, NULL),
(71, 'Fiji', 'FJ', 'FJI', 1, NULL),
(72, 'Finland', 'FI', 'FIN', 1, NULL),
(73, 'France', 'FR', 'FRA', 1, NULL),
(74, 'France, Metropolitan', 'FX', 'FXX', 1, NULL),
(75, 'French Guiana', 'GF', 'GUF', 1, NULL),
(76, 'French Polynesia', 'PF', 'PYF', 1, NULL),
(77, 'French Southern Territories', 'TF', 'ATF', 1, NULL),
(78, 'Gabon', 'GA', 'GAB', 1, NULL),
(79, 'Gambia', 'GM', 'GMB', 1, NULL),
(80, 'Georgia', 'GE', 'GEO', 1, NULL),
(81, 'Germany', 'DE', 'DEU', 5, NULL),
(82, 'Ghana', 'GH', 'GHA', 1, NULL),
(83, 'Gibraltar', 'GI', 'GIB', 1, NULL),
(84, 'Greece', 'GR', 'GRC', 1, NULL),
(85, 'Greenland', 'GL', 'GRL', 1, NULL),
(86, 'Grenada', 'GD', 'GRD', 1, NULL),
(87, 'Guadeloupe', 'GP', 'GLP', 1, NULL),
(88, 'Guam', 'GU', 'GUM', 1, NULL),
(89, 'Guatemala', 'GT', 'GTM', 1, NULL),
(90, 'Guinea', 'GN', 'GIN', 1, NULL),
(91, 'Guinea-bissau', 'GW', 'GNB', 1, NULL),
(92, 'Guyana', 'GY', 'GUY', 1, NULL),
(93, 'Haiti', 'HT', 'HTI', 1, NULL),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1, NULL),
(95, 'Honduras', 'HN', 'HND', 1, NULL),
(96, 'Hong Kong', 'HK', 'HKG', 1, NULL),
(97, 'Hungary', 'HU', 'HUN', 1, NULL),
(98, 'Iceland', 'IS', 'ISL', 1, NULL),
(99, 'India', 'IN', 'IND', 1, NULL),
(100, 'Indonesia', 'ID', 'IDN', 1, NULL),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1, NULL),
(102, 'Iraq', 'IQ', 'IRQ', 1, NULL),
(103, 'Ireland', 'IE', 'IRL', 1, NULL),
(104, 'Israel', 'IL', 'ISR', 1, NULL),
(105, 'Italy', 'IT', 'ITA', 1, NULL),
(106, 'Jamaica', 'JM', 'JAM', 1, NULL),
(107, 'Japan', 'JP', 'JPN', 1, NULL),
(108, 'Jordan', 'JO', 'JOR', 1, NULL),
(109, 'Kazakhstan', 'KZ', 'KAZ', 1, NULL),
(110, 'Kenya', 'KE', 'KEN', 1, NULL),
(111, 'Kiribati', 'KI', 'KIR', 1, NULL),
(112, 'Korea, Democratic People\'s Republic of', 'KP', 'PRK', 1, NULL),
(113, 'Korea, Republic of', 'KR', 'KOR', 1, NULL),
(114, 'Kuwait', 'KW', 'KWT', 1, NULL),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 1, NULL),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', 1, NULL),
(117, 'Latvia', 'LV', 'LVA', 1, NULL),
(118, 'Lebanon', 'LB', 'LBN', 1, NULL),
(119, 'Lesotho', 'LS', 'LSO', 1, NULL),
(120, 'Liberia', 'LR', 'LBR', 1, NULL),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1, NULL),
(122, 'Liechtenstein', 'LI', 'LIE', 1, NULL),
(123, 'Lithuania', 'LT', 'LTU', 1, NULL),
(124, 'Luxembourg', 'LU', 'LUX', 1, NULL),
(125, 'Macau', 'MO', 'MAC', 1, NULL),
(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', 1, NULL),
(127, 'Madagascar', 'MG', 'MDG', 1, NULL),
(128, 'Malawi', 'MW', 'MWI', 1, NULL),
(129, 'Malaysia', 'MY', 'MYS', 1, NULL),
(130, 'Maldives', 'MV', 'MDV', 1, NULL),
(131, 'Mali', 'ML', 'MLI', 1, NULL),
(132, 'Malta', 'MT', 'MLT', 1, NULL),
(133, 'Marshall Islands', 'MH', 'MHL', 1, NULL),
(134, 'Martinique', 'MQ', 'MTQ', 1, NULL),
(135, 'Mauritania', 'MR', 'MRT', 1, NULL),
(136, 'Mauritius', 'MU', 'MUS', 1, NULL),
(137, 'Mayotte', 'YT', 'MYT', 1, NULL),
(138, 'Mexico', 'MX', 'MEX', 1, NULL),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', 1, NULL),
(140, 'Moldova, Republic of', 'MD', 'MDA', 1, NULL),
(141, 'Monaco', 'MC', 'MCO', 1, NULL),
(142, 'Mongolia', 'MN', 'MNG', 1, NULL),
(143, 'Montserrat', 'MS', 'MSR', 1, NULL),
(144, 'Morocco', 'MA', 'MAR', 1, NULL),
(145, 'Mozambique', 'MZ', 'MOZ', 1, NULL),
(146, 'Myanmar', 'MM', 'MMR', 1, NULL),
(147, 'Namibia', 'NA', 'NAM', 1, NULL),
(148, 'Nauru', 'NR', 'NRU', 1, NULL),
(149, 'Nepal', 'NP', 'NPL', 1, NULL),
(150, 'Netherlands', 'NL', 'NLD', 1, NULL),
(151, 'Netherlands Antilles', 'AN', 'ANT', 1, NULL),
(152, 'New Caledonia', 'NC', 'NCL', 1, NULL),
(153, 'New Zealand', 'NZ', 'NZL', 1, NULL),
(154, 'Nicaragua', 'NI', 'NIC', 1, NULL),
(155, 'Niger', 'NE', 'NER', 1, NULL),
(156, 'Nigeria', 'NG', 'NGA', 1, NULL),
(157, 'Niue', 'NU', 'NIU', 1, NULL),
(158, 'Norfolk Island', 'NF', 'NFK', 1, NULL),
(159, 'Northern Mariana Islands', 'MP', 'MNP', 1, NULL),
(160, 'Norway', 'NO', 'NOR', 1, NULL),
(161, 'Oman', 'OM', 'OMN', 1, NULL),
(162, 'Pakistan', 'PK', 'PAK', 1, NULL),
(163, 'Palau', 'PW', 'PLW', 1, NULL),
(164, 'Panama', 'PA', 'PAN', 1, NULL),
(165, 'Papua New Guinea', 'PG', 'PNG', 1, NULL),
(166, 'Paraguay', 'PY', 'PRY', 1, NULL),
(167, 'Peru', 'PE', 'PER', 1, NULL),
(168, 'Philippines', 'PH', 'PHL', 1, NULL),
(169, 'Pitcairn', 'PN', 'PCN', 1, NULL),
(170, 'Poland', 'PL', 'POL', 1, NULL),
(171, 'Portugal', 'PT', 'PRT', 1, NULL),
(172, 'Puerto Rico', 'PR', 'PRI', 1, NULL),
(173, 'Qatar', 'QA', 'QAT', 1, NULL),
(174, 'Reunion', 'RE', 'REU', 1, NULL),
(175, 'Romania', 'RO', 'ROM', 1, NULL),
(176, 'Russian Federation', 'RU', 'RUS', 1, NULL),
(177, 'Rwanda', 'RW', 'RWA', 1, NULL),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1, NULL),
(179, 'Saint Lucia', 'LC', 'LCA', 1, NULL),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1, NULL),
(181, 'Samoa', 'WS', 'WSM', 1, NULL),
(182, 'San Marino', 'SM', 'SMR', 1, NULL),
(183, 'Sao Tome and Principe', 'ST', 'STP', 1, NULL),
(184, 'Saudi Arabia', 'SA', 'SAU', 1, NULL),
(185, 'Senegal', 'SN', 'SEN', 1, NULL),
(186, 'Seychelles', 'SC', 'SYC', 1, NULL),
(187, 'Sierra Leone', 'SL', 'SLE', 1, NULL),
(188, 'Singapore', 'SG', 'SGP', 4, NULL),
(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 1, NULL),
(190, 'Slovenia', 'SI', 'SVN', 1, NULL),
(191, 'Solomon Islands', 'SB', 'SLB', 1, NULL),
(192, 'Somalia', 'SO', 'SOM', 1, NULL),
(193, 'South Africa', 'ZA', 'ZAF', 1, NULL),
(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 1, NULL),
(195, 'Spain', 'ES', 'ESP', 3, NULL),
(196, 'Sri Lanka', 'LK', 'LKA', 1, NULL),
(197, 'St. Helena', 'SH', 'SHN', 1, NULL),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1, NULL),
(199, 'Sudan', 'SD', 'SDN', 1, NULL),
(200, 'Suriname', 'SR', 'SUR', 1, NULL),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1, NULL),
(202, 'Swaziland', 'SZ', 'SWZ', 1, NULL),
(203, 'Sweden', 'SE', 'SWE', 1, NULL),
(204, 'Switzerland', 'CH', 'CHE', 1, NULL),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 1, NULL),
(206, 'Taiwan', 'TW', 'TWN', 1, NULL),
(207, 'Tajikistan', 'TJ', 'TJK', 1, NULL),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1, NULL),
(209, 'Thailand', 'TH', 'THA', 1, NULL),
(210, 'Togo', 'TG', 'TGO', 1, NULL),
(211, 'Tokelau', 'TK', 'TKL', 1, NULL),
(212, 'Tonga', 'TO', 'TON', 1, NULL),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 1, NULL),
(214, 'Tunisia', 'TN', 'TUN', 1, NULL),
(215, 'Turkey', 'TR', 'TUR', 1, NULL),
(216, 'Turkmenistan', 'TM', 'TKM', 1, NULL),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1, NULL),
(218, 'Tuvalu', 'TV', 'TUV', 1, NULL),
(219, 'Uganda', 'UG', 'UGA', 1, NULL),
(220, 'Ukraine', 'UA', 'UKR', 1, NULL),
(221, 'United Arab Emirates', 'AE', 'ARE', 1, NULL),
(222, 'United Kingdom', 'GB', 'GBR', 1, NULL),
(223, 'United States', 'US', 'USA', 2, NULL),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1, NULL),
(225, 'Uruguay', 'UY', 'URY', 1, NULL),
(226, 'Uzbekistan', 'UZ', 'UZB', 1, NULL),
(227, 'Vanuatu', 'VU', 'VUT', 1, NULL),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1, NULL),
(229, 'Venezuela', 'VE', 'VEN', 1, NULL),
(230, 'Viet Nam', 'VN', 'VNM', 1, NULL),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 1, NULL),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1, NULL),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1, NULL),
(234, 'Western Sahara', 'EH', 'ESH', 1, NULL),
(235, 'Yemen', 'YE', 'YEM', 1, NULL),
(236, 'Yugoslavia', 'YU', 'YUG', 1, NULL),
(237, 'Zaire', 'ZR', 'ZAR', 1, NULL),
(238, 'Zambia', 'ZM', 'ZMB', 1, NULL),
(239, 'Zimbabwe', 'ZW', 'ZWE', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupans_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `discount_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Options: fixed_cart, percent, fixed_product and percent_product. Default: fixed_cart.',
  `amount` int(11) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `usage_count` int(11) NOT NULL,
  `individual_use` tinyint(1) NOT NULL DEFAULT '0',
  `product_ids` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `exclude_product_ids` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `usage_limit_per_user` int(11) DEFAULT NULL,
  `limit_usage_to_x_items` int(11) NOT NULL,
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `product_categories` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `excluded_product_categories` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `exclude_sale_items` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount` decimal(10,2) NOT NULL,
  `maximum_amount` decimal(10,2) NOT NULL,
  `email_restrictions` text COLLATE utf8_unicode_ci NOT NULL,
  `used_by` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `symbol_left` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol_right` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thousands_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_places` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `value` double(13,8) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_current` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `created_at`, `updated_at`, `value`, `is_default`, `status`, `is_current`) VALUES
(1, 'Zar', 'ZAR', 'R', '', NULL, NULL, '2', '2020-08-13 07:24:59', '2020-08-13 07:24:59', 1.00000000, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency_record`
--

CREATE TABLE `currency_record` (
  `id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `currency_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currency_record`
--

INSERT INTO `currency_record` (`id`, `code`, `currency_name`) VALUES
(1, 'AED', 'United Arab Emirates Dirham'),
(2, 'AFN', 'Afghan Afghani'),
(3, 'ALL', 'Albanian Lek'),
(4, 'AMD', 'Armenian Dram'),
(5, 'ANG', 'Netherlands Antillean Guilder'),
(6, 'AOA', 'Angolan Kwanza'),
(7, 'ARS', 'Argentine Peso'),
(8, 'AUD', 'Australian Dollar'),
(9, 'AWG', 'Aruban Florin'),
(10, 'AZN', 'Azerbaijani Manat'),
(11, 'BAM', 'Bosnia-Herzegovina Convertible Mark'),
(12, 'BBD', 'Barbadian Dollar'),
(13, 'BDT', 'Bangladeshi Taka'),
(14, 'BGN', 'Bulgarian Lev'),
(15, 'BHD', 'Bahraini Dinar'),
(16, 'BIF', 'Burundian Franc'),
(17, 'BMD', 'Bermudan Dollar'),
(18, 'BND', 'Brunei Dollar'),
(19, 'BOB', 'Bolivian Boliviano'),
(20, 'BRL', 'Brazilian Real'),
(21, 'BSD', 'Bahamian Dollar'),
(22, 'BTC', 'Bitcoin'),
(23, 'BTN', 'Bhutanese Ngultrum'),
(24, 'BWP', 'Botswanan Pula'),
(25, 'BYN', 'Belarusian Ruble'),
(26, 'BZD', 'Belize Dollar'),
(27, 'CAD', 'Canadian Dollar'),
(28, 'CDF', 'Congolese Franc'),
(29, 'CHF', 'Swiss Franc'),
(30, 'CLF', 'Chilean Unit of Account (UF)'),
(31, 'CLP', 'Chilean Peso'),
(32, 'CNH', 'Chinese Yuan (Offshore)'),
(33, 'CNY', 'Chinese Yuan'),
(34, 'COP', 'Colombian Peso'),
(35, 'CRC', 'Costa Rican Colón'),
(36, 'CUC', 'Cuban Convertible Peso'),
(37, 'CUP', 'Cuban Peso'),
(38, 'CVE', 'Cape Verdean Escudo'),
(39, 'CZK', 'Czech Republic Koruna'),
(40, 'DJF', 'Djiboutian Franc'),
(41, 'DKK', 'Danish Krone'),
(42, 'DOP', 'Dominican Peso'),
(43, 'DZD', 'Algerian Dinar'),
(44, 'EGP', 'Egyptian Pound'),
(45, 'ERN', 'Eritrean Nakfa'),
(46, 'ETB', 'Ethiopian Birr'),
(47, 'EUR', 'Euro'),
(48, 'FJD', 'Fijian Dollar'),
(49, 'FKP', 'Falkland Islands Pound'),
(50, 'GBP', 'British Pound Sterling'),
(51, 'GEL', 'Georgian Lari'),
(52, 'GGP', 'Guernsey Pound'),
(53, 'GHS', 'Ghanaian Cedi'),
(54, 'GIP', 'Gibraltar Pound'),
(55, 'GMD', 'Gambian Dalasi'),
(56, 'GNF', 'Guinean Franc'),
(57, 'GTQ', 'Guatemalan Quetzal'),
(58, 'GYD', 'Guyanaese Dollar'),
(59, 'HKD', 'Hong Kong Dollar'),
(60, 'HNL', 'Honduran Lempira'),
(61, 'HRK', 'Croatian Kuna'),
(62, 'HTG', 'Haitian Gourde'),
(63, 'HUF', 'Hungarian Forint'),
(64, 'IDR', 'Indonesian Rupiah'),
(65, 'ILS', 'Israeli New Sheqel'),
(66, 'IMP', 'Manx pound'),
(67, 'INR', 'Indian Rupee'),
(68, 'IQD', 'Iraqi Dinar'),
(69, 'IRR', 'Iranian Rial'),
(70, 'ISK', 'Icelandic Króna'),
(71, 'JEP', 'Jersey Pound'),
(72, 'JMD', 'Jamaican Dollar'),
(73, 'JOD', 'Jordanian Dinar'),
(74, 'JPY', 'Japanese Yen'),
(75, 'KES', 'Kenyan Shilling'),
(76, 'KGS', 'Kyrgystani Som'),
(77, 'KHR', 'Cambodian Riel'),
(78, 'KMF', 'Comorian Franc'),
(79, 'KPW', 'North Korean Won'),
(80, 'KRW', 'South Korean Won'),
(81, 'KWD', 'Kuwaiti Dinar'),
(82, 'KYD', 'Cayman Islands Dollar'),
(83, 'KZT', 'Kazakhstani Tenge'),
(84, 'LAK', 'Laotian Kip'),
(85, 'LBP', 'Lebanese Pound'),
(86, 'LKR', 'Sri Lankan Rupee'),
(87, 'LRD', 'Liberian Dollar'),
(88, 'LSL', 'Lesotho Loti'),
(89, 'LYD', 'Libyan Dinar'),
(90, 'MAD', 'Moroccan Dirham'),
(91, 'MDL', 'Moldovan Leu'),
(92, 'MGA', 'Malagasy Ariary'),
(93, 'MKD', 'Macedonian Denar'),
(94, 'MMK', 'Myanma Kyat'),
(95, 'MNT', 'Mongolian Tugrik'),
(96, 'MOP', 'Macanese Pataca'),
(97, 'MRO', 'Mauritanian Ouguiya (pre-2018)'),
(98, 'MRU', 'Mauritanian Ouguiya'),
(99, 'MUR', 'Mauritian Rupee'),
(100, 'MVR', 'Maldivian Rufiyaa'),
(101, 'MWK', 'Malawian Kwacha'),
(102, 'MXN', 'Mexican Peso'),
(103, 'MYR', 'Malaysian Ringgit'),
(104, 'MZN', 'Mozambican Metical'),
(105, 'NAD', 'Namibian Dollar'),
(106, 'NGN', 'Nigerian Naira'),
(107, 'NIO', 'Nicaraguan Córdoba'),
(108, 'NOK', 'Norwegian Krone'),
(109, 'NPR', 'Nepalese Rupee'),
(110, 'NZD', 'New Zealand Dollar'),
(111, 'OMR', 'Omani Rial'),
(112, 'PAB', 'Panamanian Balboa'),
(113, 'PEN', 'Peruvian Nuevo Sol'),
(114, 'PGK', 'Papua New Guinean Kina'),
(115, 'PHP', 'Philippine Peso'),
(116, 'PKR', 'Pakistani Rupee'),
(117, 'PLN', 'Polish Zloty'),
(118, 'PYG', 'Paraguayan Guarani'),
(119, 'QAR', 'Qatari Rial'),
(120, 'RON', 'Romanian Leu'),
(121, 'RSD', 'Serbian Dinar'),
(122, 'RUB', 'Russian Ruble'),
(123, 'RWF', 'Rwandan Franc'),
(124, 'SAR', 'Saudi Riyal'),
(125, 'SBD', 'Solomon Islands Dollar'),
(126, 'SCR', 'Seychellois Rupee'),
(127, 'SDG', 'Sudanese Pound'),
(128, 'SEK', 'Swedish Krona'),
(129, 'SGD', 'Singapore Dollar'),
(130, 'SHP', 'Saint Helena Pound'),
(131, 'SLL', 'Sierra Leonean Leone'),
(132, 'SOS', 'Somali Shilling'),
(133, 'SRD', 'Surinamese Dollar'),
(134, 'SSP', 'South Sudanese Pound'),
(135, 'STD', 'São Tomé and Príncipe Dobra (pre-2018)'),
(136, 'STN', 'São Tomé and Príncipe Dobra'),
(137, 'SVC', 'Salvadoran Colón'),
(138, 'SYP', 'Syrian Pound'),
(139, 'SZL', 'Swazi Lilangeni'),
(140, 'THB', 'Thai Baht'),
(141, 'TJS', 'Tajikistani Somoni'),
(142, 'TMT', 'Turkmenistani Manat'),
(143, 'TND', 'Tunisian Dinar'),
(144, 'TOP', 'Tongan Pa\'anga'),
(145, 'TRY', 'Turkish Lira'),
(146, 'TTD', 'Trinidad and Tobago Dollar'),
(147, 'TWD', 'New Taiwan Dollar'),
(148, 'TZS', 'Tanzanian Shilling'),
(149, 'UAH', 'Ukrainian Hryvnia'),
(150, 'UGX', 'Ugandan Shilling'),
(151, 'USD', 'United States Dollar'),
(152, 'UYU', 'Uruguayan Peso'),
(153, 'UZS', 'Uzbekistan Som'),
(154, 'VEF', 'Venezuelan Bolívar Fuerte'),
(155, 'VND', 'Vietnamese Dong'),
(156, 'VUV', 'Vanuatu Vatu'),
(157, 'WST', 'Samoan Tala'),
(158, 'XAF', 'CFA Franc BEAC'),
(159, 'XAG', 'Silver Ounce'),
(160, 'XAU', 'Gold Ounce'),
(161, 'XCD', 'East Caribbean Dollar'),
(162, 'XDR', 'Special Drawing Rights'),
(163, 'XOF', 'CFA Franc BCEAO'),
(164, 'XPD', 'Palladium Ounce'),
(165, 'XPF', 'CFP Franc'),
(166, 'XPT', 'Platinum Ounce'),
(167, 'YER', 'Yemeni Rial'),
(168, 'ZAR', 'South African Rand'),
(169, 'ZMW', 'Zambian Kwacha'),
(170, 'ZWL', 'Zimbabwean Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `current_theme`
--

CREATE TABLE `current_theme` (
  `id` int(11) NOT NULL,
  `top_offer` int(11) NOT NULL,
  `header` int(11) NOT NULL,
  `carousel` int(11) NOT NULL,
  `banner` int(11) NOT NULL,
  `footer` int(11) NOT NULL,
  `product_section_order` text COLLATE utf8_unicode_ci NOT NULL,
  `cart` int(11) NOT NULL,
  `news` int(11) NOT NULL,
  `detail` int(11) NOT NULL,
  `shop` int(11) NOT NULL,
  `contact` int(11) NOT NULL,
  `login` int(11) NOT NULL,
  `transitions` int(11) NOT NULL,
  `banner_two` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `current_theme`
--

INSERT INTO `current_theme` (`id`, `top_offer`, `header`, `carousel`, `banner`, `footer`, `product_section_order`, `cart`, `news`, `detail`, `shop`, `contact`, `login`, `transitions`, `banner_two`) VALUES
(1, 1, 1, 1, 1, 1, '[{\"id\":10,\"name\":\"Second Ad Section\",\"order\":1,\"file_name\":\"sec_ad_banner\",\"status\":1,\"image\":\"images\\/prototypes\\/sec_ad_section.jpg\",\"disabled_image\":\"images\\/prototypes\\/sec_ad_section-cross.jpg\",\"alt\":\"Second Ad Section\"},{\"id\":5,\"name\":\"Categories\",\"order\":2,\"file_name\":\"categories\",\"status\":1,\"image\":\"images\\/prototypes\\/categories.jpg\",\"disabled_image\":\"images\\/prototypes\\/categories-cross.jpg\",\"alt\":\"Categories\"},{\"id\":1,\"name\":\"Banner Section\",\"order\":3,\"file_name\":\"banner_section\",\"status\":1,\"image\":\"images\\/prototypes\\/banner_section.jpg\",\"alt\":\"Banner Section\"},{\"id\":9,\"name\":\"Top Selling\",\"order\":4,\"file_name\":\"top\",\"status\":1,\"image\":\"images\\/prototypes\\/top.jpg\",\"disabled_image\":\"images\\/prototypes\\/top-cross.jpg\",\"alt\":\"Top Selling\"},{\"id\":8,\"name\":\"Newest Product Section\",\"order\":5,\"file_name\":\"newest_product\",\"status\":1,\"image\":\"images\\/prototypes\\/newest_product.jpg\",\"disabled_image\":\"images\\/prototypes\\/newest_product-cross.jpg\",\"alt\":\"Newest Product Section\"},{\"id\":11,\"name\":\"Tab Products View\",\"order\":6,\"file_name\":\"tab\",\"status\":1,\"image\":\"images\\/prototypes\\/tab.jpg\",\"disabled_image\":\"images\\/prototypes\\/tab-cross.jpg\",\"alt\":\"Tab Products View\"},{\"id\":3,\"name\":\"Special Products Section\",\"order\":7,\"file_name\":\"special\",\"status\":1,\"image\":\"images\\/prototypes\\/special_product.jpg\",\"disabled_image\":\"images\\/prototypes\\/special_product-cross.jpg\",\"alt\":\"Special Products Section\"},{\"id\":2,\"name\":\"Flash Sale Section\",\"order\":8,\"file_name\":\"flash_sale_section\",\"status\":1,\"image\":\"images\\/prototypes\\/flash_sale_section.jpg\",\"disabled_image\":\"images\\/prototypes\\/flash_sale_section-cross.jpg\",\"alt\":\"Flash Sale Section\"},{\"id\":4,\"name\":\"Ad Section\",\"order\":9,\"file_name\":\"ad_banner_section\",\"status\":1,\"image\":\"images\\/prototypes\\/ad_banner_section.jpg\",\"disabled_image\":\"images\\/prototypes\\/ad_banner_section-cross.jpg\",\"alt\":\"Ad Section\"},{\"id\":6,\"name\":\"Blog Section\",\"order\":10,\"file_name\":\"blog_section\",\"status\":1,\"image\":\"images\\/prototypes\\/blog_section.jpg\",\"disabled_image\":\"images\\/prototypes\\/blog_section-cross.jpg\",\"alt\":\"Blog Section\"},{\"id\":7,\"name\":\"Info Boxes\",\"order\":11,\"file_name\":\"info_boxes\",\"status\":1,\"image\":\"images\\/prototypes\\/info_boxes.jpg\",\"disabled_image\":\"images\\/prototypes\\/info_boxes-cross.jpg\",\"alt\":\"Info Boxes\"}]', 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customers_id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `customers_fax` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_newsletter` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_id_tiwilo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers_basket`
--

CREATE TABLE `customers_basket` (
  `customers_basket_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `products_id` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_basket_quantity` int(11) NOT NULL,
  `final_price` decimal(15,2) DEFAULT NULL,
  `customers_basket_date_added` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_order` tinyint(1) NOT NULL DEFAULT '0',
  `session_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers_basket_attributes`
--

CREATE TABLE `customers_basket_attributes` (
  `customers_basket_attributes_id` int(11) NOT NULL,
  `customers_basket_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `products_id` text COLLATE utf8_unicode_ci NOT NULL,
  `products_options_id` int(11) NOT NULL,
  `products_options_values_id` int(11) NOT NULL,
  `session_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers_info`
--

CREATE TABLE `customers_info` (
  `customers_info_id` int(11) NOT NULL,
  `customers_info_date_of_last_logon` datetime DEFAULT NULL,
  `customers_info_number_of_logons` int(11) DEFAULT NULL,
  `customers_info_date_account_created` datetime DEFAULT NULL,
  `customers_info_date_account_last_modified` datetime DEFAULT NULL,
  `global_product_notifications` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `device_id` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `device_type` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ram` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_os` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latittude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_model` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `operating_system` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `browser_info` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_notify` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categories_id` bigint(20) NOT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `file_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `document_path` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `categories_id`, `user_type_id`, `file_name`, `file_type`, `document_path`, `created_at`, `updated_at`) VALUES
(1, 3, 0, 'top20-Aug2020.xlsx', 'xlsx', 'C:\\Users\\User\\Documents\\dev\\Web\\xampp\\freshstop_audit\\storage\\app\\2020\\08\\top20-Aug2020.xlsx', '2020-08-19 07:44:04', '2020-08-19 07:44:04'),
(2, 3, 0, 'Spur-Main-Standard-Menu.pdf', 'pdf', 'C:\\Users\\User\\Documents\\dev\\Web\\xampp\\freshstop_audit\\storage\\app/2020/08/Spur-Main-Standard-Menu.pdf', '2020-08-19 08:49:40', '2020-08-19 08:49:40'),
(3, 5, 0, 'Spur-Main-Standard-Menu.pdf', 'pdf', 'C:\\Users\\User\\Documents\\dev\\Web\\xampp\\freshstop_audit\\storage\\app/2020/08/Spur-Main-Standard-Menu.pdf', '2020-08-19 09:26:30', '2020-08-19 09:26:30'),
(4, 7, 0, 'Spur-Main-Standard-Menu.pdf', 'pdf', 'C:\\Users\\User\\Documents\\dev\\Web\\xampp\\freshstop_audit\\storage\\app/2020/08/Spur-Main-Standard-Menu.pdf', '2020-08-19 11:03:17', '2020-08-19 11:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `flash_sale`
--

CREATE TABLE `flash_sale` (
  `flash_sale_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `flash_sale_products_price` decimal(15,2) NOT NULL,
  `flash_sale_date_added` int(11) NOT NULL,
  `flash_sale_last_modified` int(11) NOT NULL,
  `flash_start_date` int(11) NOT NULL,
  `flash_expires_date` int(11) NOT NULL,
  `flash_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flate_rate`
--

CREATE TABLE `flate_rate` (
  `id` int(11) NOT NULL,
  `flate_rate` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `flate_rate`
--

INSERT INTO `flate_rate` (`id`, `flate_rate`, `currency`) VALUES
(1, '11', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `front_end_theme_content`
--

CREATE TABLE `front_end_theme_content` (
  `id` int(11) NOT NULL,
  `top_offers` text COLLATE utf8_unicode_ci NOT NULL,
  `headers` text COLLATE utf8_unicode_ci NOT NULL,
  `carousels` text COLLATE utf8_unicode_ci NOT NULL,
  `banners` text COLLATE utf8_unicode_ci NOT NULL,
  `footers` text COLLATE utf8_unicode_ci NOT NULL,
  `product_section_order` text COLLATE utf8_unicode_ci NOT NULL,
  `cart` text COLLATE utf8_unicode_ci NOT NULL,
  `news` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `shop` text COLLATE utf8_unicode_ci NOT NULL,
  `contact` text COLLATE utf8_unicode_ci NOT NULL,
  `login` text COLLATE utf8_unicode_ci NOT NULL,
  `transitions` text COLLATE utf8_unicode_ci NOT NULL,
  `banners_two` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `front_end_theme_content`
--

INSERT INTO `front_end_theme_content` (`id`, `top_offers`, `headers`, `carousels`, `banners`, `footers`, `product_section_order`, `cart`, `news`, `detail`, `shop`, `contact`, `login`, `transitions`, `banners_two`) VALUES
(1, '', '[\n{\n\"id\": 1,\n\"name\": \"Header One\",\n\"image\": \"images/prototypes/header1.jpg\",\n\"alt\" : \"header One\" \n},\n{\n\"id\": 2,\n\"name\": \"Header Two\",\n\"image\": \"images/prototypes/header2.jpg\",\n\"alt\" : \"header Two\" \n},\n{\n\"id\": 3,\n\"name\": \"Header Three\",\n\"image\": \"images/prototypes/header3.jpg\",\n\"alt\" : \"header Three\" \n},\n{\n\"id\": 4,\n\"name\": \"Header Four\",\n\"image\": \"images/prototypes/header4.jpg\",\n\"alt\" : \"header Four\" \n},\n{\n\"id\": 5,\n\"name\": \"Header Five\",\n\"image\": \"images/prototypes/header5.jpg\",\n\"alt\" : \"header Five\" \n},\n{\n\"id\": 6,\n\"name\": \"Header Six\",\n\"image\": \"images/prototypes/header6.jpg\",\n\"alt\" : \"header Six\" \n},\n{\n\"id\": 7,\n\"name\": \"Header Seven\",\n\"image\": \"images/prototypes/header7.jpg\",\n\"alt\" : \"header Seven\" \n},\n{\n\"id\": 8,\n\"name\": \"Header Eight\",\n\"image\": \"images/prototypes/header8.jpg\",\n\"alt\" : \"header Eight\" \n},\n{\n\"id\": 9,\n\"name\": \"Header Nine\",\n\"image\": \"images/prototypes/header9.jpg\",\n\"alt\" : \"header Nine\" \n},\n{\n\"id\": 10,\n\"name\": \"Header Ten\",\n\"image\": \"images/prototypes/header10.jpg\",\n\"alt\" : \"header Ten\" \n}\n]', '[\n{\n\"id\": 1,\n\"name\": \"Bootstrap Carousel Content Full Screen\",\n\"image\": \"images/prototypes/carousal1.jpg\",\n\"alt\": \"Bootstrap Carousel Content Full Screen\"\n},\n{\n\"id\": 2,\n\"name\": \"Bootstrap Carousel Content Full Width\",\n\"image\": \"images/prototypes/carousal2.jpg\",\n\"alt\": \"Bootstrap Carousel Content Full Width\"\n},\n{\n\"id\": 3,\n\"name\": \"Bootstrap Carousel Content with Left Banner\",\n\"image\": \"images/prototypes/carousal3.jpg\",\n\"alt\": \"Bootstrap Carousel Content with Left Banner\"\n},\n{\n\"id\": 4,\n\"name\": \"Bootstrap Carousel Content with Navigation\",\n\"image\": \"images/prototypes/carousal4.jpg\",\n\"alt\": \"Bootstrap Carousel Content with Navigation\"\n},\n{\n\"id\": 5,\n\"name\": \"Bootstrap Carousel Content with Right Banner\",\n\"image\": \"images/prototypes/carousal5.jpg\",\n\"alt\": \"Bootstrap Carousel Content with Right Banner\"\n}\n]', '[\n{\n\"id\": 1,\n\"name\": \"Banner One\",\n\"image\": \"images/prototypes/banner1.jpg\",\n\"alt\": \"Banner One\"\n},\n{\n\"id\": 2,\n\"name\": \"Banner Two\",\n\"image\": \"images/prototypes/banner2.jpg\",\n\"alt\": \"Banner Two\"\n},\n{\n\"id\": 3,\n\"name\": \"Banner Three\",\n\"image\": \"images/prototypes/banner3.jpg\",\n\"alt\": \"Banner Three\"\n},\n{\n\"id\": 4,\n\"name\": \"Banner Four\",\n\"image\": \"images/prototypes/banner4.jpg\",\n\"alt\": \"Banner Four\"\n},\n{\n\"id\": 5,\n\"name\": \"Banner Five\",\n\"image\": \"images/prototypes/banner5.jpg\",\n\"alt\": \"Banner Five\"\n},\n{\n\"id\": 6,\n\"name\": \"Banner Six\",\n\"image\": \"images/prototypes/banner6.jpg\",\n\"alt\": \"Banner Six\"\n},\n{\n\"id\": 7,\n\"name\": \"Banner Seven\",\n\"image\": \"images/prototypes/banner7.jpg\",\n\"alt\": \"Banner Seven\"\n},\n{\n\"id\": 8,\n\"name\": \"Banner Eight\",\n\"image\": \"images/prototypes/banner8.jpg\",\n\"alt\": \"Banner Eight\"\n},\n{\n\"id\": 9,\n\"name\": \"Banner Nine\",\n\"image\": \"images/prototypes/banner9.jpg\",\n\"alt\": \"Banner Nine\"\n},\n{\n\"id\": 10,\n\"name\": \"Banner Ten\",\n\"image\": \"images/prototypes/banner10.jpg\",\n\"alt\": \"Banner Ten\"\n},\n{\n\"id\": 11,\n\"name\": \"Banner Eleven\",\n\"image\": \"images/prototypes/banner11.jpg\",\n\"alt\": \"Banner Eleven\"\n},\n{\n\"id\": 12,\n\"name\": \"Banner Twelve\",\n\"image\": \"images/prototypes/banner12.jpg\",\n\"alt\": \"Banner Twelve\"\n},\n{\n\"id\": 13,\n\"name\": \"Banner Thirteen\",\n\"image\": \"images/prototypes/banner13.jpg\",\n\"alt\": \"Banner Thirteen\"\n},\n{\n\"id\": 14,\n\"name\": \"Banner Fourteen\",\n\"image\": \"images/prototypes/banner14.jpg\",\n\"alt\": \"Banner Fourteen\"\n},\n{\n\"id\": 15,\n\"name\": \"Banner Fifteen\",\n\"image\": \"images/prototypes/banner15.jpg\",\n\"alt\": \"Banner Fifteen\"\n},\n{\n\"id\": 16,\n\"name\": \"Banner Sixteen\",\n\"image\": \"images/prototypes/banner16.jpg\",\n\"alt\": \"Banner Sixteen\"\n},\n{\n\"id\": 17,\n\"name\": \"Banner Seventeen\",\n\"image\": \"images/prototypes/banner17.jpg\",\n\"alt\": \"Banner Seventeen\"\n},\n{\n\"id\": 18,\n\"name\": \"Banner Eighteen\",\n\"image\": \"images/prototypes/banner18.jpg\",\n\"alt\": \"Banner Eighteen\"\n},\n{\n\"id\": 19,\n\"name\": \"Banner Nineteen\",\n\"image\": \"images/prototypes/banner19.jpg\",\n\"alt\": \"Banner Nineteen\"\n}\n]', '[\n{\n\"id\": 1,\n\"name\": \"Footer One\",\n\"image\": \"images/prototypes/footer1.png\",\n\"alt\" : \"Footer One\"\n},\n{\n\"id\": 2,\n\"name\": \"Footer Two\",\n\"image\": \"images/prototypes/footer2.png\",\n\"alt\" : \"Footer Two\"\n},\n{\n\"id\": 3,\n\"name\": \"Footer Three\",\n\"image\": \"images/prototypes/footer3.png\",\n\"alt\" : \"Footer Three\"\n},\n{\n\"id\": 4,\n\"name\": \"Footer Four\",\n\"image\": \"images/prototypes/footer4.png\",\n\"alt\" : \"Footer Four\"\n},\n{\n\"id\": 5,\n\"name\": \"Footer Five\",\n\"image\": \"images/prototypes/footer5.png\",\n\"alt\" : \"Footer Five\"\n},\n{\n\"id\": 6,\n\"name\": \"Footer Six\",\n\"image\": \"images/prototypes/footer6.png\",\n\"alt\" : \"Footer Six\"\n},\n{\n\"id\": 7,\n\"name\": \"Footer Seven\",\n\"image\": \"images/prototypes/footer7.png\",\n\"alt\" : \"Footer Seven\"\n},\n{\n\"id\": 8,\n\"name\": \"Footer Eight\",\n\"image\": \"images/prototypes/footer8.png\",\n\"alt\" : \"Footer Eight\"\n},\n{\n\"id\": 9,\n\"name\": \"Footer Nine\",\n\"image\": \"images/prototypes/footer9.png\",\n\"alt\" : \"Footer Nine\"\n},\n{\n\"id\": 10,\n\"name\": \"Footer Ten\",\n\"image\": \"images/prototypes/footer10.png\",\n\"alt\" : \"Footer Ten\"\n}\n]', '[{\n                    \"id\": 10,\n                    \"name\": \"Second Ad Section\",\n                    \"order\": 1,\n                    \"file_name\": \"sec_ad_banner\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/sec_ad_section.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/sec_ad_section-cross.jpg\",\n                    \"alt\": \"Second Ad Section\"\n                }, {\n                    \"id\": 5,\n                    \"name\": \"Categories\",\n                    \"order\": 2,\n                    \"file_name\": \"categories\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/categories.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/categories-cross.jpg\",\n                    \"alt\": \"Categories\"\n                }, {\n                    \"id\": 1,\n                    \"name\": \"Banner Section\",\n                    \"order\": 3,\n                    \"file_name\": \"banner_section\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/banner_section.jpg\",\n                    \"alt\": \"Banner Section\"\n                }, {\n                    \"id\": 9,\n                    \"name\": \"Top Selling\",\n                    \"order\": 4,\n                    \"file_name\": \"top\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/top.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/top-cross.jpg\",\n                    \"alt\": \"Top Selling\"\n                }, {\n                    \"id\": 8,\n                    \"name\": \"Newest Product Section\",\n                    \"order\": 5,\n                    \"file_name\": \"newest_product\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/newest_product.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/newest_product-cross.jpg\",\n                    \"alt\": \"Newest Product Section\"\n                }, {\n                    \"id\": 11,\n                    \"name\": \"Tab Products View\",\n                    \"order\": 6,\n                    \"file_name\": \"tab\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/tab.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/tab-cross.jpg\",\n                    \"alt\": \"Tab Products View\"\n                }, {\n                    \"id\": 3,\n                    \"name\": \"Special Products Section\",\n                    \"order\": 7,\n                    \"file_name\": \"special\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/special_product.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/special_product-cross.jpg\",\n                    \"alt\": \"Special Products Section\"\n                }, {\n                    \"id\": 2,\n                    \"name\": \"Flash Sale Section\",\n                    \"order\": 8,\n                    \"file_name\": \"flash_sale_section\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/flash_sale_section.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/flash_sale_section-cross.jpg\",\n                    \"alt\": \"Flash Sale Section\"\n                }, {\n                    \"id\": 4,\n                    \"name\": \"Ad Section\",\n                    \"order\": 9,\n                    \"file_name\": \"ad_banner_section\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/ad_banner_section.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/ad_banner_section-cross.jpg\",\n                    \"alt\": \"Ad Section\"\n                }, {\n                    \"id\": 6,\n                    \"name\": \"Blog Section\",\n                    \"order\": 10,\n                    \"file_name\": \"blog_section\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/blog_section.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/blog_section-cross.jpg\",\n                    \"alt\": \"Blog Section\"\n                }, {\n                    \"id\": 12,\n                    \"name\": \"Banner 2 Section\",\n                    \"order\": 11,\n                    \"file_name\": \"banner_two_section\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/sec_ad_section.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/sec_ad_section-cross.jpg\",\n                    \"alt\": \"Banner 2 Section\"\n                }, {\n                    \"id\": 7,\n                    \"name\": \"Info Boxes\",\n                    \"order\": 12,\n                    \"file_name\": \"info_boxes\",\n                    \"status\": 1,\n                    \"image\": \"images\\/prototypes\\/info_boxes.jpg\",\n                    \"disabled_image\": \"images\\/prototypes\\/info_boxes-cross.jpg\",\n                    \"alt\": \"Info Boxes\"\n                }]\n                ', '[      {         \"id\":1,       \"name\":\"Cart One\"    },    {         \"id\":2,       \"name\":\"Cart Two\"    }     ]', '[      {         \"id\":1,       \"name\":\"News One\"    },    {         \"id\":2,       \"name\":\"News Two\"    }     ]', '[  \n{  \n\"id\":1,\n\"name\":\"Product Detail Page One\"\n},\n{  \n\"id\":2,\n\"name\":\"Product Detail Page Two\"\n},\n{  \n\"id\":3,\n\"name\":\"Product Detail Page Three\"\n},\n{  \n\"id\":4,\n\"name\":\"Product Detail Page Four\"\n},\n{  \n\"id\":5,\n\"name\":\"Product Detail Page Five\"\n},\n{  \n\"id\":6,\n\"name\":\"Product Detail Page Six\"\n}\n\n]', '[      {         \"id\":1,       \"name\":\"Shop Page One\"    },    {         \"id\":2,       \"name\":\"Shop Page Two\"    },    {         \"id\":3,       \"name\":\"Shop Page Three\"    },    {         \"id\":4,       \"name\":\"Shop Page Four\"    },    {         \"id\":5,       \"name\":\"Shop Page Five\"    }     ]', '[      {         \"id\":1,       \"name\":\"Contact Page One\"    },    {         \"id\":2,       \"name\":\"Contact Page Two\"    } ]', '[      {         \"id\":1,       \"name\":\"Login Page One\"    },    {         \"id\":2,       \"name\":\"Login Page Two\"    } ]', '[ { \"id\":1, \"name\":\"Transition Zoomin\" }, { \"id\":2, \"name\":\"Transition Flashing\" }, { \"id\":3, \"name\":\"Transition Shine\" }, { \"id\":4, \"name\":\"Transition Circle\" }, { \"id\":5, \"name\":\"Transition Opacity\" } ]', '[ { \"id\": 1, \"name\": \"Banner One\", \"image\": \"images/prototypes/banner1.jpg\", \"alt\": \"Banner One\" }, { \"id\": 2, \"name\": \"Banner Two\", \"image\": \"images/prototypes/banner2.jpg\", \"alt\": \"Banner Two\" }, { \"id\": 3, \"name\": \"Banner Three\", \"image\": \"images/prototypes/banner3.jpg\", \"alt\": \"Banner Three\" }, { \"id\": 4, \"name\": \"Banner Four\", \"image\": \"images/prototypes/banner4.jpg\", \"alt\": \"Banner Four\" }, { \"id\": 5, \"name\": \"Banner Five\", \"image\": \"images/prototypes/banner5.jpg\", \"alt\": \"Banner Five\" }, { \"id\": 6, \"name\": \"Banner Six\", \"image\": \"images/prototypes/banner6.jpg\", \"alt\": \"Banner Six\" }, { \"id\": 7, \"name\": \"Banner Seven\", \"image\": \"images/prototypes/banner7.jpg\", \"alt\": \"Banner Seven\" }, { \"id\": 8, \"name\": \"Banner Eight\", \"image\": \"images/prototypes/banner8.jpg\", \"alt\": \"Banner Eight\" }, { \"id\": 9, \"name\": \"Banner Nine\", \"image\": \"images/prototypes/banner9.jpg\", \"alt\": \"Banner Nine\" }, { \"id\": 10, \"name\": \"Banner Ten\", \"image\": \"images/prototypes/banner10.jpg\", \"alt\": \"Banner Ten\" }, { \"id\": 11, \"name\": \"Banner Eleven\", \"image\": \"images/prototypes/banner11.jpg\", \"alt\": \"Banner Eleven\" }, { \"id\": 12, \"name\": \"Banner Twelve\", \"image\": \"images/prototypes/banner12.jpg\", \"alt\": \"Banner Twelve\" }, { \"id\": 13, \"name\": \"Banner Thirteen\", \"image\": \"images/prototypes/banner13.jpg\", \"alt\": \"Banner Thirteen\" }, { \"id\": 14, \"name\": \"Banner Fourteen\", \"image\": \"images/prototypes/banner14.jpg\", \"alt\": \"Banner Fourteen\" }, { \"id\": 15, \"name\": \"Banner Fifteen\", \"image\": \"images/prototypes/banner15.jpg\", \"alt\": \"Banner Fifteen\" }, { \"id\": 16, \"name\": \"Banner Sixteen\", \"image\": \"images/prototypes/banner16.jpg\", \"alt\": \"Banner Sixteen\" }, { \"id\": 17, \"name\": \"Banner Seventeen\", \"image\": \"images/prototypes/banner17.jpg\", \"alt\": \"Banner Seventeen\" }, { \"id\": 18, \"name\": \"Banner Eighteen\", \"image\": \"images/prototypes/banner18.jpg\", \"alt\": \"Banner Eighteen\" }, { \"id\": 19, \"name\": \"Banner Nineteen\", \"image\": \"images/prototypes/banner19.jpg\", \"alt\": \"Banner Nineteen\" } ]');

-- --------------------------------------------------------

--
-- Table structure for table `geo_zones`
--

CREATE TABLE `geo_zones` (
  `geo_zone_id` int(11) NOT NULL,
  `geo_zone_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `geo_zone_description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `home_banners_id` bigint(20) UNSIGNED NOT NULL,
  `banner_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `text` text COLLATE utf8_unicode_ci,
  `image` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `http_call_record`
--

CREATE TABLE `http_call_record` (
  `id` int(11) NOT NULL,
  `device_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ping_time` datetime DEFAULT NULL,
  `difference_from_previous` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'XUF1110211.png', 1, NULL, NULL, NULL),
(4, '0S9Uj10711.png', 1, NULL, NULL, NULL),
(5, '49YbL10411.png', 1, NULL, NULL, NULL),
(121, 'xNG9i28309.png', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_categories`
--

CREATE TABLE `image_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_id` int(10) UNSIGNED NOT NULL,
  `image_type` enum('ACTUAL','THUMBNAIL','LARGE','MEDIUM') COLLATE utf8_unicode_ci NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `image_categories`
--

INSERT INTO `image_categories` (`id`, `image_id`, `image_type`, `height`, `width`, `path`, `created_at`, `updated_at`) VALUES
(334, 121, 'ACTUAL', 230, 219, 'images/media/2020/10/xNG9i28309.png', NULL, NULL),
(335, 121, 'THUMBNAIL', 150, 143, 'images/media/2020/10/thumbnail1603878802xNG9i28309.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inventory_ref_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `added_date` int(11) NOT NULL,
  `reference_code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `purchase_price` decimal(10,2) NOT NULL,
  `stock_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_detail`
--

CREATE TABLE `inventory_detail` (
  `inventory_ref_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `label_id` int(11) NOT NULL,
  `label_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`label_id`, `label_name`) VALUES
(1, 'I\'ve forgotten my password?'),
(2, 'Creating an account means you’re okay with shopify\'s Terms of Service, Privacy Policy'),
(872, 'Login with'),
(873, 'or'),
(874, 'Email'),
(875, 'Password'),
(876, 'Register'),
(877, 'Forgot Password'),
(878, 'Send'),
(879, 'About Us'),
(880, 'Categories'),
(881, 'Contact Us'),
(882, 'Name'),
(883, 'Your Messsage'),
(884, 'Please connect to the internet'),
(885, 'Recently Viewed'),
(886, 'Products are available.'),
(887, 'Top Seller'),
(888, 'Special Deals'),
(889, 'Most Liked'),
(890, 'All Categories'),
(891, 'Deals'),
(892, 'REMOVE'),
(893, 'Intro'),
(894, 'Skip Intro'),
(895, 'Got It!'),
(896, 'Order Detail'),
(897, 'Price Detail'),
(898, 'Total'),
(899, 'Sub Total'),
(900, 'Shipping'),
(901, 'Product Details'),
(902, 'New'),
(903, 'Out of Stock'),
(904, 'In Stock'),
(905, 'Add to Cart'),
(906, 'ADD TO CART'),
(907, 'Product Description'),
(908, 'Techincal details'),
(909, 'OFF'),
(910, 'No Products Found'),
(911, 'Reset Filters'),
(912, 'Search'),
(913, 'Main Categories'),
(914, 'Sub Categories'),
(915, 'Shipping method'),
(916, 'Thank You'),
(917, 'Thank you for shopping with us.'),
(918, 'My Orders'),
(919, 'Continue Shopping'),
(920, 'Favourite'),
(921, 'Your wish List is empty'),
(922, 'Continue Adding'),
(923, 'Explore'),
(924, 'Word Press Post Detail'),
(925, 'Go Back'),
(926, 'Top Sellers'),
(927, 'News'),
(928, 'Enter keyword'),
(929, 'Settings'),
(930, 'Shop'),
(931, 'Reset'),
(932, 'Select Language'),
(933, 'OUT OF STOCK'),
(934, 'Newest'),
(935, 'Refund Policy'),
(936, 'Privacy Policy'),
(937, 'Term and Services'),
(938, 'Skip'),
(939, 'Top Dishes'),
(940, 'Recipe of Day'),
(941, 'Food Categories'),
(942, 'Coupon Code'),
(943, 'Coupon Amount'),
(944, 'coupon code'),
(945, 'Coupon'),
(946, 'Note to the buyer'),
(947, 'Explore More'),
(948, 'All'),
(949, 'A - Z'),
(950, 'Z - A'),
(951, 'Price : high - low'),
(952, 'Price : low - high'),
(953, 'Special Products'),
(954, 'Sort Products'),
(955, 'Cancel'),
(956, 'most liked'),
(957, 'special'),
(958, 'top seller'),
(959, 'newest'),
(960, 'Likes'),
(961, 'My Account'),
(962, 'Mobile'),
(963, 'Date of Birth'),
(964, 'Update'),
(965, 'Current Password'),
(966, 'New Password'),
(967, 'Change Password'),
(968, 'Customer Orders'),
(969, 'Order Status'),
(970, 'Orders ID'),
(971, 'Product Price'),
(972, 'No. of Products'),
(973, 'Date'),
(974, 'Customer Address'),
(975, 'Please add your new shipping address for the futher processing of the your order'),
(976, 'Add new Address'),
(977, 'Create an Account'),
(978, 'First Name'),
(979, 'Last Name'),
(980, 'Already Memeber?'),
(981, 'Billing Info'),
(982, 'Address'),
(983, 'Phone'),
(984, 'Same as Shipping Address'),
(985, 'Next'),
(986, 'Order'),
(987, 'Billing Address'),
(988, 'Shipping Method'),
(989, 'Products'),
(990, 'SubTotal'),
(991, 'Products Price'),
(992, 'Tax'),
(993, 'Shipping Cost'),
(994, 'Order Notes'),
(995, 'Payment'),
(996, 'Card Number'),
(997, 'Expiration Date'),
(998, 'Expiration'),
(999, 'Error: invalid card number!'),
(1000, 'Error: invalid expiry date!'),
(1001, 'Error: invalid cvc number!'),
(1002, 'Continue'),
(1003, 'My Cart'),
(1004, 'Your cart is empty'),
(1005, 'continue shopping'),
(1006, 'Price'),
(1007, 'Quantity'),
(1008, 'by'),
(1009, 'View'),
(1010, 'Remove'),
(1011, 'Proceed'),
(1012, 'Shipping Address'),
(1013, 'Country'),
(1014, 'other'),
(1015, 'Zone'),
(1016, 'City'),
(1017, 'Post code'),
(1018, 'State'),
(1019, 'Update Address'),
(1020, 'Save Address'),
(1021, 'Login & Register'),
(1022, 'Please login or create an account for free'),
(1023, 'Log Out'),
(1024, 'My Wish List'),
(1025, 'Filters'),
(1026, 'Price Range'),
(1027, 'Close'),
(1028, 'Apply'),
(1029, 'Clear'),
(1030, 'Menu'),
(1031, 'Home'),
(1033, 'Creating an account means you’re okay with our'),
(1034, 'Login'),
(1035, 'Turn on/off Local Notifications'),
(1036, 'Turn on/off Notifications'),
(1037, 'Change Language'),
(1038, 'Official Website'),
(1039, 'Rate Us'),
(1040, 'Share'),
(1041, 'Edit Profile'),
(1042, 'A percentage discount for the entire cart'),
(1043, 'A fixed total discount for the entire cart'),
(1044, 'A fixed total discount for selected products only'),
(1045, 'A percentage discount for selected products only'),
(1047, 'Network Connected Reloading Data'),
(1048, 'Sort by'),
(1049, 'Flash Sale'),
(1050, 'ok'),
(1051, 'Number'),
(1052, 'Expire Month'),
(1053, 'Expire Year'),
(1054, 'Payment Method'),
(1055, 'Status'),
(1056, 'And'),
(1057, 'cccc'),
(1058, 'Shop More'),
(1059, 'Me'),
(1060, 'View All'),
(1061, 'Featured'),
(1062, 'Shop Now'),
(1063, 'New Arrivals'),
(1064, 'Sort'),
(1065, 'Help & Support'),
(1066, 'Select Currency'),
(1067, 'Your Price'),
(1068, 'Billing'),
(1069, 'Ship to a different address?'),
(1070, 'Method'),
(1071, 'Summary'),
(1072, 'Discount'),
(1073, 'Error in initialization, maybe PayPal isnt supported or something else'),
(1074, 'Alert'),
(1075, 'Your Wishlist is Empty'),
(1076, 'Press heart icon on products to add them in wishlist'),
(1077, 'Wishlist'),
(1078, 'All Items'),
(1079, 'Account Info'),
(1080, 'You Must Be Logged in to use this Feature!'),
(1081, 'Remove from Wishlist'),
(1082, 'Sign Up'),
(1083, 'Reset Password'),
(1084, 'Invalid email or password'),
(1085, 'Recent Searches'),
(1086, 'Add to Wishlist'),
(1087, 'Discover Latest Trends'),
(1088, 'Add To My Wishlist'),
(1089, 'Start Shoping'),
(1090, 'A Smart Shopping Experience'),
(1091, 'Addresses'),
(1092, 'Account'),
(1093, 'DETAILS'),
(1094, 'Dark Mode'),
(1095, 'Enter a description'),
(1096, 'Grocery Store'),
(1097, 'Post Comment'),
(1098, 'Rate and write a review'),
(1099, 'Ratings & Reviews'),
(1100, 'Write a review'),
(1101, 'Your Rating'),
(1102, 'rating'),
(1103, 'rating and review'),
(1104, 'Coupon Codes List'),
(1105, 'Custom Orders'),
(1106, 'Ecommerce'),
(1107, 'Featured Products'),
(1108, 'House Hold 1'),
(1109, 'Newest Products'),
(1110, 'On Sale Products'),
(1111, 'Braintree'),
(1112, 'Hyperpay'),
(1113, 'Instamojo'),
(1114, 'PayTm'),
(1115, 'Paypal'),
(1116, 'Razor Pay'),
(1117, 'Stripe');

-- --------------------------------------------------------

--
-- Table structure for table `label_value`
--

CREATE TABLE `label_value` (
  `label_value_id` int(11) NOT NULL,
  `label_value` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `label_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `label_value`
--

INSERT INTO `label_value` (`label_value_id`, `label_value`, `language_id`, `label_id`) VALUES
(1297, 'Home', 1, 1031),
(1298, 'Menu', 1, 1030),
(1299, 'Clear', 1, 1029),
(1300, 'Apply', 1, 1028),
(1301, 'Close', 1, 1027),
(1302, 'Price Range', 1, 1026),
(1303, 'Filters', 1, 1025),
(1304, 'My Wish List', 1, 1024),
(1305, 'Log Out', 1, 1023),
(1306, 'Please login or create an account for free', 1, 1022),
(1307, 'login & Register', 1, 1021),
(1308, 'Save Address', 1, 1020),
(1309, 'State', 1, 1018),
(1310, 'Update Address', 1, 1019),
(1311, 'Post code', 1, 1017),
(1312, 'City', 1, 1016),
(1313, 'Zone', 1, 1015),
(1314, 'other', 1, 1014),
(1315, 'Country', 1, 1013),
(1316, 'Shipping Address', 1, 1012),
(1317, 'Proceed', 1, 1011),
(1318, 'Remove', 1, 1010),
(1319, 'by', 1, 1008),
(1320, 'View', 1, 1009),
(1321, 'Quantity', 1, 1007),
(1322, 'Price', 1, 1006),
(1323, 'continue shopping', 1, 1005),
(1324, 'Your cart is empty', 1, 1004),
(1325, 'My Cart', 1, 1003),
(1326, 'Continue', 1, 1002),
(1327, 'Error: invalid cvc number!', 1, 1001),
(1328, 'Error: invalid expiry date!', 1, 1000),
(1329, 'Error: invalid card number!', 1, 999),
(1330, 'Expiration', 1, 998),
(1331, 'Expiration Date', 1, 997),
(1332, 'Card Number', 1, 996),
(1333, 'Payment', 1, 995),
(1334, 'Order Notes', 1, 994),
(1335, 'Shipping Cost', 1, 993),
(1336, 'Tax', 1, 992),
(1337, 'Products Price', 1, 991),
(1338, 'SubTotal', 1, 990),
(1339, 'Products', 1, 989),
(1340, 'Shipping Method', 1, 988),
(1341, 'Billing Address', 1, 987),
(1342, 'Order', 1, 986),
(1343, 'Next', 1, 985),
(1344, 'Same as Shipping Address', 1, 984),
(1345, 'Billing Info', 1, 981),
(1346, 'Address', 1, 982),
(1347, 'Phone', 1, 983),
(1348, 'Already Memeber?', 1, 980),
(1349, 'Last Name', 1, 979),
(1350, 'First Name', 1, 978),
(1351, 'Create an Account', 1, 977),
(1352, 'Add new Address', 1, 976),
(1353, 'Please add your new shipping address for the futher processing of the your order', 1, 975),
(1354, 'Order Status', 1, 969),
(1355, 'Orders ID', 1, 970),
(1356, 'Product Price', 1, 971),
(1357, 'No. of Products', 1, 972),
(1358, 'Date', 1, 973),
(1359, 'Customer Address', 1, 974),
(1360, 'Customer Orders', 1, 968),
(1361, 'Change Password', 1, 967),
(1362, 'New Password', 1, 966),
(1363, 'Current Password', 1, 965),
(1364, 'Update', 1, 964),
(1365, 'Date of Birth', 1, 963),
(1366, 'Mobile', 1, 962),
(1367, 'My Account', 1, 961),
(1368, 'Likes', 1, 960),
(1369, 'Newest', 1, 959),
(1370, 'Top Seller', 1, 958),
(1371, 'Special', 1, 957),
(1372, 'Most Liked', 1, 956),
(1373, 'Cancel', 1, 955),
(1374, 'Sort Products', 1, 954),
(1375, 'Special Products', 1, 953),
(1376, 'Price : low - high', 1, 952),
(1377, 'Price : high - low', 1, 951),
(1378, 'Z - A', 1, 950),
(1379, 'A - Z', 1, 949),
(1380, 'All', 1, 948),
(1381, 'Explore More', 1, 947),
(1382, 'Note to the buyer', 1, 946),
(1383, 'Coupon', 1, 945),
(1384, 'coupon code', 1, 944),
(1385, 'Coupon Amount', 1, 943),
(1386, 'Coupon Code', 1, 942),
(1387, 'Food Categories', 1, 941),
(1388, 'Recipe of Day', 1, 940),
(1389, 'Top Dishes', 1, 939),
(1390, 'Skip', 1, 938),
(1391, 'Term and Services', 1, 937),
(1392, 'Privacy Policy', 1, 936),
(1393, 'Refund Policy', 1, 935),
(1394, 'Newest', 1, 934),
(1395, 'OUT OF STOCK', 1, 933),
(1396, 'Select Language', 1, 932),
(1397, 'Reset', 1, 931),
(1398, 'Shop', 1, 930),
(1399, 'Settings', 1, 929),
(1400, 'Enter keyword', 1, 928),
(1401, 'News', 1, 927),
(1402, 'Top Sellers', 1, 926),
(1403, 'Go Back', 1, 925),
(1404, 'Word Press Post Detail', 1, 924),
(1405, 'Explore', 1, 923),
(1406, 'Continue Adding', 1, 922),
(1407, 'Your wish List is empty', 1, 921),
(1408, 'Favourite', 1, 920),
(1409, 'Continue Shopping', 1, 919),
(1410, 'My Orders', 1, 918),
(1411, 'Thank you for shopping with us.', 1, 917),
(1412, 'Thank You', 1, 916),
(1413, 'Shipping method', 1, 915),
(1414, 'Sub Categories', 1, 914),
(1415, 'Main Categories', 1, 913),
(1416, 'Search', 1, 912),
(1417, 'Reset Filters', 1, 911),
(1418, 'No Products Found', 1, 910),
(1419, 'OFF', 1, 909),
(1420, 'Techincal details', 1, 908),
(1421, 'Product Description', 1, 907),
(1422, 'ADD TO CART', 1, 906),
(1423, 'Add to Cart', 1, 905),
(1424, 'In Stock', 1, 904),
(1425, 'Out of Stock', 1, 903),
(1426, 'New', 1, 902),
(1427, 'Product Details', 1, 901),
(1428, 'Shipping', 1, 900),
(1429, 'Sub Total', 1, 899),
(1430, 'Total', 1, 898),
(1431, 'Price Detail', 1, 897),
(1432, 'Order Detail', 1, 896),
(1433, 'Got It!', 1, 895),
(1434, 'Skip Intro', 1, 894),
(1435, 'Intro', 1, 893),
(1436, 'REMOVE', 1, 892),
(1437, 'Deals', 1, 891),
(1438, 'All Categories', 1, 890),
(1439, 'Most Liked', 1, 889),
(1440, 'Special Deals', 1, 888),
(1441, 'Top Seller', 1, 887),
(1442, 'Products are available.', 1, 886),
(1443, 'Recently Viewed', 1, 885),
(1444, 'Please connect to the internet', 1, 884),
(1445, 'Contact Us', 1, 881),
(1446, 'Name', 1, 882),
(1447, 'Your Message', 1, 883),
(1448, 'Categories', 1, 880),
(1449, 'About Us', 1, 879),
(1450, 'Send', 1, 878),
(1451, 'Forgot Password', 1, 877),
(1452, 'Register', 1, 876),
(1453, 'Password', 1, 875),
(1454, 'Email', 1, 874),
(1455, 'or', 1, 873),
(1456, 'Login with', 1, 872),
(1457, 'Creating an account means you\'re okay with shopify\'s Terms of Service, Privacy Policy', 1, 2),
(1458, 'I\'ve forgotten my password?', 1, 1),
(1459, NULL, 1, NULL),
(1462, 'Creating an account means you’re okay with our', 1, 1033),
(1465, 'Login', 1, 1034),
(1468, 'Turn on/off Local Notifications', 1, 1035),
(1471, 'Turn on/off Notifications', 1, 1036),
(1474, 'Change Language', 1, 1037),
(1477, 'Official Website', 1, 1038),
(1480, 'Rate Us', 1, 1039),
(1483, 'Share', 1, 1040),
(1486, 'Edit Profile', 1, 1041),
(1489, 'A percentage discount for the entire cart', 1, 1042),
(1492, 'A fixed total discount for the entire cart', 1, 1043),
(1495, 'A fixed total discount for selected products only', 1, 1044),
(1498, 'A percentage discount for selected products only', 1, 1045),
(1501, 'Network Connected Reloading Data', 1, 1047),
(1503, 'Sort by', 1, 1048),
(1505, 'Flash Sale', 1, 1049),
(1507, 'ok', 1, 1050),
(1509, 'Number', 1, 1051),
(1511, 'Expire Month', 1, 1052),
(1513, 'Expire Year', 1, 1053),
(1515, 'Payment Method', 1, 1054),
(1517, 'Status', 1, 1055),
(1519, 'And', 1, 1056),
(1520, 'نسيت كلمة المرور الخاصة بي؟', 4, 1),
(1521, 'إن إنشاء حساب يعني موافقتك على شروط الخدمة وسياسة الخصوصية', 4, 2),
(1522, 'تسجيل الدخول مع', 4, 872),
(1523, 'أو', 4, 873),
(1524, 'البريد الإلكتروني', 4, 874),
(1525, 'كلمه السر', 4, 875),
(1526, 'تسجيل', 4, 876),
(1527, 'هل نسيت كلمة المرور', 4, 877),
(1528, 'إرسال', 4, 878),
(1529, 'معلومات عنا', 4, 879),
(1530, 'التصنيفات', 4, 880),
(1531, 'اتصل بنا', 4, 881),
(1532, 'اسم', 4, 882),
(1533, 'رسالتك', 4, 883),
(1534, 'يرجى الاتصال بالإنترنت', 4, 884),
(1535, 'شوهدت مؤخرا', 4, 885),
(1536, 'المنتجات المتاحة.', 4, 886),
(1537, 'أعلى بائع', 4, 887),
(1538, 'صفقة خاصة', 4, 888),
(1539, 'الأكثر إعجابا', 4, 889),
(1540, 'جميع الفئات', 4, 890),
(1541, 'صفقات', 4, 891),
(1542, 'إزالة', 4, 892),
(1543, 'مقدمة', 4, 893),
(1544, 'تخطي المقدمة', 4, 894),
(1545, 'فهمتك!', 4, 895),
(1546, 'تفاصيل الطلب', 4, 896),
(1547, 'سعر التفاصيل', 4, 897),
(1548, 'مجموع', 4, 898),
(1549, 'المجموع الفرعي', 4, 899),
(1550, 'الشحن', 4, 900),
(1551, 'تفاصيل المنتج', 4, 901),
(1552, 'جديد', 4, 902),
(1553, 'إنتهى من المخزن', 4, 903),
(1554, 'في المخزن', 4, 904),
(1555, 'أضف إلى السلة', 4, 905),
(1556, 'أضف إلى السلة', 4, 906),
(1557, 'وصف المنتج', 4, 907),
(1558, 'تفاصيل تقنية', 4, 908),
(1559, 'إيقاف', 4, 909),
(1560, 'لا توجد منتجات', 4, 910),
(1561, 'إعادة تعيين المرشحات', 4, 911),
(1562, 'بحث', 4, 912),
(1563, 'الفئات الرئيسية', 4, 913),
(1564, 'الفئات الفرعية', 4, 914),
(1565, 'طريقة الشحن', 4, 915),
(1566, 'شكرا جزيلا', 4, 916),
(1567, 'شكرا للتسوق معنا.', 4, 917),
(1568, 'طلباتي', 4, 918),
(1569, 'مواصلة التسوق', 4, 919),
(1570, NULL, 4, NULL),
(1571, 'مفضل', 4, 920),
(1572, 'قائمة رغباتك فارغة', 4, 921),
(1573, 'متابعة الإضافة', 4, 922),
(1574, 'يكتشف', 4, 923),
(1575, 'وورد بوست التفاصيل', 4, 924),
(1576, 'عد', 4, 925),
(1577, 'أفضل البائعين', 4, 926),
(1578, 'أخبار', 4, 927),
(1579, 'أدخل الكلمة المفتاحية', 4, 928),
(1580, 'الإعدادات', 4, 929),
(1581, 'متجر', 4, 930),
(1582, 'إعادة تعيين', 4, 931),
(1583, 'اختار اللغة', 4, 932),
(1584, 'إنتهى من المخزن', 4, 933),
(1585, 'الأحدث', 4, 934),
(1586, 'سياسة الاسترجاع', 4, 935),
(1587, 'سياسة خاصة', 4, 936),
(1588, 'مصطلح والخدمات', 4, 937),
(1589, 'تخطى', 4, 938),
(1590, 'أطباق الأعلى', 4, 939),
(1591, 'وصفة اليوم', 4, 940),
(1592, 'فئات الغذاء', 4, 941),
(1593, 'رمز الكوبون', 4, 942),
(1594, 'مبلغ القسيمة', 4, 943),
(1595, 'رمز الكوبون', 4, 944),
(1596, 'كوبون', 4, 945),
(1597, 'ملاحظة للمشتري', 4, 946),
(1598, 'استكشاف المزيد', 4, 947),
(1599, 'الكل', 4, 948),
(1600, 'أ - ي', 4, 949),
(1601, 'ي - أ', 4, 950),
(1602, 'السعر مرتفع منخفض', 4, 951),
(1603, 'سعر منخفض مرتفع', 4, 952),
(1604, 'المنتجات الخاصة', 4, 953),
(1605, 'فرز المنتجات', 4, 954),
(1606, 'إلغاء', 4, 955),
(1607, 'الأكثر إعجابا', 4, 956),
(1608, 'خاص', 4, 957),
(1609, 'أعلى بائع', 4, 958),
(1610, 'الأحدث', 4, 959),
(1611, 'الإعجابات', 4, 960),
(1612, 'حسابي', 4, 961),
(1613, 'التليفون المحمول', 4, 962),
(1614, 'تاريخ الولادة', 4, 963),
(1615, 'تحديث', 4, 964),
(1616, 'كلمة المرور الحالية', 4, 965),
(1617, 'كلمة سر جديدة', 4, 966),
(1618, 'تغيير كلمة المرور', 4, 967),
(1619, 'طلبات العملاء', 4, 968),
(1620, 'حالة الطلب', 4, 969),
(1621, 'معرف الطلبات', 4, 970),
(1622, 'سعر المنتج', 4, 971),
(1623, 'عدد المنتجات', 4, 972),
(1624, 'تاريخ', 4, 973),
(1625, 'عنوان العميل', 4, 974),
(1626, 'يرجى إضافة عنوان الشحن الجديد لمزيد من المعالجة لطلبك', 4, 975),
(1627, 'إضافة عنوان جديد', 4, 976),
(1628, 'انشئ حساب', 4, 977),
(1629, 'الاسم الاول', 4, 978),
(1630, 'الكنية', 4, 979),
(1631, 'هل أنت عضو بالفعل؟', 4, 980),
(1632, 'معلومات الفواتير', 4, 981),
(1633, 'عنوان', 4, 982),
(1634, 'هاتف', 4, 983),
(1635, 'نفس عنوان الشحن', 4, 984),
(1636, 'التالى', 4, 985),
(1637, 'طلب', 4, 986),
(1638, 'عنوان وصول الفواتير', 4, 987),
(1639, 'طريقة الشحن', 4, 988),
(1640, 'منتجات', 4, 989),
(1641, 'حاصل الجمع', 4, 990),
(1642, 'سعر المنتجات', 4, 991),
(1643, 'ضريبة', 4, 992),
(1644, 'تكلفة الشحن', 4, 993),
(1645, 'ترتيب ملاحظات', 4, 994),
(1646, 'دفع', 4, 995),
(1647, 'رقم البطاقة', 4, 996),
(1648, 'تاريخ الإنتهاء', 4, 997),
(1649, 'انتهاء الصلاحية', 4, 998),
(1650, 'خطأ: رقم البطاقة غير صالح!', 4, 999),
(1651, 'خطأ: تاريخ انتهاء الصلاحية غير صحيح!', 4, 1000),
(1652, 'خطأ: رقم cvc غير صالح!', 4, 1001),
(1653, 'استمر', 4, 1002),
(1654, 'سلتي', 4, 1003),
(1655, 'عربة التسوق فارغة', 4, 1004),
(1656, 'مواصلة التسوق', 4, 1005),
(1657, 'السعر', 4, 1006),
(1658, 'كمية', 4, 1007),
(1659, 'بواسطة', 4, 1008),
(1660, 'رأي', 4, 1009),
(1661, 'إزالة', 4, 1010),
(1662, 'تقدم', 4, 1011),
(1663, 'عنوان الشحن', 4, 1012),
(1664, 'بلد', 4, 1013),
(1665, 'آخر', 4, 1014),
(1666, 'منطقة', 4, 1015),
(1667, 'مدينة', 4, 1016),
(1668, 'الرمز البريدي', 4, 1017),
(1669, 'حالة', 4, 1018),
(1670, 'تحديث العنوان', 4, 1019),
(1671, 'حفظ العنوان', 4, 1020),
(1672, 'دخولتسجيل', 4, 1021),
(1673, 'يرجى تسجيل الدخول أو إنشاء حساب مجانا', 4, 1022),
(1674, 'تسجيل خروج', 4, 1023),
(1675, 'قائمة امنياتي', 4, 1024),
(1676, 'مرشحات', 4, 1025),
(1677, 'نطاق السعر', 4, 1026),
(1678, 'قريب', 4, 1027),
(1679, 'تطبيق', 4, 1028),
(1680, 'واضح', 4, 1029),
(1681, 'قائمة طعام', 4, 1030),
(1682, 'الصفحة الرئيسية', 4, 1031),
(1683, 'إن إنشاء حساب يعني أنك بخير من خلال موقعنا', 4, 1033),
(1684, 'تسجيل الدخول', 4, 1034),
(1685, 'تشغيل / إيقاف الإشعارات', 4, 1035),
(1686, 'تشغيل / إيقاف الإشعارات', 4, 1036),
(1687, 'تغيير اللغة', 4, 1037),
(1688, 'الموقع الرسمي', 4, 1038),
(1689, 'قيمنا', 4, 1039),
(1690, 'شارك', 4, 1040),
(1691, 'تعديل الملف الشخصي', 4, 1041),
(1692, 'خصم النسبة المئوية للسلة بأكملها', 4, 1042),
(1693, 'خصم إجمالي ثابت للعربة بأكملها', 4, 1043),
(1694, 'خصم إجمالي ثابت للمنتجات المحددة فقط', 4, 1044),
(1695, 'خصم النسبة المئوية للمنتجات المختارة فقط', 4, 1045),
(1696, 'شبكة متصلة إعادة تحميل البيانات', 4, 1047),
(1697, 'صنف حسب', 4, 1048),
(1698, 'بيع مفاجئ', 4, 1049),
(1699, 'حسنا', 4, 1050),
(1700, 'رقم', 4, 1051),
(1701, 'انتهاء الشهر', 4, 1052),
(1702, 'انتهاء السنة', 4, 1053),
(1703, 'طريقة الدفع او السداد', 4, 1054),
(1704, 'الحالة', 4, 1055),
(1705, 'و', 4, 1056),
(1706, 'cccc', 1, 1057),
(1707, 'cccc', 4, 1057),
(1708, 'Shop More', 1, 1058),
(1709, 'عربي', 4, 1058),
(1710, 'Discount', 1, 1072),
(1711, 'خصم', 4, 1072),
(1712, 'Error in initialization, maybe PayPal isnt supported or something else', 1, 1073),
(1713, 'خطأ في التهيئة ، ربما لا يتم دعم PayPal أو أي شيء آخر', 4, 1073),
(1714, 'Alert', 1, 1074),
(1715, 'إنذار', 4, 1074),
(1716, 'Your Wishlist is Empty', 1, 1075),
(1717, 'قائمة رغباتك فارغة', 4, 1075),
(1718, 'Press heart icon on products to add them in wishlist', 1, 1076),
(1719, 'اضغط على أيقونة القلب على المنتجات لإضافتها إلى قائمة الرغبات', 4, 1076),
(1720, 'Wishlist', 1, 1077),
(1721, 'قائمة الرغبات', 4, 1077),
(1722, 'All Items', 1, 1078),
(1723, 'كل الاشياء', 4, 1078),
(1724, 'Account Info', 1, 1079),
(1725, 'معلومات الحساب', 4, 1079),
(1726, 'You Must Be Logged in to use this Feature!', 1, 1080),
(1727, 'يجب عليك تسجيل الدخول لاستخدام هذه الميزة!', 4, 1080),
(1728, 'Remove from Wishlist', 1, 1081),
(1729, 'إزالة من قائمة الرغبات', 4, 1081),
(1730, 'Sign Up', 1, 1082),
(1731, 'سجل', 4, 1082),
(1732, 'Reset Password', 1, 1083),
(1733, 'إعادة تعيين كلمة المرور', 4, 1083),
(1734, 'Invalid email or password', 1, 1084),
(1735, 'البريد الإلكتروني أو كلمة السر خاطئة', 4, 1084),
(1736, 'Recent Searches', 1, 1085),
(1737, 'عمليات البحث الأخيرة', 4, 1085),
(1738, 'Add to Wishlist', 1, 1086),
(1739, 'أضف إلى قائمة الامنيات', 4, 1086),
(1740, 'Discover Latest Trends', 1, 1087),
(1741, 'اكتشف أحدث الاتجاهات', 4, 1087),
(1742, 'Add To My Wishlist', 1, 1088),
(1743, 'أضف إلى قائمة أمنياتي', 4, 1088),
(1744, 'Start Shoping', 1, 1089),
(1745, 'ابدأ التسوق', 4, 1089),
(1746, 'A Smart Shopping Experience', 1, 1090),
(1747, 'تجربة تسوق ذكية', 4, 1090),
(1748, 'Addresses', 1, 1091),
(1749, 'عناوين', 4, 1091),
(1750, 'Account', 1, 1092),
(1751, 'الحساب', 4, 1092),
(1752, 'DETAILS', 1, 1093),
(1753, 'تفاصيل', 4, 1093),
(1754, 'Dark Mode', 1, 1094),
(1755, 'الوضع الداكن', 4, 1094),
(1756, 'Enter a description', 1, 1095),
(1757, 'أدخل وصفًا', 4, 1095),
(1758, 'Grocery Store', 1, 1096),
(1759, 'بقالة', 4, 1096),
(1760, 'Post Comment', 1, 1097),
(1761, 'أضف تعليقا', 4, 1097),
(1762, 'Rate and write a review', 1, 1098),
(1763, 'تقييم وكتابة مراجعة', 4, 1098),
(1764, 'Ratings & Reviews', 1, 1099),
(1765, 'التقييمات والمراجعات', 4, 1099),
(1766, 'Write a review', 1, 1100),
(1767, 'أكتب مراجعة', 4, 1100),
(1768, 'Your Rating', 1, 1101),
(1769, 'تقييمك', 4, 1101),
(1770, 'rating', 1, 1102),
(1771, 'تقييم', 4, 1102),
(1772, 'rating and review', 1, 1103),
(1773, 'تصنيف ومراجعة', 4, 1103),
(1774, 'Coupon Codes List', 1, 1104),
(1775, 'قائمة رموز القسيمة', 4, 1104),
(1776, 'Custom Orders', 1, 1105),
(1777, 'أوامر مخصصة', 4, 1105),
(1778, 'Ecommerce', 1, 1106),
(1779, 'التجارة الإلكترونية', 4, 1106),
(1780, 'Featured Products', 1, 1107),
(1781, 'منتجات مميزة', 4, 1107),
(1782, 'House Hold 1', 1, 1108),
(1783, 'المنزل عقد 1', 4, 1108),
(1784, 'Newest Products', 1, 1109),
(1785, 'أحدث المنتجات', 4, 1109),
(1786, 'On Sale Products', 1, 1110),
(1787, 'المنتجات المعروضة للبيع', 4, 1110),
(1788, 'Braintree', 1, 1111),
(1789, 'برينتري', 4, 1111),
(1790, 'Hyperpay', 1, 1112),
(1791, 'Hyperpay', 4, 1112),
(1792, 'Instamojo', 1, 1113),
(1793, 'Instamojo', 4, 1113),
(1794, 'PayTm', 1, 1114),
(1795, 'PayTm', 4, 1114),
(1796, 'Paypal', 1, 1115),
(1797, 'باي بال', 4, 1115),
(1798, 'Razor Pay', 1, 1116),
(1799, 'الحلاقة الدفع', 4, 1116),
(1800, 'Stripe', 1, 1117),
(1801, 'شريط', 4, 1117),
(1802, 'Me', 1, 1059),
(1803, 'أنا', 4, 1059),
(1804, 'View All', 1, 1060),
(1805, 'عرض الكل', 4, 1060),
(1806, 'Featured', 1, 1061),
(1807, 'متميز', 4, 1061),
(1808, 'Shop Now', 1, 1062),
(1809, 'تسوق الآن', 4, 1062),
(1810, 'New Arrivals', 1, 1063),
(1811, 'الوافدون الجدد', 4, 1063),
(1812, 'Sort', 1, 1064),
(1813, 'فرز', 4, 1064),
(1814, 'Help & Support', 1, 1065),
(1815, 'ساعد لدعم', 4, 1065),
(1816, 'Select Currency', 1, 1066),
(1817, 'اختر العملة', 4, 1066),
(1818, 'Your Price', 1, 1067),
(1819, 'السعر الخاص', 4, 1067),
(1820, 'Billing', 1, 1068),
(1821, 'الفواتير', 4, 1068),
(1822, 'Ship to a different address?', 1, 1069),
(1823, 'هل تريد الشحن إلى عنوان مختلف؟', 4, 1069),
(1824, 'Method', 1, 1070),
(1825, 'طريقة', 4, 1070),
(1826, 'Summary', 1, 1071),
(1827, 'ملخص', 4, 1071);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `languages_id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `directory` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `direction` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_default` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`languages_id`, `name`, `code`, `image`, `directory`, `sort_order`, `direction`, `status`, `is_default`) VALUES
(1, 'English', 'en', '118', NULL, 1, 'ltr', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `liked_products`
--

CREATE TABLE `liked_products` (
  `like_id` int(11) NOT NULL,
  `liked_products_id` int(11) NOT NULL,
  `liked_customers_id` int(11) NOT NULL,
  `date_liked` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manage_min_max`
--

CREATE TABLE `manage_min_max` (
  `min_max_id` int(11) NOT NULL,
  `min_level` int(11) NOT NULL,
  `max_level` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `inventory_ref_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manage_role`
--

CREATE TABLE `manage_role` (
  `manage_role_id` int(11) NOT NULL,
  `user_types_id` tinyint(1) NOT NULL DEFAULT '0',
  `dashboard_view` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_view` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_create` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_update` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_delete` tinyint(1) NOT NULL DEFAULT '0',
  `categories_view` tinyint(1) NOT NULL DEFAULT '0',
  `categories_create` tinyint(1) NOT NULL DEFAULT '0',
  `categories_update` tinyint(1) NOT NULL DEFAULT '0',
  `categories_delete` tinyint(1) NOT NULL DEFAULT '0',
  `products_view` tinyint(1) NOT NULL DEFAULT '0',
  `products_create` tinyint(1) NOT NULL DEFAULT '0',
  `products_update` tinyint(1) NOT NULL DEFAULT '0',
  `products_delete` tinyint(1) NOT NULL DEFAULT '0',
  `news_view` tinyint(1) NOT NULL DEFAULT '0',
  `news_create` tinyint(1) NOT NULL DEFAULT '0',
  `news_update` tinyint(1) NOT NULL DEFAULT '0',
  `news_delete` tinyint(1) NOT NULL DEFAULT '0',
  `customers_view` tinyint(1) NOT NULL DEFAULT '0',
  `customers_create` tinyint(1) NOT NULL DEFAULT '0',
  `customers_update` tinyint(1) NOT NULL DEFAULT '0',
  `customers_delete` tinyint(1) NOT NULL DEFAULT '0',
  `tax_location_view` tinyint(1) NOT NULL DEFAULT '0',
  `tax_location_create` tinyint(1) NOT NULL DEFAULT '0',
  `tax_location_update` tinyint(1) NOT NULL DEFAULT '0',
  `tax_location_delete` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_view` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_create` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_update` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_delete` tinyint(1) NOT NULL DEFAULT '0',
  `notifications_view` tinyint(1) NOT NULL DEFAULT '0',
  `notifications_send` tinyint(1) NOT NULL DEFAULT '0',
  `orders_view` tinyint(1) NOT NULL DEFAULT '0',
  `orders_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_methods_view` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_methods_update` tinyint(1) NOT NULL DEFAULT '0',
  `payment_methods_view` tinyint(1) NOT NULL DEFAULT '0',
  `payment_methods_update` tinyint(1) NOT NULL DEFAULT '0',
  `reports_view` tinyint(1) NOT NULL DEFAULT '0',
  `website_setting_view` tinyint(1) NOT NULL DEFAULT '0',
  `website_setting_update` tinyint(1) NOT NULL DEFAULT '0',
  `application_setting_view` tinyint(1) NOT NULL DEFAULT '0',
  `application_setting_update` tinyint(1) NOT NULL DEFAULT '0',
  `general_setting_view` tinyint(1) NOT NULL DEFAULT '0',
  `general_setting_update` tinyint(1) NOT NULL DEFAULT '0',
  `manage_admins_view` tinyint(1) NOT NULL DEFAULT '0',
  `manage_admins_create` tinyint(1) NOT NULL DEFAULT '0',
  `manage_admins_update` tinyint(1) NOT NULL DEFAULT '0',
  `manage_admins_delete` tinyint(1) NOT NULL DEFAULT '0',
  `language_view` tinyint(1) NOT NULL DEFAULT '0',
  `language_create` tinyint(1) NOT NULL DEFAULT '0',
  `language_update` tinyint(1) NOT NULL DEFAULT '0',
  `language_delete` tinyint(1) NOT NULL DEFAULT '0',
  `profile_view` tinyint(1) NOT NULL DEFAULT '0',
  `profile_update` tinyint(1) NOT NULL DEFAULT '0',
  `admintype_view` tinyint(1) NOT NULL DEFAULT '0',
  `admintype_create` tinyint(1) NOT NULL DEFAULT '0',
  `admintype_update` tinyint(1) NOT NULL DEFAULT '0',
  `admintype_delete` tinyint(1) NOT NULL DEFAULT '0',
  `manage_admins_role` tinyint(1) NOT NULL DEFAULT '0',
  `add_media` tinyint(1) NOT NULL DEFAULT '0',
  `edit_media` tinyint(1) NOT NULL DEFAULT '0',
  `view_media` tinyint(1) NOT NULL DEFAULT '0',
  `delete_media` tinyint(1) NOT NULL DEFAULT '0',
  `edit_management` tinyint(1) NOT NULL DEFAULT '0',
  `reviews_view` tinyint(1) NOT NULL DEFAULT '0',
  `reviews_update` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `manage_role`
--

INSERT INTO `manage_role` (`manage_role_id`, `user_types_id`, `dashboard_view`, `manufacturer_view`, `manufacturer_create`, `manufacturer_update`, `manufacturer_delete`, `categories_view`, `categories_create`, `categories_update`, `categories_delete`, `products_view`, `products_create`, `products_update`, `products_delete`, `news_view`, `news_create`, `news_update`, `news_delete`, `customers_view`, `customers_create`, `customers_update`, `customers_delete`, `tax_location_view`, `tax_location_create`, `tax_location_update`, `tax_location_delete`, `coupons_view`, `coupons_create`, `coupons_update`, `coupons_delete`, `notifications_view`, `notifications_send`, `orders_view`, `orders_confirm`, `shipping_methods_view`, `shipping_methods_update`, `payment_methods_view`, `payment_methods_update`, `reports_view`, `website_setting_view`, `website_setting_update`, `application_setting_view`, `application_setting_update`, `general_setting_view`, `general_setting_update`, `manage_admins_view`, `manage_admins_create`, `manage_admins_update`, `manage_admins_delete`, `language_view`, `language_create`, `language_update`, `language_delete`, `profile_view`, `profile_update`, `admintype_view`, `admintype_create`, `admintype_update`, `admintype_delete`, `manage_admins_role`, `add_media`, `edit_media`, `view_media`, `delete_media`, `edit_management`, `reviews_view`, `reviews_update`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 12, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `manufacturers_id` int(10) UNSIGNED NOT NULL,
  `manufacturer_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturers_slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers_info`
--

CREATE TABLE `manufacturers_info` (
  `manufacturers_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `manufacturers_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_clicked` int(11) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `sub_sort_order` int(11) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `external_link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `sort_order`, `sub_sort_order`, `parent_id`, `type`, `external_link`, `link`, `page_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 0, 1, NULL, '/', NULL, 1, NULL, NULL),
(2, 2, NULL, 0, 1, NULL, 'shop', NULL, 1, NULL, NULL),
(22, 6, NULL, 0, 1, NULL, 'contact', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_translation`
--

CREATE TABLE `menu_translation` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `menu_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_translation`
--

INSERT INTO `menu_translation` (`id`, `menu_id`, `language_id`, `menu_name`) VALUES
(2, 1, 1, 'Home'),
(3, 1, 2, 'Homee'),
(11, 2, 1, 'SHOP'),
(12, 2, 2, 'SHOP'),
(39, 21, 1, 'Demo'),
(40, 21, 2, 'Demo'),
(41, 22, 1, 'Contact Us'),
(42, 22, 2, 'Contact Us');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_09_24_122557_create_address_book_table', 1),
(2, '2019_09_24_122557_create_alert_settings_table', 1),
(3, '2019_09_24_122557_create_api_calls_list_table', 1),
(4, '2019_09_24_122557_create_banners_history_table', 1),
(5, '2019_09_24_122557_create_banners_table', 1),
(6, '2019_09_24_122557_create_block_ips_table', 1),
(7, '2019_09_24_122557_create_categories_description_table', 1),
(8, '2019_09_24_122557_create_categories_role_table', 1),
(9, '2019_09_24_122557_create_categories_table', 1),
(10, '2019_09_24_122557_create_compare_table', 1),
(11, '2019_09_24_122557_create_constant_banners_table', 1),
(12, '2019_09_24_122557_create_countries_table', 1),
(13, '2019_09_24_122557_create_coupons_table', 1),
(14, '2019_09_24_122557_create_currencies_table', 1),
(15, '2019_09_24_122557_create_currency_record_table', 1),
(16, '2019_09_24_122557_create_current_theme_table', 1),
(17, '2019_09_24_122557_create_customers_basket_attributes_table', 1),
(18, '2019_09_24_122557_create_customers_basket_table', 1),
(19, '2019_09_24_122557_create_customers_info_table', 1),
(20, '2019_09_24_122557_create_customers_table', 1),
(21, '2019_09_24_122557_create_devices_table', 1),
(22, '2019_09_24_122557_create_flash_sale_table', 1),
(23, '2019_09_24_122557_create_flate_rate_table', 1),
(24, '2019_09_24_122557_create_front_end_theme_content_table', 1),
(25, '2019_09_24_122557_create_geo_zones_table', 1),
(26, '2019_09_24_122557_create_http_call_record_table', 1),
(27, '2019_09_24_122557_create_image_categories_table', 1),
(28, '2019_09_24_122557_create_images_table', 1),
(29, '2019_09_24_122557_create_inventory_detail_table', 1),
(30, '2019_09_24_122557_create_inventory_table', 1),
(31, '2019_09_24_122557_create_label_value_table', 1),
(32, '2019_09_24_122557_create_labels_table', 1),
(33, '2019_09_24_122557_create_languages_table', 1),
(34, '2019_09_24_122557_create_liked_products_table', 1),
(35, '2019_09_24_122557_create_manage_min_max_table', 1),
(36, '2019_09_24_122557_create_manage_role_table', 1),
(37, '2019_09_24_122557_create_manufacturers_info_table', 1),
(38, '2019_09_24_122557_create_manufacturers_table', 1),
(39, '2019_09_24_122557_create_news_categories_description_table', 1),
(40, '2019_09_24_122557_create_news_categories_table', 1),
(41, '2019_09_24_122557_create_news_description_table', 1),
(42, '2019_09_24_122557_create_news_table', 1),
(43, '2019_09_24_122557_create_news_to_news_categories_table', 1),
(44, '2019_09_24_122557_create_orders_products_attributes_table', 1),
(45, '2019_09_24_122557_create_orders_products_table', 1),
(46, '2019_09_24_122557_create_orders_status_description_table', 1),
(47, '2019_09_24_122557_create_orders_status_history_table', 1),
(48, '2019_09_24_122557_create_orders_status_table', 1),
(49, '2019_09_24_122557_create_orders_table', 1),
(50, '2019_09_24_122557_create_orders_total_table', 1),
(51, '2019_09_24_122557_create_pages_description_table', 1),
(52, '2019_09_24_122557_create_pages_table', 1),
(53, '2019_09_24_122557_create_payment_description_table', 1),
(54, '2019_09_24_122557_create_payment_methods_detail_table', 1),
(55, '2019_09_24_122557_create_payment_methods_table', 1),
(56, '2019_09_24_122557_create_permissions_table', 1),
(57, '2019_09_24_122557_create_products_attributes_table', 1),
(58, '2019_09_24_122557_create_products_description_table', 1),
(59, '2019_09_24_122557_create_products_images_table', 1),
(60, '2019_09_24_122557_create_products_options_descriptions_table', 1),
(61, '2019_09_24_122557_create_products_options_table', 1),
(62, '2019_09_24_122557_create_products_options_values_descriptions_table', 1),
(63, '2019_09_24_122557_create_products_options_values_table', 1),
(64, '2019_09_24_122557_create_products_shipping_rates_table', 1),
(65, '2019_09_24_122557_create_products_table', 1),
(66, '2019_09_24_122557_create_products_to_categories_table', 1),
(67, '2019_09_24_122557_create_reviews_description_table', 1),
(68, '2019_09_24_122557_create_reviews_table', 1),
(69, '2019_09_24_122557_create_sessions_table', 1),
(70, '2019_09_24_122557_create_settings_table', 1),
(71, '2019_09_24_122557_create_shipping_description_table', 1),
(72, '2019_09_24_122557_create_shipping_methods_table', 1),
(73, '2019_09_24_122557_create_sliders_images_table', 1),
(74, '2019_09_24_122557_create_specials_table', 1),
(75, '2019_09_24_122557_create_tax_class_table', 1),
(76, '2019_09_24_122557_create_tax_rates_table', 1),
(77, '2019_09_24_122557_create_units_descriptions_table', 1),
(78, '2019_09_24_122557_create_units_table', 1),
(79, '2019_09_24_122557_create_ups_shipping_table', 1),
(80, '2019_09_24_122557_create_user_to_address_table', 1),
(81, '2019_09_24_122557_create_user_types_table', 1),
(82, '2019_09_24_122557_create_users_table', 1),
(83, '2019_09_24_122557_create_whos_online_table', 1),
(84, '2019_09_24_122557_create_zones_table', 1),
(85, '2019_09_24_122557_create_zones_to_geo_zones_table', 1),
(86, '2019_12_11_070737_create_menus_table', 1),
(87, '2019_12_11_070821_create_menu_translation_table', 1),
(88, '2020_02_04_121358_top_offers', 1),
(89, '2020_03_25_141022_create_home_banners', 1),
(90, '2020_08_14_093955_create_organizations_table', 2),
(96, '2020_08_14_101304_create_audits_table', 3),
(97, '2020_08_18_080753_create_documents_table', 3),
(98, '2020_08_18_115005_create_audit_questions_table', 3),
(99, '2021_02_08_100011_create_audit_images', 4),
(100, '2021_02_08_100136_create_site_audits', 5);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_image` text COLLATE utf8_unicode_ci,
  `news_date_added` datetime NOT NULL,
  `news_last_modified` datetime DEFAULT NULL,
  `news_status` tinyint(1) NOT NULL,
  `is_feature` tinyint(1) NOT NULL DEFAULT '0',
  `news_slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

CREATE TABLE `news_categories` (
  `categories_id` int(11) NOT NULL,
  `categories_image` text COLLATE utf8_unicode_ci,
  `categories_icon` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `news_categories_slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `categories_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_categories_description`
--

CREATE TABLE `news_categories_description` (
  `categories_description_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_description`
--

CREATE TABLE `news_description` (
  `language_id` int(11) NOT NULL DEFAULT '1',
  `news_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `news_id` int(11) NOT NULL,
  `news_description` text COLLATE utf8_unicode_ci,
  `news_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `news_viewed` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_to_news_categories`
--

CREATE TABLE `news_to_news_categories` (
  `news_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orders_id` int(11) NOT NULL,
  `total_tax` decimal(7,2) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `customers_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `customers_company` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_street_address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `customers_suburb` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `customers_postcode` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `customers_state` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `customers_telephone` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `customers_address_format_id` int(11) DEFAULT NULL,
  `delivery_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_company` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_street_address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_suburb` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_postcode` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_state` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address_format_id` int(11) DEFAULT NULL,
  `billing_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `billing_company` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_street_address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `billing_suburb` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `billing_postcode` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_format_id` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_owner` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_expires` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_price` decimal(10,2) NOT NULL,
  `shipping_cost` decimal(10,2) NOT NULL,
  `shipping_method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_duration` int(11) DEFAULT NULL,
  `order_information` text COLLATE utf8_unicode_ci NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `coupon_code` text COLLATE utf8_unicode_ci NOT NULL,
  `coupon_amount` int(11) NOT NULL,
  `exclude_product_ids` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `product_categories` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `excluded_product_categories` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `product_ids` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `ordered_source` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Website, 2: App',
  `delivery_phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `billing_phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_products`
--

CREATE TABLE `orders_products` (
  `orders_products_id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `products_model` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_price` decimal(15,2) NOT NULL,
  `final_price` decimal(15,2) NOT NULL,
  `products_tax` decimal(7,0) NOT NULL,
  `products_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_products_attributes`
--

CREATE TABLE `orders_products_attributes` (
  `orders_products_attributes_id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `orders_products_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `products_options` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_values` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `options_values_price` decimal(15,2) NOT NULL,
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_status`
--

CREATE TABLE `orders_status` (
  `orders_status_id` int(11) NOT NULL,
  `public_flag` int(11) DEFAULT '0',
  `downloads_flag` int(11) DEFAULT '0',
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders_status`
--

INSERT INTO `orders_status` (`orders_status_id`, `public_flag`, `downloads_flag`, `role_id`) VALUES
(1, 0, 0, 2),
(2, 0, 0, 2),
(3, 0, 0, 2),
(4, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders_status_description`
--

CREATE TABLE `orders_status_description` (
  `orders_status_description_id` int(11) NOT NULL,
  `orders_status_id` int(11) NOT NULL,
  `orders_status_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders_status_description`
--

INSERT INTO `orders_status_description` (`orders_status_description_id`, `orders_status_id`, `orders_status_name`, `language_id`) VALUES
(1, 1, 'Pending', 1),
(2, 2, 'Completed', 1),
(3, 3, 'Cancel', 1),
(4, 4, 'Return', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_status_history`
--

CREATE TABLE `orders_status_history` (
  `orders_status_history_id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `orders_status_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `customer_notified` int(11) DEFAULT '0',
  `comments` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_total`
--

CREATE TABLE `orders_total` (
  `orders_total_id` int(10) UNSIGNED NOT NULL,
  `orders_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(15,4) NOT NULL,
  `class` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Freshstop', '', NULL, NULL),
(2, 'Food Lovers', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages_description`
--

CREATE TABLE `pages_description` (
  `page_description_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_description`
--

CREATE TABLE `payment_description` (
  `id` int(11) NOT NULL,
  `payment_methods_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `sub_name_1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sub_name_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_description`
--

INSERT INTO `payment_description` (`id`, `payment_methods_id`, `name`, `language_id`, `sub_name_1`, `sub_name_2`) VALUES
(1, 1, 'Braintree', 1, 'Credit Card', 'Paypal'),
(4, 2, 'Stripe', 1, '', ''),
(5, 3, 'Paypal', 1, '', ''),
(6, 4, 'Cash on Delivery', 1, '', ''),
(7, 5, 'Instamojo', 1, '', ''),
(8, 0, 'Cybersoure', 1, '', ''),
(9, 6, 'Hyperpay', 1, '', ''),
(10, 7, 'Razor Pay', 1, '', ''),
(11, 8, 'PayTm', 1, '', ''),
(12, 9, 'Direct Bank Transfer', 1, 'Make your payment directly into our bank account. Please use your Order ID as the payment reference.', ''),
(13, 10, 'Paystack', 1, '', ''),
(14, 11, 'Midtrans', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `payment_methods_id` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `environment` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`payment_methods_id`, `payment_method`, `status`, `environment`, `created_at`, `updated_at`) VALUES
(1, 'braintree', 0, 0, '2019-09-18 14:40:13', '0000-00-00 00:00:00'),
(2, 'stripe', 0, 0, '2019-09-18 14:56:17', '0000-00-00 00:00:00'),
(3, 'paypal', 0, 0, '2019-09-18 14:56:04', '0000-00-00 00:00:00'),
(4, 'cash_on_delivery', 1, 0, '2019-09-18 14:56:37', '0000-00-00 00:00:00'),
(5, 'instamojo', 0, 0, '2019-09-18 14:57:23', '0000-00-00 00:00:00'),
(6, 'hyperpay', 0, 0, '2019-09-18 14:56:44', '0000-00-00 00:00:00'),
(7, 'razor_pay', 0, 0, '2019-09-18 14:56:44', '0000-00-00 00:00:00'),
(8, 'pay_tm', 0, 0, '2019-09-18 14:56:44', '0000-00-00 00:00:00'),
(9, 'banktransfer', 0, 0, '2019-09-18 14:56:44', '0000-00-00 00:00:00'),
(10, 'paystack', 0, 0, '2019-09-18 14:56:44', '0000-00-00 00:00:00'),
(11, 'midtrans', 0, 0, '2019-09-18 14:56:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods_detail`
--

CREATE TABLE `payment_methods_detail` (
  `id` int(11) NOT NULL,
  `payment_methods_id` int(11) NOT NULL,
  `key` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods_detail`
--

INSERT INTO `payment_methods_detail` (`id`, `payment_methods_id`, `key`, `value`) VALUES
(3, 1, 'merchant_id', ''),
(4, 1, 'public_key', ''),
(5, 1, 'private_key', ''),
(9, 2, 'secret_key', ''),
(10, 2, 'publishable_key', ''),
(15, 3, 'id', ''),
(18, 3, 'payment_currency', 'USD'),
(21, 5, 'api_key', ''),
(22, 5, 'auth_token', ''),
(23, 5, 'client_id', ''),
(24, 5, 'client_secret', ''),
(32, 6, 'userid', ''),
(33, 6, 'password', ''),
(34, 6, 'entityid', ''),
(35, 7, 'RAZORPAY_KEY', ''),
(36, 7, 'RAZORPAY_SECRET', ''),
(37, 8, 'paytm_mid', ''),
(39, 8, 'paytm_key', 'w#'),
(40, 8, 'current_domain_name', ''),
(41, 9, 'account_name', ''),
(42, 9, 'account_number', ''),
(43, 9, 'bank_name', ''),
(44, 9, 'short_code', ''),
(45, 9, 'iban', ''),
(46, 9, 'swift', ''),
(47, 10, 'secret_key', ''),
(48, 10, 'public_key', ''),
(49, 11, 'merchant_id', ''),
(50, 11, 'server_key', ''),
(51, 11, 'client_key', '');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `role_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `products_id` int(11) NOT NULL,
  `products_quantity` int(11) NOT NULL,
  `products_model` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_image` text COLLATE utf8_unicode_ci,
  `products_video_link` text COLLATE utf8_unicode_ci,
  `products_price` decimal(15,2) NOT NULL,
  `products_date_added` datetime NOT NULL,
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_weight_unit` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_status` tinyint(1) NOT NULL,
  `is_current` tinyint(1) NOT NULL,
  `products_tax_class_id` int(11) NOT NULL,
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_liked` int(11) NOT NULL,
  `low_limit` int(11) NOT NULL,
  `is_feature` tinyint(1) NOT NULL DEFAULT '0',
  `products_slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `products_type` int(11) NOT NULL DEFAULT '0',
  `products_min_order` int(11) NOT NULL DEFAULT '1',
  `products_max_stock` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_attributes`
--

CREATE TABLE `products_attributes` (
  `products_attributes_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `options_id` int(11) NOT NULL,
  `options_values_id` int(11) NOT NULL,
  `options_values_price` decimal(15,2) NOT NULL,
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_description`
--

CREATE TABLE `products_description` (
  `id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_description` text COLLATE utf8_unicode_ci,
  `products_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_viewed` int(11) DEFAULT '0',
  `products_left_banner` text COLLATE utf8_unicode_ci,
  `products_left_banner_start_date` int(11) DEFAULT NULL,
  `products_left_banner_expire_date` int(11) DEFAULT NULL,
  `products_right_banner` text COLLATE utf8_unicode_ci,
  `products_right_banner_start_date` int(11) DEFAULT NULL,
  `products_right_banner_expire_date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE `products_images` (
  `id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_options`
--

CREATE TABLE `products_options` (
  `products_options_id` int(11) NOT NULL,
  `products_options_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_options_descriptions`
--

CREATE TABLE `products_options_descriptions` (
  `products_options_descriptions_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `options_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_options_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_options_values`
--

CREATE TABLE `products_options_values` (
  `products_options_values_id` int(11) NOT NULL,
  `products_options_id` int(11) NOT NULL,
  `products_options_values_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_options_values_descriptions`
--

CREATE TABLE `products_options_values_descriptions` (
  `products_options_values_descriptions_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `options_values_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_values_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products_shipping_rates`
--

CREATE TABLE `products_shipping_rates` (
  `products_shipping_rates_id` int(11) NOT NULL,
  `weight_from` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_to` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_price` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `products_shipping_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_shipping_rates`
--

INSERT INTO `products_shipping_rates` (`products_shipping_rates_id`, `weight_from`, `weight_to`, `weight_price`, `unit_id`, `products_shipping_status`) VALUES
(1, '0', '10', 10, 1, 1),
(2, '10', '20', 10, 1, 1),
(3, '20', '30', 10, 1, 1),
(4, '30', '50', 50, 1, 1),
(5, '50', '100000', 70, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_to_categories`
--

CREATE TABLE `products_to_categories` (
  `products_to_categories_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `reviews_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `reviews_rating` int(11) DEFAULT NULL,
  `reviews_status` tinyint(1) NOT NULL DEFAULT '0',
  `reviews_read` int(11) NOT NULL DEFAULT '0',
  `vendors_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews_description`
--

CREATE TABLE `reviews_description` (
  `id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `reviews_text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `sesskey` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` int(10) UNSIGNED NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'facebook_app_id', 'FB_CLIENT_ID', '2018-04-26 22:00:00', '2019-11-01 04:58:53'),
(2, 'facebook_secret_id', 'FB_SECRET_KEY', '2018-04-26 22:00:00', '2019-11-01 04:58:53'),
(3, 'facebook_login', '0', '2018-04-26 22:00:00', '2019-11-01 04:58:53'),
(4, 'contact_us_email', '', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(5, 'address', 'address', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(6, 'city', 'City', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(7, 'state', 'State', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(8, 'zip', 'Zip', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(9, 'country', 'Country', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(10, 'latitude', 'Latitude', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(11, 'longitude', 'Longitude', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(12, 'phone_no', '+92 312 1234567', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(13, 'fcm_android', '', '2018-04-26 22:00:00', '2019-02-05 09:42:15'),
(14, 'fcm_ios', NULL, '2018-04-26 22:00:00', NULL),
(15, 'fcm_desktop', NULL, '2018-04-26 22:00:00', NULL),
(16, 'website_logo', 'images/admin_logo/logo-laravel-blue-v1.png', '2018-04-26 22:00:00', '2019-10-11 09:57:29'),
(17, 'fcm_android_sender_id', NULL, '2018-04-26 22:00:00', NULL),
(18, 'fcm_ios_sender_id', '', '2018-04-26 22:00:00', '2019-02-05 09:42:15'),
(19, 'app_name', 'Laravel Audit', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(20, 'currency_symbol', 'R', '2018-04-26 22:00:00', '2018-11-19 05:26:01'),
(21, 'new_product_duration', '20', '2018-04-26 22:00:00', '2019-10-21 05:36:53'),
(22, 'notification_title', 'Ionic Ecommerce', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(23, 'notification_text', 'A bundle of products waiting for you!', '2018-04-26 22:00:00', NULL),
(24, 'lazzy_loading_effect', 'Detail', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(25, 'footer_button', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(26, 'cart_button', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(27, 'featured_category', NULL, '2018-04-26 22:00:00', NULL),
(28, 'notification_duration', 'year', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(29, 'home_style', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(30, 'wish_list_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(31, 'edit_profile_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(32, 'shipping_address_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(33, 'my_orders_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(34, 'contact_us_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(35, 'about_us_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(36, 'news_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(37, 'intro_page', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(38, 'setting_page', '1', '2018-04-26 22:00:00', NULL),
(39, 'share_app', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(40, 'rate_app', '1', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(41, 'site_url', 'URL', '2018-04-26 22:00:00', '2018-11-19 05:26:01'),
(42, 'admob', '0', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(43, 'admob_id', 'ID', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(44, 'ad_unit_id_banner', 'Unit ID', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(45, 'ad_unit_id_interstitial', 'Indestrial', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(46, 'category_style', '4', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(47, 'package_name', 'package name', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(48, 'google_analytic_id', 'test', '2018-04-26 22:00:00', '2019-05-15 08:58:30'),
(49, 'themes', 'themeone', '2018-04-26 22:00:00', NULL),
(50, 'company_name', '#', '2018-04-26 22:00:00', '2019-10-07 07:52:24'),
(51, 'facebook_url', '#', '2018-04-26 22:00:00', '2019-10-11 09:57:29'),
(52, 'google_url', '#', '2018-04-26 22:00:00', '2019-10-11 09:57:29'),
(53, 'twitter_url', '#', '2018-04-26 22:00:00', '2019-10-11 09:57:29'),
(54, 'linked_in', '#', '2018-04-26 22:00:00', '2019-10-11 09:57:29'),
(55, 'default_notification', 'onesignal', '2018-04-26 22:00:00', '2019-02-05 09:42:15'),
(56, 'onesignal_app_id', '6053d948-b8f6-472a-87e4-379fa89f78d8', '2018-04-26 22:00:00', '2019-02-05 09:42:15'),
(57, 'onesignal_sender_id', '', '2018-04-26 22:00:00', '2019-02-05 09:42:15'),
(58, 'ios_admob', '0', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(59, 'ios_admob_id', 'AdMob ID', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(60, 'ios_ad_unit_id_banner', 'Unit ID Banner', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(61, 'ios_ad_unit_id_interstitial', 'ID Interstitial', '2018-04-26 22:00:00', '2019-05-15 08:58:05'),
(62, 'google_login', '0', NULL, '2019-11-01 04:58:36'),
(63, 'google_app_id', NULL, NULL, NULL),
(64, 'google_secret_id', NULL, NULL, NULL),
(65, 'google_callback_url', NULL, NULL, NULL),
(66, 'facebook_callback_url', NULL, NULL, NULL),
(67, 'is_app_purchased', '1', NULL, '2018-05-04 01:24:44'),
(68, 'is_web_purchased', '0', NULL, '2018-05-04 01:24:44'),
(69, 'consumer_key', 'dadb7a7c1557917902724bbbf5', NULL, '2019-05-15 08:58:22'),
(70, 'consumer_secret', '3ba77f821557917902b1d57373', NULL, '2019-05-15 08:58:22'),
(71, 'order_email', 'orders@gmail.com', NULL, '2019-10-21 05:36:53'),
(72, 'website_themes', '1', NULL, NULL),
(73, 'seo_title', '', NULL, '2018-11-19 05:21:57'),
(74, 'seo_metatag', '', NULL, '2018-11-19 05:21:57'),
(75, 'seo_keyword', '', NULL, '2018-11-19 05:21:57'),
(76, 'seo_description', '', NULL, '2018-11-19 05:21:57'),
(77, 'before_head_tag', '', NULL, '2018-11-19 05:22:15'),
(78, 'end_body_tag', 'name', NULL, '2019-10-11 09:57:29'),
(79, 'sitename_logo', 'name', NULL, '2019-10-11 09:57:29'),
(80, 'website_name', '<strong>E</strong>COMMERCE', NULL, '2018-11-19 05:22:25'),
(81, 'web_home_pages_style', 'two', NULL, '2018-11-19 05:22:25'),
(82, 'web_color_style', 'app', NULL, '2018-11-19 05:22:25'),
(83, 'free_shipping_limit', '400', NULL, '2019-10-21 05:36:53'),
(84, 'app_icon_image', 'icon', NULL, '2019-02-05 08:12:59'),
(85, 'twilio_status', '0', NULL, NULL),
(86, 'twilio_authy_api_id', '', NULL, NULL),
(87, 'favicon', 'images/admin_logo/logo-laravel-blue-v1.png', NULL, NULL),
(88, 'Thumbnail_height', '150', NULL, NULL),
(89, 'Thumbnail_width', '150', NULL, NULL),
(90, 'Medium_height', '400', NULL, NULL),
(91, 'Medium_width', '400', NULL, NULL),
(92, 'Large_height', '900', NULL, NULL),
(93, 'Large_width', '900', NULL, NULL),
(94, 'environmentt', 'local', NULL, '2019-10-21 05:36:53'),
(95, 'maintenance_text', 'Website is under maintenance', NULL, '2019-10-21 05:36:53'),
(96, 'package_charge_taxt', '0', NULL, NULL),
(97, 'order_commission', '0', NULL, NULL),
(98, 'all_items_price_included_tax', 'yes', NULL, NULL),
(99, 'all_items_price_included_tax_value', '0', NULL, NULL),
(100, 'driver_accept_multiple_order', '1', NULL, NULL),
(101, 'min_order_price', '20', NULL, '2019-10-21 05:36:53'),
(102, 'youtube_link', '0', NULL, NULL),
(103, 'external_website_link', 'http://localhost', NULL, '2019-10-21 05:36:53'),
(104, 'google_map_api', '', NULL, '2019-10-21 05:36:53'),
(105, 'is_pos_purchased', '0', NULL, NULL),
(106, 'admin_version', '4.0.18', NULL, NULL),
(107, 'app_version', '4.0.18', NULL, NULL),
(108, 'web_version', '4.0.18', NULL, NULL),
(109, 'pos_version', '0', NULL, NULL),
(110, 'android_app_link', '#', NULL, NULL),
(111, 'iphone_app_link', '#', NULL, NULL),
(112, 'about_content', 'Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum', NULL, '2019-10-11 09:57:29'),
(113, 'contact_content', 'Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum Lorum Ipsum Upsum Kupsum Jipsum Mipsum', NULL, '2019-10-11 09:57:29'),
(114, 'contents', NULL, NULL, NULL),
(115, 'fb_redirect_url', 'http://YOUR_DOMAIN_NAME/login/facebook/callback', NULL, '2019-11-01 04:58:53'),
(116, 'google_client_id', 'GOOGLE_CLIENT_ID', NULL, '2019-11-01 04:58:36'),
(117, 'google_client_secret', 'GOOGLE_SECRET_KEY', NULL, '2019-11-01 04:58:36'),
(118, 'google_redirect_url', 'http://YOUR_DOMAIN_NAME/login/google/callback', NULL, '2019-11-01 04:58:36'),
(119, 'newsletter', '0', NULL, '2019-11-01 04:58:36'),
(120, 'allow_cookies', '0', NULL, '2019-11-01 04:58:36'),
(121, 'card_style', '1', NULL, '2019-11-01 04:58:36'),
(122, 'banner_style', '1', NULL, '2019-11-01 04:58:36'),
(123, 'mail_chimp_api', '', NULL, '2019-11-01 04:58:36'),
(124, 'mail_chimp_list_id', '', NULL, '2019-11-01 04:58:36'),
(125, 'newsletter_image', 'images/media/2019/10/newsletter.jpg', NULL, '2019-11-01 04:58:36'),
(126, 'instauserid', '', NULL, '2019-11-01 04:58:36'),
(127, 'web_card_style', '1', NULL, '2019-11-01 04:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_description`
--

CREATE TABLE `shipping_description` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `table_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sub_labels` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_description`
--

INSERT INTO `shipping_description` (`id`, `name`, `language_id`, `table_name`, `sub_labels`) VALUES
(1, 'Free Shipping', 1, 'free_shipping', ''),
(4, 'Local Pickup', 1, 'local_pickup', ''),
(7, 'Flat Rate', 1, 'flate_rate', ''),
(10, 'UPS Shipping', 1, 'ups_shipping', '{\"nextDayAir\":\"Next Day Air\",\"secondDayAir\":\"2nd Day Air\",\"ground\":\"Ground\",\"threeDaySelect\":\"3 Day Select\",\"nextDayAirSaver\":\"Next Day AirSaver\",\"nextDayAirEarlyAM\":\"Next Day Air Early A.M.\",\"secondndDayAirAM\":\"2nd Day Air A.M.\"}'),
(13, 'Shipping Price', 1, 'shipping_by_weight', '');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_methods`
--

CREATE TABLE `shipping_methods` (
  `shipping_methods_id` int(11) NOT NULL,
  `methods_type_link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `table_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_methods`
--

INSERT INTO `shipping_methods` (`shipping_methods_id`, `methods_type_link`, `isDefault`, `status`, `table_name`) VALUES
(1, 'upsShipping', 0, 0, 'ups_shipping'),
(2, 'freeShipping', 0, 0, 'free_shipping'),
(3, 'localPickup', 0, 0, 'local_pickup'),
(4, 'flateRate', 1, 1, 'flate_rate'),
(5, 'shippingByWeight', 0, 0, 'shipping_by_weight');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `id_no` int(10) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `site_name` varchar(80) DEFAULT NULL,
  `site_number` varchar(50) DEFAULT NULL,
  `slip_number` varchar(7) DEFAULT NULL,
  `qr_code` varchar(7) DEFAULT NULL,
  `bc` varchar(10) DEFAULT NULL,
  `retailer` varchar(200) DEFAULT NULL,
  `manager` varchar(200) DEFAULT NULL,
  `area` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `landline` varchar(30) DEFAULT NULL,
  `cell_no` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `id_no`, `location_id`, `site_name`, `site_number`, `slip_number`, `qr_code`, `bc`, `retailer`, `manager`, `area`, `address`, `landline`, `cell_no`, `email`, `status`, `timestamp`) VALUES
(1, 1, 180, 'FRESHSTOP GIE ROAD', '315732', NULL, 'efb0ab9', 'AMA', 'DARRYL  JOSHUA', 'DALE JOSHUA', 'WC', 'CNR GIE & PARKLANDS MAIN ROAD', '215564513', '825532006', 'wc.gieroad@freshstop.co.za; propertyinc@mweb.co.za', 1, '2020-07-30 10:37:04'),
(2, 2, 181, 'FRESHSTOP RAVENSVIEW', '315731', NULL, 'ef26ab6', 'AMA', 'PIETER & DEBBIE BOUWER', 'DEBBIE BOUWER', 'WC', 'CNR RAVENSCOURT ROAD & LINK ROAD', '021 556 4021', '83 626 8853', ' wc.ravensview@freshstop.co.za; debbiebouwer@icloud.co.za', 1, '2020-07-30 10:37:04'),
(3, 3, 182, 'FRESHSTOP MELKBOS', '315211', NULL, '12064f5', 'AMA', 'JP PEQUENEZA', 'JACI HAMMAN / ODETTE', 'WC', '6TH AVENUE MELKBOSSTRAND', '215533010', '072 079 8257 / 072 030 9448 / ', 'wc.melkbos@freshstop.co.za; jacimm@netpoint.co.za; odettemm@netpoint.co.za', 1, '2020-07-30 10:37:04'),
(4, 4, 183, 'FRESHSTOP MARINE DRIVE', '315705', NULL, '97bc913', 'AMA', 'ERNST BADENHORST', 'CHREDDY MAYIMELA', 'WC', 'MARINE DRIVE ROAD', '215108191', '082 324 1658(B) /076 869 8294(', 'marinedrv@mweb.co.za; wc.marinedrive@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(5, 5, 0, 'FRESHSTOP SEAVIEW', '316995', NULL, '7080d28', 'SD', 'ALI GAFFOOR', 'RAEES GAFOOR', 'WC', 'CNR SPINE AND CAPRI ROADS', '021 393 5292', '072 595 0851 / 074 333 7774', 'wc.seaview@freshstop.co.za; ubarey@mweb.co.za', 1, '2020-07-30 10:37:04'),
(6, 6, 222, 'FRESHSTOP RUHAMAH - Temp Closed', '314060', NULL, '9bac7a2', 'AG', 'ROSS MACGREGAR', 'ELIZE', 'INL', '1 RUHAMAH DRIVE', '011 764 5081', '082 938 6777', 'gau.ruhamah@freshstop.co.za; ross@macfuel.co.za; elize@macfuel.co.za', 0, '2020-07-30 10:37:04'),
(7, 7, 223, 'FRESHSTOP RANDBURG WATERFALL', '333566', NULL, '2e6c652', 'AG', 'ERIKA PRINSLOO', 'JAMES', 'INL', '89 JAN SMUTS AVE,BLAIRGOWRIE', '011 787 4704', '082 451 3624', 'gau.waterfall@freshstop.co.za; erika@waterfallgarage.co.za', 1, '2020-07-30 10:37:04'),
(8, 8, 224, 'FRESHSTOP RANDBURG WATERFRONT', '313629', NULL, '835569e', 'AG', 'ELIZE PRINSLOO', 'MOHAMED', 'INL', 'CNR WEST & REPUBLIC RDS', '011 886 3551', '083 309 2358', 'gau.randburg@freshstop.co.za; worx@wam.co.za', 1, '2020-07-30 10:37:04'),
(9, 9, 185, 'FRESHSTOP TUSCANY', '312499', NULL, 'a180bd1', 'SD', 'RIAZ JAFFER /   MURAD  DALVIE', 'ODESSA ARENDSE', 'WC', 'CNR HINDLE AND BLUE DOWNS RDS', '219096449', '079 667 2711', 'wc.tuscany@freshstop.co.za; murad24171@gmail.com', 1, '2020-07-30 10:37:04'),
(10, 10, 20, 'FRESHSTOP CENTENARY', '316028', NULL, 'ea33761', 'RP', 'HASSAN GANGAT', 'MOHAMED TAYOB', 'EC', '1 CENTENARY ROAD', '041 368 1631', '083 266 5646', 'ec.centenary@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(11, 11, 0, 'FRESHSTOP HARDING', '313942', NULL, '226ec55', '', 'CASSIEM MOOSA', 'ROWAIDA MOOSA', 'KZN S', '17 CNR MURCHISON & HAWKINS STREETS', '394332851', '082 212 6786 / 079 959 8903(R)', 'nat.harding@freshstop.co.za; cyamoosa@mweb.co.za', 1, '2020-07-30 10:37:04'),
(12, 12, 225, 'FRESHSTOP OAKLANDS', '315620', NULL, 'ef15e09', 'AG', 'FAYYAZ MOOSA', 'ZUFAEL', 'INL', 'CNR 17th AVE & 4th STREET ,OAKLANDS', '011 483 3233', '845115652 / 074 325 7432', 'gau.oaklands@freshstop.co.za;oaklandsadmin@affaholdings.com;zufaels@affaholdings.com;', 1, '2020-07-30 10:37:04'),
(13, 13, 0, 'FRESHSTOP MALELANE', '315109', NULL, '4274cf4', 'HMcD', 'MAS CORPORATION', 'BERLINDA', 'MPU N', '6 BUFFELS STREET', '013 790 0214', '082 783 5423', 'mpu.malelane@freshstop.co.za; belinda.m@mascor.co.za; ', 1, '2020-07-30 10:37:04'),
(14, 14, 21, 'FRESHSTOP PORT ALFRED (ANDY\'S)', '316022', NULL, '9ef6b5f', 'CB', 'JASON PRINCE', 'JUSTIN LOUW', 'EC', '80 SOUTHWELL ROAD', '046 624 5792', '082 365 2911', 'ec.andys@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(15, 15, 22, 'FRESHSTOP WESTERING', '316082', NULL, 'd8fd128', 'RP', 'KEVIN PURCELL', 'EVE PURCELL', 'EC', '513 CAPE ROAD', '041 360 1232', '082 784 6789', 'ec.westering@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(16, 16, 9, 'FRESHSTOP DRIVE INN', '333809', NULL, '0467d6e', 'MN', 'PAUL SNYMAN & DAWN GADINABOKAO', 'DAVID YEZE', 'INL', '60 BLACK REEF RD', '011 902 2277', '082 322 2178 / 072 599 0621', 'gau.driveinn@freshstop.co.za; paulsnyman13@gmail.com', 1, '2020-07-30 10:37:04'),
(17, 17, 186, 'FRESHSTOP WATERFRONT', '312795', NULL, '33e5cb7', 'AMA', 'RIAZ JAFFER', 'PETER ARENDSE', 'WC', '2 DOCK ROAD, V & A WATERFRONT', '214056400', '083 226 3251', 'riazjaffer@absamail.co.za; waterfrontss@mwebbiz.co.za; waterfront@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(18, 18, 74, 'FRESHSTOP GATEWAY', '315510', NULL, 'cd460c1', 'TM', 'MAJEED DAWOOD', 'GINNY STROEBEL', 'KZN', '5 CENTENARY BOULEVARD', '315665234', '079 784 3177 / 083 357 0664', 'office@caltexgateway.co.za; raeesa@caltexgateway.co.za', 1, '2020-07-30 10:37:04'),
(19, 19, 187, 'FRESHSTOP PAARL', '338676', NULL, 'b2b855c', 'SD', 'MICKEY KEYSER', 'MICHELLE BRAAF', 'WC', '393 MAIN STREET ', '021 872 3118', '083 444 9854 / 082 400 9743', 'mickey@paarlfreshstop.co.za; paarlfreshstop@telkomsa.net; michelle@paarlfreshstop.co.za', 1, '2020-07-30 10:37:04'),
(20, 20, 23, 'FRESHSTOP HOBIE BEACH', '316105', NULL, '2808a1f', 'RP', 'CHRISMO BOTHA', 'LAWRENCE GIDI', 'EC', '1 MARINE DRIVE ', '415833157', '072 740 8781', 'ec.hobiebeach@freshstop.co.za; management.hobiebeach@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(21, 21, 0, 'FRESHSTOP NELSONS (BEREA)', '313246', NULL, 'ec7d45e', 'TM', 'RIDWAAN SADAR', 'ROOKS NAIDU', 'KZN', '211 BEREA ROAD', '312019038', '072 410 9659', 'berea@freshstop.co.za;ridwaansadar1@gmail.com', 1, '2020-07-30 10:37:04'),
(22, 22, 53, 'FRESHSTOP ANZAC', '332949', NULL, 'e4c7b55', 'RC', 'SHAUN & ELEANOR DASRATH ', 'NOZIE', 'INL', '2 MAIN REEF ROAD', '117400458', '073 132 9998', 'gau.anzac@freshstop.co.za; shaund@nqoba.co.za; ', 1, '2020-07-30 10:37:04'),
(23, 23, 0, 'FRESHSTOP EGOLI NORTH', '334243', NULL, '32d1f2c', '', 'HENNIE MAREE', 'BRUCE VAN DER RIET', 'INL', 'N1 HIGHWAY & NEW ROAD, (ON EITHER SIDE OF THE N1 HIGHWAY)', '113121520', '082 3300 897 / 079 493 0888(B)', 'bigbird@peg.co.za; h.maree@live.co.za; gau.egolinorth@freshstop.co.za; brucev50@gmail.com', 1, '2020-07-30 10:37:04'),
(24, 24, 0, 'FRESHSTOP EGOLI SOUTH', '334245', NULL, '241958f', '', 'LE ROUX GELDERMAN', 'FOSTER GIYANI', 'INL', 'N1 HIGHWAY & NEW ROAD, (ON EITHER SIDE OF THE N1 HIGHWAY)', '113121520', '082 85  2068', 'bigbird@peg.co.za; gau.egolisouth@freshstop.co.za; fosterhlungwani@gmail.com', 1, '2020-07-30 10:37:04'),
(25, 25, 188, 'FRESHSTOP WETTON ', '317679', NULL, '54b61da', 'SD', 'ANWAR GALVAAN', 'ZIYAAD GALVAAN', 'WC', '131 WETTON ROAD', '021 762 1123', '084 580 0954', 'wc.wetton@freshstop.co.za; zgalvaan@gmail.com', 1, '2020-07-30 10:37:04'),
(26, 26, 0, 'FRESHSTOP MIDDELBURG ( SAME AS 177)', '312170', NULL, '1d93275', '', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 1, '2020-07-30 10:37:04'),
(27, 27, 176, 'FRESHSTOP CORPORATE PARK', '336300', NULL, '4122947', 'GS', 'THABO MARUMA', 'PHELADI MOJAPELO', 'INL', 'OLD JOHANNESBURG ROAD', '011 314 3596', '083 222 6985', 'caltex@saharaonline.co.za; gau.corporatepark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(28, 28, 54, 'FRESHSTOP BUNYAN ST', '333272', NULL, '3fb7a4f', 'RC', 'LINCOLN PHAKISI', 'CECELIA MPHAHLELE', 'INL', '4 BUNYAN STREET', '114224646', '083 476 6916 / 083 747 2103', 'bunyan@icon.co.za; bunyan@titimagroup.com; lincolnp@titimagroup.com\nlincolp@titimagroup.com \n', 1, '2020-07-30 10:37:04'),
(29, 29, 125, 'FRESHSTOP WASHINGTON POST', '312170', NULL, '7d527a6', 'MN', 'EBRAHIM SAMARIA', 'MUHAMMAD', 'INL', '53 OLD JOHANNESBURG ROAD', '016 428 1881', '082 903 7668', 'ebrahim.samaria@yahoo.com; gau.washingtonpost@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(30, 30, 55, 'FRESHSTOP BUFFELSPOORT (ALSO KNOWN AS BUFFELSPARK)', '312709', NULL, '0c72601', 'NB', 'LINKIE STAEGEMANN', 'HENRY SCHUTTE', 'INL', 'BUFFELSPARK CENTRE, R104', '014 572 3118', '076 445 7770', 'linkie@buffelspark.co.za; gau.buffelspoort@freshstop.co.za; accounts@buffelspark.co.za', 1, '2020-07-30 10:37:04'),
(31, 31, 189, 'FRESHSTOP STELLENDALE (CORPORATE)', '356726', NULL, 'c4a0f94', 'HL', 'CORPORATE STORE', 'DENICE MCLAINE', 'WC', 'CNR SUNVALLEY & STELLENDALE ', '219030104', '076 568 7623(S)', 'wc.stellendale@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(32, 32, 190, 'FRESHSTOP WETLANDS', '315301', NULL, '53fe3e1', 'AMA', 'JERUSHA PERUMAL & MARINUS DE KOK', 'RENIER', 'WC', 'C/O KOEBERG & BLAAUWBERG ROAD', '021 557 9451', '072 313 2094 / 082 664 2591', 'wetlands@mweb.co.za; wc.wetlands@freshstop.co.za; jerusha.perumal@gmail.co.za; marinusdk@gmail.co.za', 1, '2020-07-30 10:37:04'),
(33, 33, 157, 'FRESHSTOP N4 ORCHARDS', '315489', NULL, '39f6774', 'HMcD', 'PRUDENCE MASHABA', 'JULIET', 'MPU N', '999 C/O DU PREEZ', '013 755 6054', '082 494 6373', 'mpu.orchards@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(34, 34, 0, 'FRESHSTOP KOMATIEPOORT ', '314631', NULL, '45457bf', 'HMcD', 'MAS CORPORATION', 'CORNELIUS', 'MPU N', '19 RISSIK STREET', '013 793 8159', '079 500 2540', 'komatipoort@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(35, 35, 1, 'FRESHSTOP CYRILDENE ', '352677', NULL, 'bfd96bf', 'RC', 'VINESH REDDI / RICHARD ', 'JULIAN / JACKIE', 'INL', '1 COOPER ROAD', '011 615 1494', '071 885 6939', 'cyrildene@freshstop.co.za;  vineshreddi@gmail.com', 1, '2020-07-30 10:37:04'),
(36, 36, 191, 'FRESHSTOP PENLYN', '312748', NULL, '29d9ab5', 'SD', 'LEE or NAFISA PARKER', 'LEE PARKER', 'WC', '285 BELGRAVIA RD', '021 691 1973', '072 618 2755', 'penlyn@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(37, 37, 0, 'FRESHSTOP THE DOWNS SERVICE STATION', '338639', NULL, '21d4b56', '', 'PATRICIA KHANYILE', 'CHRISTIAN SIBANDA', 'INL', 'CNR N1 & WILLIAM NICOL ROAD, EPSOM DOWNS', '117067144', '082 461 3880', 'thedowns@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(38, 38, 107, 'FRESHSTOP JOHN ROSS', '313485', NULL, '660b20e', 'LH', 'MIKE FRENCH', 'WADE FRENCH', 'KZN N', 'CNR OLD TANNER/JOHN', '035 772 4721', '083 630 5073', 'johnross@freshstop.co.za; frenchm@iwirelessafrica.com; delijohnross@freshstop.co.za; wade@frenchgroup.co.za', 1, '2020-07-30 10:37:04'),
(39, 39, 24, 'FRESHSTOP GONUBIE', '313829', NULL, '285977b', 'CB', 'ADRIAN MULLER', 'TREVOR MULLER', 'EC', 'MAIN ROAD', '043 732 1155', '083 793 1984', '0823033855@vodamail.co.za; gonubie@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(40, 40, 192, 'FRESHSTOP HOWARD CENTRE ', '336292', NULL, '43b7d84', 'AMA', 'TERENCE SMITH', 'MONIQUE SMITH', 'WC', 'FOREST DRIVE, PINELANDS', '215314760', '083 564 1872', 'hcmotors@absamail.co.za; hcmotors2@absamail.co.za; howardcentre@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(41, 41, 226, 'FRESHSTOP MALANSHOF - PRESENTLY CLOSED', '315930', NULL, 'a1d51d7', 'AG', 'MOHAMED KHAN', 'TITAS', 'INL', 'CNR PHILLIP LE ROUX & HANS SCHOEMAN STREETS MALANSHOF', '011 791 4909', '076 167 6860', 'caltexmalanshof@telkomsa.net; malanshof@freshstop.co.za; mohammed@maxibuild.co.za', 1, '2020-07-30 10:37:04'),
(42, 42, 193, 'FRESHSTOP MASAKHANE', '315354', NULL, '13a0a2a', 'AMA', 'BEN VOLSCHENK ', 'MARE NEL', 'WC', '15 BOSMANSDAM ROAD', '021 551 8662', '082 771 9360', ' benvol@telkomsa.net; bvolschenk@gmail.com; masakhaness@freshstop.co.za; mk@bluepump.co.za', 1, '2020-07-30 10:37:04'),
(43, 43, 194, 'FRESHSTOP MILMAR ', '315286', NULL, '0ba8229', 'AMA', 'GERRIT KNOETZE ', 'KHAUL', 'WC', 'CNR FREEDOM AVE & KOEBERG ROAD', '215510479', '083 638 9828', 'gknoetze@gmail.com; milmar@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(44, 44, 0, 'FRESHSTOP UXOLO ', '314419', NULL, 'ea5ea74', '', 'LENNOX GESHA /MZUZUKILA & BUYISWA VIKA', 'LENNOX GESHA', 'WC', 'NEW LANDSDOWNE RD, SITE C', '213616133', '078 878 9176', 'lennoxgesha@yahoo.com; caltexuxolo@yahoo.com; khayelitsha@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(45, 45, 82, 'FRESHSTOP LE MANS', '315374', NULL, '694e47e', 'MC', 'MERVYN GOVENDER / FAHIM ANWARY', 'N/A', 'KZN', '74 KENYON HOWDEN ROAD', '031 462 1082', '082 899 4338 / 084 551 5520', 'lemans@mweb.co.za; le-mans@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(46, 46, 0, 'FRESHSTOP ELEPHANT COAST ', '314098', NULL, 'cec25bd', 'LH', 'ANDRIES GEYSER', 'EVERT', 'KZN N', '881 KWAMSANE TOWNSHIP', '035 551 0077', '083 269 2755', 'andries@zp.co.za; neale@zp.co.za; eccc@zp.co.za\nneale@zp.co.za\n', 1, '2020-07-30 10:37:04'),
(47, 47, 163, 'FRESHSTOP GOLF CLUB TERRACE', '352159', NULL, '1e578a4', 'AG', 'PUSELETSO BRIDGETTE NTLOEDIBE', 'PUSELETSO BRIDGETTE NTLOEDIBE', 'INL', '144 GOLF CLUB TERRACE', '011 475 8110', '083 744 0771', 'gcterrace@freshstop.co.za; puseletsontloedibe@yahoo.com', 1, '2020-07-30 10:37:04'),
(48, 48, 19, 'FRESHSTOP CATO RIDGE', '312872', NULL, '1a38aa6', 'AA', 'PAUL MILLER ', 'RICCARDIO CARVALHO  ', 'KZN S', '99 OLD MAIN ROAD', '031 782 1788', '065 800 1589', 'catoridge@freshstop.co.za; luis@cdtcontainers.co.za; rcarvalho@freshstop.co.za;garage.miller@gmail.com', 1, '2020-07-30 10:37:04'),
(49, 49, 0, 'FRESHSTOP FOURWAY ', '313747', NULL, '8628234', 'RVB', 'JOSE', 'NINA', 'EC', 'CNR DISCOVERY & YORK STREET ', '448780315', '083 273 3768', 'fourway@mweb.co.za', 1, '2020-07-30 10:37:04'),
(50, 50, 110, 'FRESHSTOP WINDERMERE', '313204', NULL, '6643bfd', 'TM', 'RICHARD BOUFFE', 'CAREN BOUFFE', 'KZN', 'CNR FLORIDA & ARGYLE ROADS', '313122156', '082 446 2748', 'windcal@saol.com; windermere@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(51, 51, 111, 'FRESHSTOP CAVERSHAM', '315950', NULL, '26316ae', 'AA', 'SIDNEY ROUSSEAU', 'SHIRLEY ROUSSEAU', 'KZN', '60 CAVERSHAM ROAD, PINELANDS', '031 702 9588', '082 496 8321', 'caversham@cybertrade.co.za; caversham@freshstop.co.za; sthelierstud@tekomsa.net', 1, '2020-07-30 10:37:04'),
(52, 52, 173, 'FRESHSTOP MALUTI MOTORS', '313636', NULL, '01bfe5a', 'DH', 'PIET DE LANGE', 'DOUGLAS STEVENSON', 'OFS', '98 McCABE STREET', '051 933 2969', '082 879 1781', 'malutimotors@freshstop.co.za; pieter@malutimotors.co.za; ', 1, '2020-07-30 10:37:04'),
(53, 53, 26, 'FRESHSTOP BEACON BAY MOTORS', '312254', NULL, 'b055784', 'CB', 'GREG BIND', 'LAUREN BIND', 'EC', '1 BONZA BAY ROAD', '043 748 2207', '083 644 6133', 'beaconbay@freshstop.co.za; gregorybind@gmail.com; laurenbind@gmail.com', 1, '2020-07-30 10:37:04'),
(54, 54, 112, 'FRESHSTOP PREMIER SERVICE STATION', '315886', NULL, 'f08445a', 'AA', 'LEE SVEN BENTZ', 'LEATITIA MEYER', 'KZN S', '238 CHIEF ALBERT LUTHULI STREETS', '033 394 4075', '082 774 7487 ', 'admin@premierpmb.co.za;lbentz@freshstop.co.za;lmeyer@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(55, 55, 122, 'FRESHSTOP THONGASI ', '313819', NULL, '55bc2bc', 'AA', 'WAYNE NEL & MELISSA NEL', 'GARTH NEL & WAYNE JR', 'KZN', 'CNR R61 & KINDERSTRAND ROAD', '039 319 2940', '071 679 2522', 'thongazi@freshstop.co.za; garth.n@hotmail.com; ', 1, '2020-07-30 10:37:04'),
(56, 56, 104, 'FRESHSTOP NORTH BEACH', '313320', NULL, 'ba7c74e', 'TM', 'NASIM PARAK', 'NONHLAHLA NTWANE', 'KZN', '30 PLAYFAIR RD', '313327044', '083 577 8609', 'nasimparak@freshstop.co.za; northbeach@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(57, 57, 0, 'FRESHSTOP THE WAVES ', '317703', NULL, 'b2134d6', 'RP', 'JULIUS DE JAGER', 'JULIUS DE JAGER', 'EC', '2242 SOUTH STREET ', '448771122', '082 563 1922', 'gawievh@gmail.com; the waves@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(58, 58, 115, 'FRESHSTOP GILLITTS', '313791', NULL, 'ec12257', 'MC', 'PAUL MILLER & LUIS FARIAS ', 'NATASHA & SHELLY', 'KZN', '15 OLD MAIN ROAD', '317671356', '072 413 6518 / 076 060 4875', 'gillits@freshstop.co.za; gss@gillittsfreshstop.co.za', 1, '2020-07-30 10:37:04'),
(59, 59, 73, 'FRESHSTOP FOREST DRIVE', '314851', NULL, '661b591', 'TM', 'EUGENE RAJAH', 'VAL', 'KZN', '40 FOREST DRIVE MOTORS', '315628433', '082 450 1802', 'forestdrive@freshstop.co.za; eugene.rajah@gmail.com', 1, '2020-07-30 10:37:04'),
(60, 60, 195, 'FRESHSTOP MOTORLAND', '312827', NULL, '841c791', 'AMA', 'NAZEER HASSAN', 'ASHRAF ENUS', 'WC', '262 VOORTREKKER ROAD', '021 939 3010', '083 695 8685 / 076 405 6656', 'motorland@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(61, 61, 106, 'FRESHSTOP MAIN ROAD ', '315957', NULL, 'a1bfac1', 'AA', 'KHALEEM MAJID', 'RAZIA / ZANDILE', 'KZN', '87 JOSIAH GUMEDE ROAD', '031 701 1338', '083 786 0561 ', 'ashraffamajid@gmail.com; klm@mwebbiz.co.za; razia.caltex@gmail.com; mainroad@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(62, 62, 11, 'FRESHSTOP VAAL STREET MOTORS', '333447', NULL, 'c11d5d3', 'MN', 'AUBREY JOHNSON', 'VISHNU DASS', 'INL', '58 VAAL STREET', '011 900 1547', '083 509 0019', 'info@vaalmotors.co.za; vaalmotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(63, 63, 196, 'FRESHSTOP HOUT BAY', '314123', NULL, 'd41fed8', 'SD', 'NAEEM', 'ASAD MIA', 'WC', 'MAIN ROAD', '021 790 2030', '082 963 4744 / 081 366 2233', 'houtbay@freshstop.co.za; naeem@sulnisa.co.za', 1, '2020-07-30 10:37:04'),
(64, 64, 197, 'FRESHSTOP FISH HOEK', '313643', NULL, 'eae7033', 'AMA', 'PATRICK VAN ZYL ', 'PATRICK VAN ZYL ', 'WC', '31 MAIN ROAD', '217824136', '082 772 3870', 'fishhoek@freshstop.co.za; autocenta@telkomsa.net; patlucy@webafrica.org.za', 1, '2020-07-30 10:37:04'),
(65, 65, 198, 'FRESHSTOP KENILWORTH', '314400', NULL, 'b3286eb', 'SD', 'MOHAMED EBRAHIM', 'DONNA LENTING', 'WC', '305A MAIN STREET', '021 762 2605', '083 309 1728 / 078 670 1163', 'mebrahim@absamail.co.za; kenilworth@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(66, 66, 78, 'FRESHSTOP KIBLER PARK', '314422', NULL, '168512b', 'MN', 'NOBLEMAN KANI', 'MARSHAL MAKWANZA', 'INL', 'C/O GORDON & MAIN ROAD', '011 943 4207/8/9', '083 691 0636', 'kiblerpark@freshstop.co.za; nobleman.kani@amajabhusi.co.za', 1, '2020-07-30 10:37:04'),
(67, 67, 199, 'FRESHSTOP OAKDALE MOTORS', '312796', NULL, 'c751abf', 'AMA', 'DAVID TEUBES', 'CARLA TEUBES, JW DE KLERK, RICARDO', 'WC', '261 DURBAN ROAD', '219191013', '082 444 6431 / 081 056 1008', 'oakdale@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(68, 68, 119, 'FRESHSTOP SUBWAY MOTORS', '315202', NULL, 'f889416', 'TM', 'ESSEN NAIDOO', 'VISHEN', 'KZN', '13 BRICKFIELD ROAD', '312073380', '083 776 6641', 'subwaymotors@mweb.co.za;subway@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(69, 69, 200, 'FRESHSTOP PEAK SERVICE STATION', '312812', NULL, 'f02583c', 'AMA', 'FARIED ISMAIL', 'FARIED ISMAIL', 'WC', '42 ORANGE STREET', '214220549', '083 278 7222', 'peak@telkomsa.net; peak@freshstop.co.za ', 1, '2020-07-30 10:37:04'),
(70, 70, 0, 'FRESHSTOP CEDAR ROAD', '316669', NULL, '5593f49', 'GS', 'JASON BEIRA', 'ARTHUR NCUBI', 'INL', '10 CEDAR ROAD', '114650966', '082 880 1602', 'cedarroad@mweb.co.za; cedarroad@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(71, 71, 56, 'FRESHSTOP HL MOTORS', '333494', NULL, 'ec1822e', 'RC', 'MITCH MADGIDSON', 'ESTELLE VAN NIEKERK', 'INL', '784 PRINCE GEORGE AVENUE', '011 817 1035', 'N/A', 'h.l.motors@absamail.co.za; hlmotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(72, 72, 97, 'FRESHSTOP MOTORSERVE', '313202', NULL, '5a553d4', 'TM', 'MUNEEN ESSOP', 'MOHAMMED', 'KZN', '3 PINE STREET', '313329791', '083 327 8690', 'motoserv@freshstop.co.za; motorserv@telkomsa.net', 1, '2020-07-30 10:37:04'),
(73, 73, 201, 'FRESHSTOP WINCH MOTORS', '312763', NULL, '49bedc0', 'AMA', 'MATTHEW LAZARUS / MICHAEL SMITH', 'MICHAEL SMITH / MATTHEW LAZARUS', 'WC', '161 BUITENGRAGHT STREET', '214244968', '082 686 2555 / 084 516 4480(M)', ' winchmotors@winchmotors.co.za', 1, '2020-07-30 10:37:04'),
(74, 74, 27, 'FRESHSTOP TOBY\'S', '316037', NULL, 'f13dca7', 'RP', 'JOHN PARRY', 'JJ JOHNSTONE', 'EC', 'CNR 8TH AVE & MAIN ROAD', '041 581 4352', '072 306 1373', 'tobysmotors@telkomsa.net; tobys@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(75, 75, 2, 'FRESHSTOP BIRCHPARK', '333015', NULL, 'ff2b4c5', 'RC ', 'SINGATWA MNQANDI', 'GABRIEL', 'INL', '62 KOEDOE AVENUE', '119724218', '082 381 4909', 'singatwamnqandi@outlook.com; birchpark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(76, 76, 128, 'FRESHSTOP WILLIAM MOFFETT SERVICE STATION', '316114', NULL, '41fc161', 'RP', 'MICHAEL BRIGGS', 'SHARON JORDAN-JOUBERT', 'EC', 'C/O WILLIAM MOFFET EXPRESSWAY & OVERBAAKENS ROAD', '041 368 4525', '083 212 4422 / 072 247 2003', 'mbriggs@telkomsa.net; williammoffett@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(77, 77, 44, 'FRESHSTOP COWEY CENTRE MOTORS', '312350', NULL, '2764bdb', 'TM', 'YOUNUS CASSIM', 'BRENDA', 'KZN', '107 COWEY ROAD', '312073413', '083 778 6706', 'younus@coweymotors.co.za; cowey@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(78, 78, 57, 'FRESHSTOP SAFARI (LOUIS TRICHARDT) knocked down and re-build', '333625', NULL, 'd78c08a', 'NB', 'JURIE MEYER', 'ALMA GOEDCH', 'INL', '54 JOAO ALBASSINI STREET', '155161420', '082 555 8475', 'safarimotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(79, 79, 29, 'FRESHSTOP WESTERN MOTORS', '317434', NULL, 'bc8cd1a', 'CB', 'WAYNE OSBORNE', 'BRETT OSBORNE', 'EC', '79 WESTERN AVENUE', '043 726 3274', '083 654 0094    ', 'wosborne@iafrica.com; westernmotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(80, 80, 30, 'FRESHSTOP WAVELANDS', '314224', NULL, '32d1493', 'RP', 'VINCENT TEE', 'ANDRE KRIEL', 'EC', '57 ST FRANCIS ROAD', '422933466', '082 777 8745', 'vincent@climaxconcrete.co.za; wavelands@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(81, 81, 227, 'FRESHSTOP CHRISTIAAN DE WET', '314101', NULL, '4b78071', 'AG', 'TREVOR VISVANATHAN', 'LORINDA', 'INL', '89 CHRISTIAAN DE WET ROAD,RANDPARK RIDGE. ', '011 794 5279', '074 448 1394', 'christiaandw@freshstop.co.za; caltexcdw@telkomsa.net', 1, '2020-07-30 10:37:04'),
(82, 82, 0, 'FRESHSTOP JAN SMUTS', '370997', NULL, '0b5b5c4', 'AMA', 'SHANAAZ MAGIET', 'TASHREQUE ISMAIL', 'WC', 'LANSDOWNE CORNER MALL, JAN SMUTS DRIVE', '021 703 3273', '082 773 2271 / 076 052 0737', 'jansmuts@freshstop.co.za; lansdowne@freshstop.co.za; tashreque@cncm.co.za', 1, '2020-07-30 10:37:04'),
(83, 83, 31, 'FRESHSTOP COLLEGE MOTORS', '313852', NULL, '3b6e516', 'RVB', 'PIERRE LE GRANGE', 'STEPHAN DE SOUZA', 'EC', '18-20 COLLEGE ROAD', '049 892 4151', '076 869 8294', 'gfrdriveway@iexchange.co.za; collegemotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(84, 84, 121, 'FRESHSTOP BIRD SANCTUARY', '312905', NULL, 'ce78803', 'AA', 'TREVOR VISVANATHAN', 'ROGER MUNSAMY/ RUSHIL CHHOTEYLAL/ JAN BOTES', 'KZN S', '9 ARMITAGE ROAD', '033 345 8484', '083 634 3940 / 082 302 9508 / ', 'rushil@trevisfuel.com; trevor@trevisfuel.com; roger@trevisfuel.com; jan.botes@trevisfuel.com', 1, '2020-07-30 10:37:04'),
(85, 85, 5, 'FRESHSTOP BELLAIR', '312282', NULL, '912e56c', 'TM ', 'ALTAAF MEER / ISMAIL KAKA', 'HAROUN', 'KZN', '938 SARNIA ROAD', '314656856', '084 585 1209', 'bellairfuel@mweb.co.za; bellair@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(86, 86, 0, 'FRESHSTOP NICOLE\'S', '314812', NULL, '11360af', 'SD', 'MALCOLM NASSON', 'JANICE', 'WC', 'CNR NOOIENSFONTEIN & GROVE STREET, OAKDENE', '218256263', '086 537 4963', 'malcolmnasson@vodamail.co.za; nicoles@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(87, 87, 32, 'FRESHSTOP DUNCAN VILLAGE ', '313175', NULL, 'a16ac94', 'CB', 'ADRIAN MULLER', 'ADRIAN MULLER ', 'EC', 'DOUGLAS SMITH HIGHWAY ', '043 742 7057', '083 793 1984', '0823033855@vodamail.co.za; duncanvillage@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(88, 88, 123, 'FRESHSTOP GLENVIEW SERVICE STATION', '313812', NULL, '0d5a24a', 'TM', 'MIKE HARCOURT', 'STEVEN BOAST ', 'KZN', '43 ASHLEY AVENUE', '315723172', '082 573 0917 / 0833018413', 'glenviewss@vodamail.co.za; glenview@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(89, 89, 4, 'FRESHSTOP ATHOLL HEIGHTS', '336324', NULL, 'ab3c2cd', 'MC', 'SANTOSH PAARDOOTMAN', 'SHAMILLA', 'KZN', '174 - 178 BLAIR ATHOLL DRIVE', '312621000', '082 907 0707 / 082 298 7770(Av', 'atholl@telkomsa.net; santosh@sansher.co.za', 1, '2020-07-30 10:37:04'),
(90, 90, 202, 'FRESHSTOP LANDSDOWNE ROAD', '370998', NULL, '4d15d49', 'AMA', 'SHANAAZ MAGIET', 'TASHREQUE ISMAIL', 'WC', 'GOVAN MBEKI ROAD', '021 703 3273', '076 052 0737 / 083 254 8649', 'shanaaz@africape.co.za; lansdowne@freshstop.co.za; saeed@cncm.co.za', 1, '2020-07-30 10:37:04'),
(91, 91, 165, 'FRESHSTOP NORTH STAR', '333517', NULL, 'eda6339', 'GS', 'FAROOK/MR ALLIE', 'RANDY SINGH', 'INL', '199 WITKOPPEN RD', '114653812', '083 446 0522', 'northstar@coalresource.co.za;shanab@telkomsa.net; northstar@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(92, 92, 118, 'FRESHSTOP STAR COURT', '315432', NULL, 'b868ae8', 'MC', 'DEENA MOODLEY / VIJAY NAIDOO', 'VINCENT', 'KZN', '1 HILLHEAD DRIVE ', '315025707', '084 409 8156', 'starcourt@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(93, 93, 126, 'FRESHSTOP STAG MOTORS', '338812', NULL, '4aa16cc', 'MC', 'MUZI HADEBE', 'MPUMELELO', 'KZN', '522 SMITH STREET', '313077993', '082 493 5749', 'hadebe@global.co.za', 1, '2020-07-30 10:37:04'),
(94, 94, 12, 'FRESHSTOP FARRAR PARK', '333120', NULL, '80e742b', 'MN', 'BERNARD SCHWARTZ', 'RINA', 'INL', 'C/O RONDEBULT & ROBINSON ROAD', '118965111', '083 702 6783', 'belssch@global.co.za;  farrarpark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(95, 95, 0, 'FRESHSTOP SALT RIVER (GILPET MOTORS)', '316653', NULL, 'ee3acab', 'SD', 'SABRIE MOSAVAL', 'BOBBY TREMBLE', 'WC', '261 VICTORIA ROAD', '214484999', '065 241 8271', 'dsscaltex@gmail.com; ', 1, '2020-07-30 10:37:04'),
(96, 96, 58, 'FRESHSTOP GARSFONTEIN MOTORS', '333535', NULL, 'dace59f', 'NB', 'TIMON & LELANI BODENSTEIN', 'SARIE PRETORIUS', 'PTA', '469 WINIFRED YELL STR', '123489196', '082 339 1656 ', 'timonbdn@gmail.com; lelanivdwesthuizen@gmail.com; garsfontein@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(97, 97, 0, 'FRESHSTOP KZN MOTORS', '316433', NULL, '9fb19c2', 'LH', 'DAVE APPEL', 'N/A', 'KZN N', '7 PREMIUM PROMONADE', '035 789 8665', '082 785 8269', 'kznmotors@intekom.co.za; kznmotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(98, 98, 120, 'FRESHSTOP SUNWARD SERVICE STATION', '312511', NULL, '57f5f4d', 'MN', 'NAEEMA ISMAIL', 'ALAIN', 'INL', '300 KINGFISHER STREET, SUNWARD PARK', '119133616', '082 861 5915', 'sunwardpark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(99, 99, 93, 'FRESHSTOP MOORTON STAR', '312907', NULL, '1c54942', 'MC', 'ESSEN NAIDOO', 'JADE YERRAYA', 'KZN', '39 MOORCROSS DRIVE', '031 406 1354', '083 776 6641', 'moorton@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(100, 100, 14, 'FRESHSTOP RAPID AUTO SERVE  (no: 100)', '333482', NULL, 'd05dd5d', 'MN', 'JATIN MAHIDA', 'BERNARD ', 'INL', 'CNR TRICHARDT & PAUL SMIT', '118943517', '076 142 9433', 'rapidauto@freshstop.co.za;jatinmahida247@gmail.com', 1, '2020-07-30 10:37:04'),
(101, 101, 204, 'FRESHSTOP PRIME PARK SERVICE STATION', '312768', NULL, '2fd4e27', 'SD', 'DAVID NEL', 'YOLANDE', 'WC', 'CNR TIENIE MEYER & LANDROSS STREET', '021 949 3922', '083 310 2449 / 082 760 4546', 'davn@netactive.co.za; primepark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(102, 102, 205, 'FRESHSTOP CARENO SERVICE STATION', '316991', NULL, 'fc83c77', 'SD', 'CASPER BADENHORST', 'MACKEY FADANA', 'WC', 'CNR MAIN ROAD & LOURENS RIVIER ROAD', '218543360', '082 893 4813', 'careno@mainroad.co.za; casper@careno.co.za; careno@freshstop.co.za;admin@careno.co.za;operations@careno.co.za', 1, '2020-07-30 10:37:04'),
(103, 103, 129, 'FRESHSTOP WEST VIEW', '313318', NULL, 'eb4aae3', 'TM', 'SANTOSH PAARDOOTMAN', 'ISAAC', 'KZN', '838 NORTH COAST ROAD', '315643748', '082 907 0707', 'westview@polka.co.za; santosh@sanasher.co.za', 1, '2020-07-30 10:37:04'),
(104, 104, 0, 'FRESHSTOP KINGFISHER SERVICE STATIONTN', '313680', NULL, 'ff4ba5b', 'GS', 'ZAHEER SAYED MOHOMED / YUSUF AU ESSOP', 'YUSUF AU ESSOP', 'INL', 'CNR KINGFISHER DR & ALEXANDRA RD', '117053531', '082 653 7709 / 083 707 1850', 'zaheer@thc.co.za; caltex@thc.co.za; kingfisher@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(105, 105, 206, 'FRESHSTOP AIRPORT CITY', '356068', NULL, '2f36171', 'SD', 'BEN FOURIE', 'ANGELO KING', 'WC', '1 MONTREAL DRIVE', '021  385 0060', '083 651 4919 / 073 832 9803', 'benf@airportfreshstop.co.za; angelo@airportfreshstop.co.za; airportcity@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(106, 106, 207, 'FRESHSTOP GREENWAYS', '316987', NULL, '0a9e9e2', 'SD', 'RIAZ JAFFER', 'SHAMSUNEESA JAFFER', 'WC', 'C/O ONVERWACHT & GORDONS BAY ROAD', '021 853 6614', '083 415 9323 / 083 226 3251', 'riazjaffer@absamail.co.za; greenways@freshstop.co.za; caltexstrand@mweb.co.za', 1, '2020-07-30 10:37:04'),
(107, 107, 77, 'FRESHSTOP AMBER MOTORS', '314384', NULL, '05c6085', 'RC ', 'AZIZ HASSAN', 'GRACE MOYO', 'INL', '3 AMPERE ST', '011 393 6320', '084 418 0708', 'ambermotors@telkomsa.net; ambermotors2@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(108, 108, 0, 'FRESHSTOP ELTO MOTORS', '314814', NULL, '647a66e', 'SD', 'ELBIE DE WITT', 'PIETER', 'WC', 'CNR VAN RIEBEECK & BOTHA STREETS', '219035101', ' 082 771 1189(E)', 'koosgas@gmail.com; eltomotors@freshstop.co.za; pieter@eltomotors.co.za', 1, '2020-07-30 10:37:04'),
(109, 109, 59, 'FRESHSTOP CAPITAL PARK', '335912', NULL, '32ae73c', 'NB', 'ILLZE COETZER ', 'GERHARDT COETZER', 'PTA', '354 PAUL KRUGER STREET', '123285571', '083 780 6000', 'g.coetzer@absamail.co.za; capitalpark@freshstop.co.za; icoetzer@absamail.co.za', 1, '2020-07-30 10:37:04'),
(110, 110, 33, 'FRESHSTOP PE CONVENIENCE', '315794', NULL, '9e66634', 'RP', 'DOU JOHAN PIENAAR', 'ROXANNE SNYMAN', 'EC', '1 WELLS ESTATE', '414611442', 'N/A', 'dou@doupienaar.co.za; peconvenience@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(111, 111, 130, 'FRESHSTOP MODERN TOWERS ', '313249', NULL, 'f78824d', 'MC', 'KEN SMITH', 'KEN', 'KZN', '17-19 BRIGHTON ROAD', '314671834', '073 783 7070', 'moderntower@telkomsa.net; mobenit@telkomsa.net', 1, '2020-07-30 10:37:04'),
(112, 112, 172, 'FRESHSTOP LINKSFIELD TERRACE  - Temp Closed', '313397', NULL, 'd75ccdc', 'GS', 'ZAHIR DESAI', 'ZAHIR DESAI', 'INL', '110 LINKSFIELD DRIVE', '118229022', '083 325 1699', 'linkcaltex@mweb.co.za; Linksfield@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(113, 113, 42, 'FRESHSTOP CHRIS HANI (BRIARDENE)', '371223', NULL, 'cf8b4e6', 'MC', 'ALLAN NAIDU', 'ROCKY', 'KZN', '759 CHRIS HANI ROAD\n', '013 564 8738', '083 441 4127', 'allen@africaz.co.za; chrishani@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(114, 114, 208, 'FRESHSTOP DUINEFONTEIN', '315134', NULL, '3a896b8', 'SD', 'SABRIE MOSAVAL', 'ASHRAF HOOSAIN', 'WC', '172 DUINEFONTEIN ROAD', '021 699 3251', '082 902 7075 / 083 229 6855', 'dsscaltex@gmail.com; ashrafhoosain78@gmail.com; duinefontein@freshstop.co.za;caltexfac@gmail.com', 1, '2020-07-30 10:37:04'),
(115, 115, 15, 'FRESHSTOP HENNICO', '332932', NULL, 'b404b12', 'MN', 'CORNE BOTHA', 'RIA SWANEPOEL', 'INL', 'CNR 5TH AVE & LOUIS TRICHARDT STR', '011 869 8017', '083 305 6126', 'cornes@mweb.co.za; hennico@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(116, 116, 132, 'FRESHSTOP SOUTHGATE', '371224', NULL, 'b64e655', 'TM', 'RICHARD CROCKER', 'MARK HARDMAN', 'KZN', '1 ASHGATE ROAD', '319143657', '072 708 9163', 'mark@vistaconstruction.co.za; richard@vistaconstruction.co.za', 1, '2020-07-30 10:37:04'),
(117, 117, 133, 'FRESHSTOP WINSTON PARK', '371228', NULL, '63dc77e', 'MC', 'LEON KOHNE & BRENDAN DU RANDT', 'PETER PRIEBE', 'KZN', '42 OLD MAIN ROAD', '317672995', '083 284 1093(B) /083 327 6130(', 'leon@bakkiehire.co.za; brendan@bakkiehire.co.za; paulgien75@live.com', 1, '2020-07-30 10:37:04'),
(118, 118, 228, 'FRESHSTOP WENDYWOOD ', '317618', NULL, '4257209', 'GS', 'WARREN OSHRY', 'ANDRIES SEHEYA', 'INL', '5 WESTERN SERVICE ROAD', '011 804 5848', '082 453 0000', 'power@storm.co.za; wendywood@freshstop.co.za;caltexwendywood@telkomsa.net', 1, '2020-07-30 10:37:04'),
(119, 119, 229, 'FRESHSTOP SUMMERFIELD ', '314302', NULL, '9c44a36', 'GS', 'CRAIG FITT', 'OURS MANYAGA', 'INL', 'CNR ROBYN AND PLATINA STREET', '117043881', '082 870 7664', 'summerfields@freshstop.co.za;summerfieldss@telkomsa.net', 1, '2020-07-30 10:37:04'),
(120, 120, 66, 'FRESHSTOP EMFULENI AUTO', '317312', NULL, '55dada7', 'MN', 'MOHAMED ALLY', 'SAMEEA', 'INL', 'CNR TANNER & MACOWAN STREET', '016 932 5667', '082 423 8752', 'emfuleni@freshstop.co.za; ally@diamondcorner.co.za;', 1, '2020-07-30 10:37:04'),
(121, 121, 34, 'FRESHSTOP ALAN HAHN MOTORS', '333956', NULL, '36f0d45', 'CB', 'GEORGE VAN DER WESTHUIZEN', 'DANE VAN DER WESTHUIZEN ', 'EC', '179 CATHCART ROAD', '045 838 3369', '079 190 9916', 'alanhahn@freshstop.co.za; danevanderw@gmail.com', 1, '2020-07-30 10:37:04'),
(122, 122, 17, 'FRESHSTOP IMPALA (BOKSBERG)', '312513', NULL, '3904044', 'MN', 'TBC - NEW RETAILER', 'TBC - NEW RETAILER', 'INL', '47 ROLLS ROYCE STREET', '011 894 6529', '083 650 0140', 'impalaservices@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(123, 123, 109, 'FRESHSTOP PARKVIEW MOTORS', '317320', NULL, '9ea1e00', 'MN', 'MUHAMED ALI', 'LEE SULUMAN / SULI', 'INL', 'C/O FRIKKIE MEYER & NASH STREET', '016 933 6686', '083 786 2002', 'parkviewmotors@telkomsa.net; parkview@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(124, 124, 35, 'FRESHSTOP AMALINDA', '312145', NULL, '81da3bf', 'CB', 'ERHARDT RICHTER', 'ELSIE RICHTER', 'EC', '29 MAIN ROAD', '043 741 1930', '083 381 2515', 'caltexamalinda@telkomsa.net; amalinda@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(125, 125, 0, 'FRESHSTOP ULUNDI', '317173', NULL, '2608574', 'LH', 'ANDRIES GEYSER', 'CHRIS MHANGO', 'KZN N', 'CNR KING SOLOMON & DINIZULU HIGHWAY', '358773000', '083 554 9978', 'andries@zp.co.za; caltexulundi@zp.co.za; chrismhango@me.com; phillip@zp.co.za', 1, '2020-07-30 10:37:04'),
(126, 126, 36, 'FRESHSTOP KNYSNA QUAYS', '314610', NULL, '8eaffe5', 'RVB', 'HEIN GILIOMEE', 'TANYA CILLIERS', 'EC', 'WATERFRONT DRIVE', '044 382 4152 / 4', '084 705 2035 ', 'knysnaquaysss@telkomsa.net', 1, '2020-07-30 10:37:04'),
(127, 127, 167, 'FRESHSTOP CAMBRIDGE COURT', '315793', NULL, 'e86bac7', 'GS', 'BRANDON MORE', 'BRANDON MORE', 'INL', '25 WITKOPPEN ROAD', '011 807 7371', '083 295 1595', 'calcambridge@mweb.co.za; cambridge@freshstop.co.za; calcambridge@mweb.co.za', 1, '2020-07-30 10:37:04'),
(128, 128, 37, 'FRESHSTOP ON YORK', '334267', NULL, '1d04965', 'RVB', 'BIANCA WAGMAN', 'EMMA SMIT', 'EC', '1 YORK STREET, C/O PLATTNER BLVD', '044-873 0903', '082 824 9348 / 076 942 0996', 'york@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(129, 129, 134, 'FRESHSTOP COTSWOLD DOWNS', '371236', NULL, '18c788e', 'MC', 'NICO VAN ROOYEN', 'QUENTIN CRESSWELL', 'KZN', '105 INANDA ROAD', '310070619', '083 776 8233', 'nico@gpisa.co.za; cotswold@freshstop.co.za; management@enicentre.co.za\' quentin@enicentre.co.za; admin1@enicentre.co.za', 1, '2020-07-30 10:37:04'),
(130, 130, 135, 'FRESHSTOP 5 STAR SERVICE STATION', '371226', NULL, 'c9cf265', 'MC', 'AHMED MOHAMED', 'AHMED', 'KZN', '90 FRAGRANCE STREET', '031 403 6222', '072 138 6222', 'fivestar5@telkomsa.net; kruzin.am@gmail.com', 1, '2020-07-30 10:37:04'),
(131, 131, 136, 'FRESHSTOP BAYWATCH', '371227', NULL, '9d23eaf', 'TM', 'ASHLEY / AVI RAMLAKEN', 'LUCY CHINYAURO', 'KZN', '127 MARGARET MNCADI AVE', '031 301 3791', '074 139 9285 / 082 455 8367 / ', 'baywatch@freshstop.co.za; sarika.ramlaken@gmail.com; ashley@angelfootwear.co.za', 1, '2020-07-30 10:37:04'),
(132, 132, 209, 'FRESHSTOP NEW MAITLAND', '312825', NULL, 'eac8b94', 'AMA', 'ABDUL AZIZ VALLIE', 'ABBIGAIL STEYN', 'WC', '254 VOORTREKKER ROAD', '021 511 2022', '083 444 4162 / 082 616 5873', 'abdul@vallie.co.za; newmaitland@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(133, 133, 0, 'FRESHSTOP ETWATWA', '333540', NULL, '41b9b9a', '', 'VUSI MTHIMKHU', 'BETTY', 'INL', '1357 ESSELEN STREET, ETWATWA', '119624702', '082 452 7441', 'etwatwa@mweb.co.za; etwatwa@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(134, 134, 60, 'FRESHSTOP THE ORCHARDS PTA', '316242', NULL, '8118985', 'NB', 'GERRIE LEWIES', 'MERRY NGOBENI', 'PTA', 'C/O DAAN DE WET NEL & DOREEN AVE', '012 549 1770', '082 806 3852', 'ocaltex@future-ent.com;orchardspta@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(135, 135, 3, 'FRESHSTOP ALEXANDRA', '338668', NULL, '6a98f42', 'GS', 'BHARAT BULLAH', 'KEITAN/WILSON', 'INL', 'CNR 10TH AVE AND LONDON ROAD', '113460298', '0715142778-KEITAN', 'ice2000@webmail.co.za; alexandra@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(136, 136, 116, 'FRESHSTOP ROUXVILLE ', '312914', NULL, '67a39c7', 'RC ', 'AADIL GAFOOR ADAM OMARJEE', 'ZARINA', 'INL', '289 LOUIS BOTHA AVE ', '116405143', '082 652 7710', 'zayaan@absamail.co.za; rouxville@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(137, 137, 6, 'FRESHSTOP CHRISVILLE', '312932', NULL, '0d18712', 'RC', 'HARSHAD BHIKHA', 'MOHON', 'INL', 'CORNER SIDE & VIVIENNE STREETS', '011 680 6005', '083 636 5389', 'ccaltex@mymtnemail.co.za; chrisville@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(138, 138, 92, 'FRESHSTOP MOFFATS', '317369', NULL, 'e855930', 'MN', 'ASGHAR YOUNUS', 'NIRAV GAJJAR', 'INL', '42 GENERAL SMUTS', '016 422 2727', '082 787 8659 / 079 748 8841', 'moffatsgarage@gmail.com; moffats@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(139, 139, 61, 'FRESHSTOP MANOR MOTORS', '316191', NULL, 'e6b28ae', 'NB', 'HUGO HANSEN', 'SAUL', 'PTA', '25 LYNNBURN ROAD', '123481363', '082 574 4559', 'hugohanson87@yahoo.com; caltexmanor@mweb.co.za\' mannormotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(140, 140, 137, 'FRESHSTOP QUEENSMEAD', '372406', NULL, 'e10d1b0', 'TM', 'AK PATEL', 'ZAHEER MOTALA', 'KZN', '21 PIET RETIEF ROAD', '031 464 8144', '078 980 3326', 'motalaz@gmail.com; queensmead@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(141, 141, 62, 'FRESHSTOP WATERKLOOF GLEN', '333128', NULL, '62b8f68', 'NB', 'CHARLé BESTER', 'YOLANDI MARAIS', 'PTA', '374 ROSLYN AVENUE', '012 993 5739', '082 850 5842', 'waterkloofglen@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(142, 142, 138, 'FRESHSTOP NU WEST', '315547', NULL, 'd2334f7', 'TM', 'CYRIL LUTHULI', 'STHE LUTHULI', 'KZN', '1 LOOPWEST CRESCENT', '031 578 7969', '082 453 9851', 'sthe.luthuli@telkomsa.net; newwesi@worldonline.co.za', 1, '2020-07-30 10:37:04'),
(143, 143, 210, 'FRESHSTOP DONCASTER', '314402', NULL, 'dd73311', 'AMA', 'YASMIEN DAWOOD', 'SHELLY BUCKTON', 'WC', 'CNR DONCASTER RD & PUNTERS WAY', '021 674 3155', '082 888 6470 / 072 441 1911', 'doncaster@freshstop.co.za; ferialm@freshstop.co.za; yasmine@sulnisa.co.za', 1, '2020-07-30 10:37:04'),
(144, 144, 211, 'FRESHSTOP DELFT', '313081', NULL, 'f35s588', 'SD', 'ZULFIKAAR SOLOMON', 'MAJDY', 'WC', 'CNR MAIN & VUURLELIE CRESCENT', '021 956 4437', '084 322 6655', 'kapsol@mweb.co.za; freshstopdelft@gmail.com; delft@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(145, 145, 212, 'FRESHSTOP KEY MOTORS', '313882', NULL, '1daec05', 'AMA', 'GULAM', 'SAIDI USI', 'WC', '38 KLIP ROAD', '021 705 2349', '083 270 5565 / 084 900 4204(G)', 'goodhopetyresgoodwood@telkomsa.net; keymotors@telkomsa.net; keymotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(146, 146, 213, 'FRESHSTOP 87 ON VICTORIA', '316794', NULL, '20458c6', 'SD', 'COBUS MOSTERT', 'DEON JOOSTE', 'WC', '87 VICTORIA ROAD', '021 851 4854', '823759595', 'caltex.xwest@telkomsa.net; mtyeni95@gmail.com; 87onvictoria@freshstop.co.za; cobusmostert5@gmail.com', 1, '2020-07-30 10:37:04'),
(147, 147, 168, 'FRESHSTOP PARKMORE (WAS FERRUCCIO) ', '316673', NULL, '5cfeaf5', 'AG', 'ELIO & ANNA FAUSTINO', 'LAURA FAUSTINO & OLMINA BASTIOTTO ', 'INL', 'CNR 11TH STREET & OLYMPIA AVE', '011 783 2815', '062 039 9355', 'ferrucio@telkomsa.net; ferruccio@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(148, 148, 7, 'FRESHSTOP FESTIVAL MALL ', '312703', NULL, 'f8ac067', 'RC ', 'FRANS LOMBARD', 'MATETE', 'INL', 'CNR KELVIN & SWART ROAD', '117818312', '082 457 0699 /083 273 4049', 'kemptonpark@fuelarama.co.za,fuelarama@worldonline.co.za', 1, '2020-07-30 10:37:04'),
(149, 149, 0, 'FRESHSTOP NELA KHALE (Ballantine)', '317194', NULL, 'c47d704', 'MC', 'BERNARD CHETTY', 'CEBISILE LANGA', 'KZN', '240 ZWE MADLALA DRIVE', '031 906 4400', '084 693 3128 ', 'ballentine@freshstop.co.za; caltexumzi@gmail.com', 1, '2020-07-30 10:37:04'),
(150, 150, 177, 'FRESHSTOP ZHUT CITY (NOW  ORLANDO STADIUM ) ', '315677', NULL, '19c0059', 'GS', 'GEORGE NKOSI', 'AUBREY', 'INL', '12519 KLIPVALLEY ROAD', '011 936 1213', '082 822 6666', 'georgie@icon.co.za', 1, '2020-07-30 10:37:04'),
(151, 151, 140, 'FRESHSTOP UMKOMAAS FUEL N ALL', '334100', NULL, '38c1c09', 'AA', 'MANITHA SINGH', 'VANESSA CHETTY', 'KZN S', '117 CRAIGIEBURN ', '039 979 7012', '083 789 1603', 'manithasingh@yahoo.com; fuelnall@telkomsa.net; ', 1, '2020-07-30 10:37:04'),
(152, 152, 0, 'FRESHSTOP ALBERANTE', '333023', NULL, '5ee23d7', 'MN', 'HENDRIK JANSEN', 'RIKA HOWARD', 'INL', '37 PENZANCE STREET', '011 861 8193', '082 293 0933', 'hendrik.jansen@bmwdealer.co.za; rika.howard@alberante.co.za; alberante@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(153, 153, 50, 'FRESHSTOP STAR SERVICES', '333289', NULL, '98fa0f0', 'MN', 'HARSHAD', 'ALEX', 'INL', 'C/O JG STRYDOM & HENNIE ALBERTS ', '011 900 3456', '083 843 9480', 'starservices@freshstop.co.za; hb1@vodamail.co.za', 1, '2020-07-30 10:37:04'),
(154, 154, 214, 'FRESHSTOP SPINE ROAD', '314415', NULL, '8bbdb9e', 'SD', 'HEYDER EBRAHIM', 'NURSHAD', 'WC', 'CNR OF SPINE & PHAKAMANI RD', '021 361 9037', '084 261 6618 / 063 615 6678', 'spineroad@freshstop.co.za; heyder@discoverymail.co.za', 1, '2020-07-30 10:37:04'),
(155, 155, 230, 'FRESHSTOP FAIRLANDS', '333601', NULL, '1ee0dc4', 'AG', 'SHAUKAT HASSEN', 'ARNOLD CHINAMATIRA', 'INL', '114-14TH AVENUE', '011 476 5201', '082 831 1660', 'busicor92@mweb.co.za; fairlands@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(156, 156, 141, 'FRESHSTOP HILLCREST', '314094', NULL, 'd263db0', 'MC', 'PAUL MILLER & LUIS FARIAS', 'WELLBELOVED MKHILA', 'KZN', '65 OLD MAIN ROAD', '031 765 5335', '082 574 7083 / 082 856 5155', 'luis@cdtcontainers.co.za; caltexhillcrest@freshstop.co.za;', 1, '2020-07-30 10:37:04'),
(157, 157, 51, 'FRESHSTOP NORTHPOINT', '333718', NULL, '1a5de02', 'MN', 'LOUIS VD WESTHUIZEN', 'CLARA', 'INL', 'CNR TRICHARD & DASSEN RDS', '011 894 1537', '082 893 4095', 'fueltrader@mweb.co.za; northpoint@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(158, 158, 215, 'FRESHSTOP OKAVANGO SERVICE STATION', '314725', NULL, '07ce1d0', 'SD', 'LATIEFA ASHTIKER', 'JUANITA MOUTON', 'WC', 'CNR OKAVANGO & VATICAN RDS ', '021 975 8194', '082 879 8733 / 071 568 8073', 'okavangocal@mweb.co.za; okavango@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(159, 159, 159, 'FRESHSTOP PARK MOTORS', '317737', NULL, '65f3f5d', 'HMcD', 'RADESH RAMPERSAD', 'BAREND GREYVENSTEYN', 'MPU N', 'C/O MANDELA & JELLICPE ROAD', '013 656 4624', '061 438 6731', 'parkmotors@mweb.co.za; parkmotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(160, 160, 160, 'FRESHSTOP GROBLERSDAL', '332957', NULL, '74a254f', 'HMcD', 'KARIN VAN DYK', 'MALAN TERBLANCHE', 'MPU N', '18 HEREFORD STREET', '013 262 4061', '082 592 7089', 'karin@tmnissan.co.za; groblersdal@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(161, 161, 161, 'FRESHSTOP WITRIVIER MOTORS', '317681', NULL, '0cdd74c', 'HMcD', 'LLEWELLYN NEL', 'JAN', 'MPU N', 'CNR TOM LAWRENCE & THEO KLEYNHANS STS', '013 751 3160', '071 606 0329', 'ldh_nel@mweb.co.za; witrivier@freshstop.co.za; caltexwitrivier@telkomsa.net', 1, '2020-07-30 10:37:04'),
(162, 162, 169, 'FRESHSTOP CUMBERLAND', '312694', NULL, '92281c4', 'GS', 'THABO SERETSE', 'THABISO MAAKE', 'INL', '113 CUMBERLAND AVE', '011 069 8193', '083 775 1863', 'thabos@mweb.co.za , cumberland@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(163, 163, 38, ' FRESHSTOP GREENBUSHES', '311777', NULL, '41666d8', 'RP', 'JURIE SNYMAN', 'DEWALD NIEMAND', 'EC', '1 OLD CAPE ROAD', '041 372 1972', '082 859 9111', 'greenbushes@freshstop.co.za; greenbushes@mweb.co.za', 1, '2020-07-30 10:37:04'),
(164, 164, 52, 'FRESHSTOP WONDERPARK', '337015', NULL, '6a1c397', 'NB', 'KOOS LIEBENBERG', 'THABISO NDWAMMBI', 'PTA', 'CNR HEINRICH & BRITS ROAD', '012 549 1562', '082 562 5141 / 071 849 1719', 'kliebenberg@freshstop.co.za; wonderpark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(165, 165, 63, 'FRESHSTOP NUFFIELD', '338817', NULL, 'f500c41', 'RC', 'DAWIE ERNST & JOHNEY DIPELA', 'PAPI', 'MPU S', '15 HEWITT AVENUE', '011 818 5836', '083 476 6916', 'nuffield@freshstop.co.za; dawie@ferthal.co.za; papi@dipnora.co.za', 1, '2020-07-30 10:37:04'),
(166, 166, 64, 'FRESHSTOP CONSTANTIA', '333438', NULL, '29e66c1', 'GS', 'GERHARD ERASMUS', 'NEIL LEROUX', 'PTA', 'CNR SOLOMON MAHLANGU & JANUARY MASISELA RDS', '012 751 4999', '083 250 6202', 'sgerasmus@telkomsa.net; constantiapark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(167, 167, 142, 'FRESHSTOP LIGHTHOUSE', '317188', NULL, '375d048', 'TM', 'GARY DAVID SYREN', 'RAYMOND', 'KZN', '2 LIGHTHOUSE ROAD', '031 561 1041', '083 787 1042', 'lhss@telkomsa.net; lighthouse@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(168, 168, 170, 'FRESHSTOP GARDEN SERVICES', '316332', NULL, '77bcbc2', 'AG', 'MITCH MADGIDSON', 'MALUSI ', 'INL', '3 MALIBONGWE DRIVE', '011 782 2938', '074 219 1485', 'gardens@freshstop.co.za; andrewg.oilpack@gmail.com', 1, '2020-07-30 10:37:04'),
(169, 169, 143, 'FRESHSTOP NORTHLANDS', '313308', NULL, 'c45dbe5', 'TM', 'GREAME CAMPBELL', 'WARREN THORNELL', 'KZN', '4 MACKEURAN AVENUE', '031 564 2096', '083 294 0359', 'northlands@telkomsa.net; northlands@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(170, 170, 171, 'FRESHSTOP MONUMENT PARK', '315376', NULL, '7b52607', 'GS', 'MIKE DI CICCIO', 'JENNIFER WHITE', 'INL', '50 ELEPHANT STR', '012 460 7221', '084 362 5366', 'mikediciccio@absamail.co.za;monumentpark@freshstop.co.za; jenniferwhite875@gmail.com', 1, '2020-07-30 10:37:04'),
(171, 171, 178, 'FRESHSTOP RUIMSIG', '316560', NULL, 'fdf938e', 'AG', 'MITCH MADGIDSON', 'DOUGLAS KENAOPE', 'INL', 'C/O HENRDICK POTGIETER & DOREEN RD', '011 958 2341', '074 219 1485', 'caltexruimsig@mweb.co.za; ruimsig@freshstop.co.za, andrewg.oilpack@gmail.com', 1, '2020-07-30 10:37:04'),
(172, 172, 0, 'FRESHSTOP RABIE STREET', '373639', NULL, 'c6132da', 'AG', 'BASHIR JASSAT', 'MOHAMED', 'INL', 'CNR RABIE & 2ND STREET, FONTAINEBLEAU', '011 792 9281', '083 2939 9005', 'jasbro@iafrica.com; rabie@freshstop.co.za;bashir@bluecubesa.com; ', 1, '2020-07-30 10:37:04'),
(173, 173, 39, 'FRESHSTOP MACLEAR', '333946', NULL, '8877487', 'LD', 'LYALL & LOUISE WICKS', 'DEWALD MARTENS', 'EC', 'Cnr Rugby & Van Riebeeck St, Maclear, Eastern Cape', '045 932 1076', '082 492 0040  / 082 851 8331', 'lwicks2@xsinet.co.za;maclear@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(174, 174, 144, 'FRESHSTOP TOTI OASIS', '372488', NULL, 'a115f0d', 'TM', 'AHMED HANSA', 'SAMEERA', 'KZN', '458  ANDREW ZONDA ROAD', '031 903 1626', '082 461 2222', 'ahmed@amusement.co.za;to@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(175, 175, 174, 'FRESHSTOP DOORNPOORT', '313160', NULL, '94670e0', 'NB', 'JAAP LEE', 'MICHELLE MATTHEE', 'PTA', '503 AIRPORT ROAD,DOORNPOORT', '012 547 0658', '082 565 3452', 'doornpoort@freshstop.co.za; doornpoort@bluepump.co.za', 1, '2020-07-30 10:37:04'),
(176, 176, 8, 'FRESHSTOP FLORENTIA', '312114', NULL, '6e49c1f', 'MN', 'MARIUS VAN DER RYST', 'MICHELLE VAN DER RYST', 'INL', '42 KRITZINGER AVENUE', '011 907 9181', '082 411 9045', 'florentia@freshstop.co.za; mvdryst@mweb.co.za', 1, '2020-07-30 10:37:04'),
(177, 177, 162, 'FRESHSTOP MIDDELBURG (IMPALA)', '315241', NULL, '0a99cd6', 'HMcD', 'ALMERO CALITZ', 'GERHARDT', 'MPU N', '161 COWEN NTULI', '132430681', '082 664 4082', 'fuel@mweb.co.za', 1, '2020-07-30 10:37:04'),
(178, 178, 216, 'FRESHSTOP VOORBRUGH', '313080', NULL, 'c1ce321', 'SD', 'DAWOOD NOORGAT', 'ZAKARIYA NOORGAT', 'WC', 'CNR MAIN & VOORBRUG ROADS', '021 954 4406', '084 556 9901 / 083 929 6926', 'dnnorgot@telkomsa.net; voorbrug@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(179, 179, 40, 'FRESHSTOP FRONTIER', '338438', NULL, '57d8a98', 'CB', 'P.L FISCHER', 'CORY / DIANA', 'EC', 'CNR OF LIMPOPO & CATHCART STREET', '045 858 8839', '076 346 3129 / 083 290 4294 / ', 'frontiercx@telkomsa.net; frontier@freshstop.co.za; frontiercx1@gmail.com', 1, '2020-07-30 10:37:04'),
(180, 180, 85, 'FRESHSTOP LOUIS BOTHA', '372971', NULL, '4256d00', 'RC', 'AADIL GAFOOR ADAM OMARJEE', 'EVAH', 'INL', '350 LOUIS BOTHA', '011 485 0164', '082 652 7710', 'louisbotha@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(181, 181, 145, 'FRESHSTOP BLUFF', '313252', NULL, '6334c71', 'MC', 'WAYNE NEL ', 'DERRICK DE LANGE', 'KZN', '1 CHERWELL ROAD', '031 466 1191', '083 543 8848', 'bluff@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(182, 182, 98, 'FRESHSTOP MOTORSERVE RANDBURG', '316324', NULL, '4eeff5e', 'AG', 'MOHAMED DEEDAT', 'ZIYAAD DEEDAT', 'INL', '160 HENDRIK VERWOERD DRIVE', '011 886 9984', '082 826 1701', 'caltexmotorserve@cybersmart.co.za;motorserver@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(183, 183, 146, 'FRESHSTOP AVOCA / DON RENNIE', '336322', NULL, 'b5e62b1', 'MC', 'AJITH RAMLUCKAN', 'RAJESH RAMPERSADH', 'KZN', '1455 NORTH COAST ROAD', '031 569 3317', '082 264 5108', 'rdm108@webmail.com;avoca@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(184, 184, 175, 'FRESHSTOP COLBYN', '333281', NULL, '2231b14', 'NB', 'ILLZE COETZER', 'JAN MOGOTSI', 'PTA', '1340 STANZA BOBAPE STREET(AKA KERKSTRAAT)', '012 430 2533', '083 780 6000', 'colbyn@freshstop.co.za; icoetzer@absamail.co.za', 1, '2020-07-30 10:37:04'),
(185, 185, 94, 'FRESHSTOP  BRITS', '312671', NULL, 'b803dfa', 'NB', 'CASPER SNYDERS', 'ISELLE SNYDERS', 'INL', '59 HENDRICK VERWOERD AVENUE', '012 252 0217', '083 708 4898', 'casper@caltexbrits.co.za;machamazaan@gmail.com', 1, '2020-07-30 10:37:04'),
(186, 186, 147, 'FRESHSTOP KOKSTAD', '314624', NULL, 'e719de8', 'LD', 'OWEN PETERS', 'JANICE KITCHING', 'EC', '78 HOPE STREET', '039 727 2577', '082 564 6492 / 083 385 0384', 'janice@kilroes.co.za;ojpeters@vodamail.co.za; kokstad@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(187, 187, 95, 'FRESHSTOP COY\'S', '333187', NULL, '38527ef', 'MN', 'TONY RIBEIRO', 'SUSAN KAY', 'INL', '1 VOORTREKKER ROAD', '011 907 9373/4', '082 866 6612 / 082 459 9231', 'coys@netdial.co.za; coys@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(188, 188, 148, 'FRESHSTOP UMLAZI MEGA CITY', '317192', NULL, '92fd770', 'MC', 'MUZZEE HADEBE', 'PLATO', 'KZN', '50 MANGOSUTHU HIGHWAY', '031 912 1737', '082 493 5749', 'hadebe@global.co.za', 1, '2020-07-30 10:37:04'),
(189, 189, 90, 'FRESHSTOP MIDWAY MOTORS', '333111', NULL, 'a37d298', 'DH', 'SUHAIFA HOOSAIN', 'FERIEDA', 'KIM', '59 BULTFONTEIN ROAD', '053 832 2912', '082 394 5097 / 082 474 7465', 'midway@freshstop.co.za;hoosain99@gmail.com; suhaifa11@gmail.com; drzhoosain@cyberscope.co.za', 1, '2020-07-30 10:37:04'),
(190, 190, 96, 'FRESHSTOP GLENANDA', '313804', NULL, 'd6b0921', 'RC', 'HARSHAD BHIKHA', 'BHAVESH', 'INL', 'CORNER VORSTER & SURMON STREET', '011 432 3600', '083 636 5389', 'Hb1@mymtnmail.co.za', 1, '2020-07-30 10:37:04');
INSERT INTO `sites` (`id`, `id_no`, `location_id`, `site_name`, `site_number`, `slip_number`, `qr_code`, `bc`, `retailer`, `manager`, `area`, `address`, `landline`, `cell_no`, `email`, `status`, `timestamp`) VALUES
(191, 191, 124, 'FRESHSTOP VAN WYK', '336380', NULL, '28fbc61', 'AG', 'RIAZ OSMAN', 'FAROUK', 'INL', '9 VAN WYK STREET', '011 760 1035', '083 578 6571', 'caltexvanwyk@gmail.com;vanwykstreet@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(192, 192, 149, 'FRESHSTOP CANEHAVEN', '312736', NULL, '108eb64', 'MC', 'ABEN NAIDOO', 'ANIRA HARRY / DENVER', 'KZN', '134/136 CANEHAVEN DRIVE, CANESIDE', '031 506 1569', '081 597 1802', 'aben.mvhd@gmail.com; canehaven@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(193, 193, 217, 'FRESHSTOP COBBLEWALK', '352930', NULL, 'fdd3d7c', 'AMA', 'COENRAD FOURIE', 'ELMAR FOURIE', 'WC', 'CNR DE VILLIERS DRIE & VERDI', '021 979 2261', '083 388 4140', 'elmarfourie@gmail.com; cantexcw@gmail.com', 1, '2020-07-30 10:37:04'),
(194, 194, 69, 'FRESHSTOP ENERGY STOP', '333511', NULL, '0ded3dc', 'GS', 'ZIYAAD MOOZA', 'IMRAN ALLY', 'INL', '21 VAN RIEBEECK AVENUE', '011 453 1629', '072 585 1513', 'energystop@freshstop.co.za;ziyaadm@octagon.za.com', 1, '2020-07-30 10:37:04'),
(195, 195, 218, 'FRESHSTOP CONCORD', '336441', NULL, 'a1dda8c', 'SD', 'OCKERT KOHNE', 'OLIVE NOSHAUTA / CHARLES SYMON', 'WC', '92 SIR LOWRY ROAD', '021 465 2967', '079 883 6091 / 063 287 7417 / ', 'ockert@solidas.co.za;concord@solidas.co.za; concord@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(196, 196, 65, 'FRESHSTOP DENNE MOTORS', '313527', NULL, '074e51d', 'HMcD', 'HASHIM MAHIDA', 'CASSIM', 'MPU S', '5 JOUBERT STREET', '017 819 3545', '082 635 6000', 'hmahida25@gmail.com', 1, '2020-07-30 10:37:04'),
(197, 197, 67, 'FRESHSTOP CONLEE MOTORS', '333473', NULL, '85d6bda', 'NB', 'JACOB FRANCOIS LEE (JAAP LEE)', 'CHARISKA VERSTER', 'PTA', '1052 NICO SMITH STREET', '012 3330128', '062 488 6565', 'conlee@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(198, 198, 68, 'FRESHSTOP MULTICHECK', '332954', NULL, '4d334fc', 'GS', 'LESLIE GOODMAN', 'LESLIE GOODMAN', 'INL', '14 VAN RIEBEECK AVENUE', '011 453 9106/7', '082 490 9254', 'hiqmulticheck@gmail.com;multicheck@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(199, 199, 88, 'FRESHSTOP LYNDHURST', '315047', NULL, '6ddfb69', 'RC', 'YONI WOLPE', 'RICHARD', 'INL', '44 JOHANNESBURG ROAD', '011 440 1806', '082 339 2358', 'lyndhurst@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(200, 200, 70, 'FRESHSTOP IVY PARK', '382545', NULL, 'c7ee652', 'NB', 'DAVID de JAGER', 'HANLIE DREYER', 'LIM', 'C/O MARSHALL & NELSON MANDELA STREET', '015 295 2220', '084 811 0213', 'dejagerd@mweb.co.za;ivypark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(201, 201, 41, 'FRESHSTOP HUNTERS RETREAT', '316065', NULL, 'a042749', 'RP', 'GORDON MACDONALD', 'CATHY  JORDAN-JOUBERT', 'EC', 'OLD CAPE ROAD', '041 360 1221', '082 444 3689', 'caltexhr@telkomsa.net; huntersretreat@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(202, 202, 150, 'FRESHSTOP WARNER BEACH', '317537', NULL, 'f7c0dba', 'TM', 'HASHEEM ASMAL', 'BILAL-SURAYA-YUSUF', 'KZN', '34 KINGSWAY', '031 916 1712', '083 324 6863', 'amallhash@gmail.com;altexca@gmail.com;caltexca@gmail.com; warnerbeach@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(203, 203, 13, 'FRESHSTOP BISHO (RAITHUSI)', '314485', NULL, 'c48fc82', 'CB', 'AHMED LIMBADA', 'MAHMED PIPERDI', 'EC', '8908 MAITLAND ROAD', '043 643 6001', '083 300 4248', 'raithusi@telkomsa.net; raithusi@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(204, 204, 0, 'FRESHSTOP HAMBURG MOTORS', '313924', NULL, '8aeb54b', 'AG', 'ROSS MACGREGAR', 'NKOSI', 'INL', '87 ALBERTINA SISULU STREET', '011 472 1047', '082 938 6777', 'ross@macfeul.co.za; elize@macfuel.co.za; hamberg@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(205, 205, 72, 'FRESHSTOP OSBORNE', '333493', NULL, '1b6f1b5', 'MN', 'OMAR ALLY', 'LOLLA', 'INL', 'OSBORNE ROAD WADEVILLE', '011 824 2919', '076 627 2559', 'osborne@freshstop.co.za; omar.ally@telkomsa.net', 1, '2020-07-30 10:37:04'),
(206, 206, 117, 'FRESHSTOP SILMARS', '314249', NULL, 'f0388c2', 'GS', 'MARCO LEOTTA', 'DANIELLA LEOTTA', 'INL', '74 CENTRAL AVE', '011 837 8791', '082 566 2323 ', 'leotta@icon.co.za', 1, '2020-07-30 10:37:04'),
(207, 207, 151, 'FRESHSTOP BRACKENHAM', '333140', NULL, 'a66b80c', 'LH', 'G. BISSESSUR / A. SINGH / P. SINGH', 'ELIZABETH NEL', 'KZN N', '81 VIA DAVALLIA', '035 798 1124', '079 220 6341', 'brackenham@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(208, 208, 99, 'FRESHSTOP TREK-IN', '333275', NULL, 'ab0bf7d', 'DH', 'ERNEST MACDONALD', 'ERMA', 'OFS', 'C/O DU TOIT & RAUTENBACH STREETS,KROONHEUWEL', '056 212 1837', '082 800 1271', 'trekin@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(209, 209, 100, 'FRESHSTOP DE GRAAFS ( M&D FILLING STATION )', '315474', NULL, '58db23f', 'HMcD', 'ADRIANO', 'ADRIANO', 'MPU N', '6 PAUL KRUGER STREET', '013 752 3303', '071 147 2044', 'mdfillingstation@gmail.com;md.fillingstation@gmail.com', 1, '2020-07-30 10:37:04'),
(210, 210, 101, 'FRESHSTOP FALCKVLEI', '333140', NULL, 'a05fd6f', 'DH', 'JEAN KHOURI', 'CORNEL MELLET', 'OFS', '157 CHURCH STREET', '051 448 4333', '083 448 7080', 'falckvlei@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(211, 211, 102, 'FRESHSTOP FLIMIEDIA', '333054', NULL, '29ab38a', 'DH', 'EDWIN DE KOCK', 'ANNE BOTMA', 'INL', '36 LAUTZ LAAN', '018 468 2010', '084 599 1264', 'flimieda@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(212, 212, 16, 'FRESHSTOP BRENTHURST AUTO', '336443', NULL, 'c49118c', 'RC', 'B A DELAIR', 'MAGMOOD', 'INL', '579 PRINCE GEORGE AVE', '011 740 9866', '082 551 1176', 'brenthurstss@telkomsa.net; brenthurst@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(213, 213, 219, 'FRESHSTOP MONTAGUE GARDENS', '315367', NULL, '6f0e2cc', 'AMA', 'BRIAN BUCKTON', 'GARTH BUCKTON', 'WC', 'CNR FREEDOM WAY & MONTAGUE DRIVE', '021 551 0607', '072 242 8018', 'caltexmg@telkomsa.net; montaguegardens@freshstop.co.za; buckton@telkomsa.net', 1, '2020-07-30 10:37:04'),
(214, 214, 75, 'FRESHSTOP BUSHBUCKRIDGE', '377406', NULL, 'f92ebb7', 'HMcD', 'KOBUS KLOPPER', 'DONOVAN', 'MPU N', 'A319 ALONG R40, MAIN ROAD', 'N/A', '072 394 8343', 'gruopops1@ithubaletu.co.za;  bushbuckridge@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(215, 215, 43, 'FRESHSTOP EDEN', '313748', NULL, '1e92904', 'RVB', 'ADELE LOUIS', 'EDDIE LOUIS', 'EC', '13 COURTENAY STREET', '044 873 5689', '083 662 1600', 'eden@freshstop.co.za; allouis07@gmail.com', 1, '2020-07-30 10:37:04'),
(216, 216, 0, 'FRESHSTOP MARLBOROUGH', '314726', NULL, '492010b', 'SD', 'JOHAN MILLER', 'MARKO STEENKAMP', 'WC', '22 MARLBOROUGH WEG', '021 988 6898', '073 204 8102', 'malborough@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(217, 217, 256, 'FRESHSTOP SIYATHA', '336391', NULL, '0e3057c', 'CB', 'ZOLA NOGAYA', 'ANATHI MAGQALA', 'EC', '1384 NU 15 ', '043 763 8019', '083 389 7667', 'Zolile.nog@gmail.com; siyatha@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(218, 218, 152, 'FRESHSTOP CROMPTON', '313198', NULL, '0e2f2dd', 'MC', 'GREG MILLER & SHAUN OLIVER', 'MANDY', 'KZN', '136 CROMPTON STREET', '031 701 4324', '083 387 6626', 'crompton@freshstop.co.za;greg.cromptonmotors@icloud.com', 1, '2020-07-30 10:37:04'),
(219, 219, 76, 'FRESHSTOP XANADU', '333836', NULL, '6d660b8', 'NB', 'MITCHEL MADGIDSON', 'SIMON MOTAU', 'INL', 'R511 MAIN ROAD, XANADU ECO PARK ESTATE', '012 259 1854', '078 973 6444', 'caltexxanadu@telkomsa.net; xanadu@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(220, 220, 290, 'FRESHSTOP STAR CIRCLE', '314104', NULL, '33b6a17', 'AG', 'ZOLANI LAMANI', 'TINASHE MATEWE', 'INL', '24 CNR PETER RD & BEYERS NAUDE DRIVE HONEYDEW', '011 795 2352', '073 337 1102', 'Winnie.lamani@gmail.com;freshstopstarcircle@gmail.com', 1, '2020-07-30 10:37:04'),
(221, 221, 49, 'FRESHSTOP CRYSTAL PARK', '333516', NULL, 'fd6d91a', 'RC', 'ASHRAF MOOSA', 'FATIMA KLEMP', 'INL', '109 TOTIUS RD ', '011 894 7541', '082 522 2557', 'crystalpark@freshstop.co.za;Wembley1@mweb.co.za', 1, '2020-07-30 10:37:04'),
(222, 222, 153, 'FRESHSTOP BIZANA', '314419', NULL, '5530ec5', 'LD', 'ASANDA MARAIS', 'COLIN NDLOVU', 'EC', '13 MAIN STREET', '039 251 0692', '082 308 8214 / 076 401 8889', 'caltexbizana@hotmail.com;jayile@telkomsa.net', 1, '2020-07-30 10:37:04'),
(223, 223, 105, 'FRESHSTOP NORTHCLIFF', '338840', NULL, 'ff64052', 'AG', 'NAZIR LOONAT', 'SHANE OSMAN', 'INL', '131 GORDON ROAD', '011 477 0549', '082 888 0200', 'zimmfuel@gmail.com; northcliff@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(224, 224, 0, 'FRESHSTOP MOLOPO', '4883', NULL, '07f64c7', 'NB', 'YUSUF AMEER', 'YUSUF AMEER', 'INL', '54 SHAPPARD ROAD', '018 381 0818', '083 786 1802', 'molopo@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(225, 225, 79, 'FRESHSTOP EAGER MOTOTS', '315332', NULL, 'd25dd2e', 'NB', 'GERT VAN DER WESTHUIZEN', 'IRMA LUDEKE', 'LIM', '90 THABO MBEKI DRIVE', '015 491 4732', '072 416 8704', 'eagermotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(226, 226, 113, 'FRESHSTOP REDSTAR', '378274', NULL, '0369818', 'RP', 'RAFEEQ MOOSAGIE', 'ZAHEER WAHED', 'EC', '292 DURBAN ROAD', '041 451 1066', '082 786 5810', 'redstar@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(227, 227, 71, 'FRESHSTOP EXCELSIOR', '333100', NULL, '640eb12', 'GS', 'JAAP-LEE', 'NADINE', 'INL', '108 LOUIS TRICHARDT STREET', '012 664 6592', '082 565 3452', 'excelsior@freshstop.co.za; excelsior@bluepump.co.za', 1, '2020-07-30 10:37:04'),
(228, 228, 154, 'FRESHSTOP ISIPINGO', '313300', NULL, '057c48f', 'MC', 'DENVER GOVENDER', 'THEMBELIHLE LANGA', 'KZN', '3 THOMAS LANE ', '031 919 2042', '082 648 0741', 'ctxisipingo@gmail.com; isipingo@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(229, 229, 81, 'FRESHSTOP REPUBLIC ROAD', '338436', NULL, 'e9b769e', 'AG', 'PATRICIA KHANYILE', 'MATHIAS', 'INL', 'CNR REPUBLIC & SURREY ROAD', '011 787 3333', '', 'republicroad@freshstop.co.za; mwdrpnk@mweb.co.za', 1, '2020-07-30 10:37:04'),
(230, 230, 0, 'FRESHSTOP NORTHCREST', '333951', NULL, 'a50f80b', 'LD', 'JO BERLYN', 'LUTHANDO QANGULE', 'EC', 'N2 MAIN DURBAN ROAD', '047 534 0029', '082 871 3315 / 063 311 6385', 'northcrest@freshstop.co.za; jo@fortgale.co.za', 1, '2020-07-30 10:37:04'),
(231, 231, 108, 'FRESHSTOP ONE UP', '333018', NULL, 'eb2cb37', 'GS', 'ROCKERFELLER MAKHUBO', 'REFILOE MAKHUBO', 'INL', '1153 SONTONGA ROAD,KHUMALO VALLEY', '011 903 1643', '083 380 2172', 'oneup@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(232, 232, 83, 'FRESHSTOP PHALABORWA', '334005', NULL, 'fb86d06', 'HMcD', 'LEON KOETZE', 'HENRY BOJE', 'MPU N', '51 ESSENHOUT STREET', '015 781 2870', '071 488 6277', 'phalaborwa@freshstop.co.za; phalaborwafuelcity@gmail.com', 1, '2020-07-30 10:37:04'),
(233, 233, 84, 'FRESHSTOP JABAVU', '333594', NULL, '21a6374', 'GS', 'LATA NGOASHENG', 'PRIMROSE NOWA', 'INL', '9090 MLANGENI & LEFOKO STREET', '087 222 6615', '0822609216', 'caltexjabavu@gmail.com; brasparks@gmail.com', 1, '2020-07-30 10:37:04'),
(234, 234, 221, 'FRESHSTOP CALEDON', '379132', NULL, '20a11fc', 'HL', 'CORPORATE STORE', 'CHRISTO KUHN', 'WC', '1 NERINA WAY', '028 214 1164', '061 711 9689', 'caledon@freshstop.co.za;ckuhn@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(235, 235, 46, 'FRESHSTOP DANIELS', '317156', NULL, '41abd9b', 'RP', 'ZUNE DANIELS', 'ZUNE DANIELS', 'EC', '40 MOSEL ROAD', '041 992 1770', '082 977 9682', 'bzdaniels@telkomsa.net; daniels@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(236, 236, 114, 'FRESHSTOP ROUTE 59 (STANDARD CHAMCOR)', '332933', NULL, 'f2a0742', 'MN', 'CHARL MARAIS', 'ROBERT', 'INL', 'JOHAN LE ROUX DRIVE', '081 704 9420', '082 513 3017', 'Route59@mweb.co.za; route59@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(237, 237, 86, 'FRESHSTOP BOOYSENS', '377755', NULL, '0ff4f13', 'RC', 'SAMEER AMEEN VALLY', 'LANDELENI', 'INL', 'CORNER LONG STREET & BOOYSENS ROAD', '011 493 0111', '071 022 9776', 'sameer@ewt.za.com; booysens@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(238, 238, 87, 'FRESHSTOP A CLUB MOTORS', '315218', NULL, '4d91ba3', 'NB', 'DONALD MAURICIO', 'KELLY MAURICIO', 'PTA', '273 LYNNWOOD ROAD, CNR LYNNWOOD& BRROKLYN RDS', '012 362 1469', '076 691 9738 / 074 337 0677', 'donald201004@hotmail.com; kellyckho@gmail.com; aclub@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(239, 239, 47, 'FRESHSTOP VAN HALDEREN', '314607', NULL, '716ddaf', 'RVB', 'NICOLA ERASMUS', 'JOHAN DE BEER', 'EC', '1 MAIN ROAD', '044 382 3966', '082 871 4070 / 079 181 5527', 'vanhalderen@freshstop.co.za; nerasmus@freshstop.co.za; jdebeer@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(240, 240, 131, 'FRESHSTOP WITBANK MOTORS', '332841', NULL, 'c4345f1', 'HMcD', 'JATIN MAHIDA', 'BERNARD ', 'MPU N', '36 BEATTY STREET', '013 656 1215', '082 554 8865', 'witbankmotors@gmail.com;jatinmahida247@gmail.com', 1, '2020-07-30 10:37:04'),
(241, 241, 89, 'FRESHSTOP WAPADRAND', '333429', NULL, '4d96965', 'NB', 'GERRIE LEWIES', 'BRAAM SMITH', 'PTA', '886 WAPADRAND ROAD', '012 807 0624', '082 806 3852', 'braam@future-ent.com; wapadrand@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(242, 242, 18, 'FRESHSTOP BUCCLEUCH SERVICE STATION', '312707', NULL, '7a9d6a7', 'GS', 'ROB MCLEOD', 'JOHANNES', 'INL', '13 BUCCLEUCH DRIVE', '011 802 4655', '082 358 7733', 'buccleuchss@telkomsa.net', 1, '2020-07-30 10:37:04'),
(243, 243, 48, 'FRESHSTOP BELL MOTORS', '333957', NULL, '0357933', 'CB', 'GARTH ALLY', 'GARTH ALLY', 'EC', '22 GRIFFITH STREET', '045 838 3904', '083 500 0354', 'garthallytrading@telkomsa.net; bellss@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(244, 244, 91, 'FRESHSTOP ANDRE\'S SERVICE STATION', '333652', NULL, '5193b35', 'DH', 'JODIE VAN WYK', 'BIANCA', 'KIM', '47 TRANSVAAL ROAD', '873 105 296', '079 882 5453', 'clar@telkomsa.net; andres@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(245, 245, 0, 'FRESHSTOP MARGATE', '315172', NULL, 'd599628', 'AA', 'ADNAAN MOOSA', 'RAYMOND', 'KZN S', '249 MAIN ROAD', '0393121162', '0619652044', 'margate@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(246, 246, 80, 'FRESHSTOP LA ROCHE', '316035', NULL, 'b560fe0', 'RP', 'ANTON KILLAN', 'ELZETTE MARAIS', 'EC', '2 FOREST HILL DRIVE', '041 585 9999', '061 181 5320', 'kiliana@telkomsa.net; laroche@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(247, 247, 155, 'FRESHSTOP ALICE STREET', '313228', NULL, '97eff55', 'TM', 'RAAJ TEWARY', 'DES NULLIAH', 'KZN', '31 JOHANNES NKOSI STREET', '031 309 5011', '083 445 1123', 'alicestreet@freshstop.co.za; des.nulliah@gmail.com', 1, '2020-07-30 10:37:04'),
(248, 248, 103, 'FRESHSTOP CROYDON ( no 250)', '333510', NULL, '325c10a', 'RC', 'LESLEY MASIA', 'ABIGAIL', 'INL', 'CNR BRABAZON & ISANDO RD', '011 974 3513', '083 258 2988', 'lesley@caltexcroydon.co.za; masial@mweb.co.za', 1, '2020-07-30 10:37:04'),
(249, 249, 231, 'FRESHSTOP ISLAND SERVICE STATION', '315617', NULL, '979771d', 'RC', 'MICHA LENHOFF', 'GRANT BENJAMIN', 'INL', '82/84 NORTH STREET C/O OAK STREET', '011 435 4620', '082 900 6546', 'isand.s@mweb.co.za', 1, '2020-07-30 10:37:04'),
(250, 250, 232, 'FRESHSTOP DELTA MOTORS', '312485', NULL, '934f5f0', 'DH', 'JOHN GAVIN', 'JOHN GAVIN', 'OFS', '80 ZASTRON STREET', '051 433 7155', '083 543 7744', 'john@globalwise.co.za ; delta@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(251, 251, 233, 'FRESHSTOP UMTENTWENI', '317204', NULL, '1ea9d1c', 'AA', 'SHIKHAR SINGH & PHUMZILE MBAMBO', 'LINDA NDLOVU', 'KZN S', 'CORNER LINKS ROAD AND R102', '039 695 0402', '083 785 5000', 'umtentweni@freshstop.co.za; malundi_excursions@yahoo.com; phumzile.mbambo2222@gmail.com', 1, '2020-07-30 10:37:04'),
(252, 252, 234, 'FRESHSTOP DAVEYTON - Temp Closed ', '338365', NULL, '2b616d3', 'RC', 'MS PEARL MORIFI', 'ROSE MOKDENA', 'INL', 'CNR EISELEN & TURTON STR, DAVEYTON, 1520', '083 324 2222', '074 178 2798', 'daveyton@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(253, 253, 114, 'FRESHSTOP 9 SEFAKO DRIVE', '333104', NULL, '0616643', 'NB', 'JAAP LEE', 'DIAAN VAN DEN BERG ', 'PTA', '9 SEFAKO MAKGATHO DRIVE, SINOVILLE, ', '082 967 5968', '082 565 3452', 'sefako@bluepump.co.za; 9sefakodrive@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(254, 254, 236, 'FRESHSTOP FORTGALE', '333953', NULL, '2d74dc4', 'LD', 'JO BERLYN', 'LORENZO GOODMAN', 'EC', 'SUTHERLAND STREET', '087 943 4783', '082 871 3315 / 072 507 7899', 'fortgale@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(255, 255, 237, 'FRESHSTOP GEORGE MOTORS', '316033', NULL, 'd82d76a', 'RP', 'ANDRE RADEMAN', 'CAROL RADEMAN', 'EC', 'C/O STRUANWAY & DAKU ROAD', '041 452 6854', '082 341 0148', 'carolrademan@yahoo.com; george@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(256, 256, 238, 'FRESHSTOP WITKOPPEN MOTORS', '333113', NULL, '00649ea', 'GS', 'BRENDAN ROBINSON', 'XANFER ALLERS', 'INL', '2 PIETER WENNING ROAD', '011 465 3847', '082 451 2885', 'brendan@fuel24seven.co.za ; manager@fuel24seven.co.za; witkoppen@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(257, 257, 239, 'FRESHSTOP DOBSONVILLE', '316806', NULL, '03e86f2', 'GS', 'GEORGE NKOSI', 'TULANI NKOSI', 'INL', '11900 KGANE RD', '011 989 4056', '082 822 6666', 'dobsonville@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(258, 258, 242, 'FRESHSTOP VERULAM', '317413', NULL, 'b4302ac', 'MC', 'AHMED MALL', 'JAY GANASEN', 'KZN', '21/23 IRELAND STREET', '032 533 3717', '073 786 2912', 'verulam@freshstop.co.za; calteximpala1@gmail.com', 1, '2020-07-30 10:37:04'),
(259, 259, 241, 'FRESHSTOP MIDDELBURG PETROL & DIESEL 2', '315238', NULL, '3824735', 'HMcD', 'ALMERO CALITZ', 'LINDY', 'MPU N', '250 CAVEN NTULI STREET', '013 243 2604', '072 630 4099', 'fuel@mweb.co.za; middelburg2@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(260, 260, 240, 'FRESHSTOP GIYANI', '334027', NULL, '5460134', 'HMcD', 'MICHAEL SAMBO', 'NTSAKO MASWANGANYI', 'LIM', '699 MAIN ROAD', '015 812 3069', '073 3688 2747', 'musa@caltexgy.co.za ; ntsako@caltexgy.co.za ; giyani@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(261, 261, 243, 'FRESHSTOP DURBANVILLE', '313324', NULL, 'ce7479e', 'AMA', 'NOELLE ELLIS', 'JOHN TYE', 'WC', '4 WELLINGTON ROAD', '0219763168', '083 565 2002', 'caltexdurbanville@norick.co.za; durbanville@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(262, 262, 244, 'FRESHSTOP STAR STOP METRO PARK', '333477', NULL, '9df3ed6', 'DH', 'BEN MYBURGH', 'NELL VAN BILJON', 'INL', 'N1 HIGHWAY', 'N/A', '065 854 1143 / 074 204 1506', 'metropark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(263, 263, 245, 'FRESHSTOP AJ MOTORS', '315562', NULL, 'f49bcfe', 'RC', 'DAWIE ERNST & JOHNEY DIPELA', 'SONIA SMIDT', 'MPU S', '2 EEUFEES AVENUE, FERRYVALE', '011 814 6533', '082 707 9209 / 073 937 2925', 'ajmotors@freshstop.co.za; ajmotorsnigel@absamail.co.za; dawie@ferthal.co.za; papi@dipnora.co.za', 1, '2020-07-30 10:37:04'),
(264, 264, 246, 'FRESHSTOP TOM JONES', '338636', NULL, '2a04b3d', 'RC', 'ROSS MACGREGAR', 'FELICIA', 'INL', '9 TOM JONES STREET', '011 420 1007', 'N/A', 'ross@macfuel.co.za; elize@macfuel.co.za; tomjones@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(265, 265, 247, 'FRESHSTOP MHLONTLO SERVICE STATION', '383111', NULL, '2ae6207', 'LD', 'SABELO XOTYENI', 'MONICA AND BONGI', 'EC', '167 MAIN ROAD', '047 542 0030 / 0780708604', '061 720 8030 ', 'sabelo.xotyeni73@gmail.com ; bongiwe.xotyeni75@gmail.com ; mhlontlo@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(266, 266, 248, 'FRESHSTOP PARK MOTORS STANDERTON', '333108', NULL, '07bacc4', 'RC', 'MARK VIENINGS / GEORGE VIENINGS', 'MARKS VIENINGS', 'MPU S', '7 BEYERS NAUDE STREET', '017 712 7155', '082 787 8617 / 082 894 1755', 'sparkmotors@freshstop.co.za ; mark@2brosventures.co.za ; agpc@mweb.co.za', 1, '2020-07-30 10:37:04'),
(267, 267, 249, 'FRESHSTOP CONTINENTAL', '312691', NULL, 'aef9c41', 'AMA', 'DRIES LUBBE', 'RONEL OOSTHUIZEN', 'WC', '281 KOEBERG ROAD', '021 511 4787', '082 453 9395 / 072 677 5264', 'drieslub@absamail.co.za; continental@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(268, 268, 250, 'FRESHSTOP KARA GLEN', '313402', NULL, 'ca9eb5f', 'RC', 'PAM HARGOVAN', 'NAVIN', 'INL', '50 BAKER STR, CNR HARRIS AVE', '011 452 4416', '082 3727 938', 'pamh@icon.co.za; karaglen@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(269, 269, 251, 'FRESHSTOP COOKHOUSE', '352918', NULL, '6aeebb1', 'CB ', 'WOLFGANG GAISER', 'SYLVIA GAISER', 'EC', '1 SOMERSET ROAD', '042 247 2525', '082 957 5300', 'wolfg@r63.co.za; cookhouse@freshstop.co.za; watson@r63.co.za', 1, '2020-07-30 10:37:04'),
(270, 270, 253, 'FRESHSTOP NOVA MOTORS', '333674', NULL, '3186283', 'HMcD', 'WERNER', 'WERNE', 'MPU N', '44 WATERMEYER STREET', '0136925280', '071 682 8944', 'novamotors@freshstop.co.za; Caltexnovamotors4@gmail.com', 1, '2020-07-30 10:37:04'),
(271, 271, 0, 'FRESHSTOP RONTREE SERVICE STATION (CAMPSBAY)', '379800', NULL, '373d144', 'SD', 'MALCOLM MICHEL', 'ANDREW GARDINER', 'WC', '2A RONTREE AVENUE', '021 4381458', '071 142 8446 / 074 219 1485', 'rontree@freshstop.co.za; radical@mweb.co.za; oilpackoffice@oilpack.co.za', 1, '2020-07-30 10:37:04'),
(272, 272, 57, 'FRESHSTOP SAFARI MOTORS', '333625', NULL, '949dfec', 'NB', 'ALMA GOETSCH', 'GEOFF GOETSCH', 'LIM', '54 HLANAGANANI STREET', '015 516 1420', '082 410 1670', 'safarimotors@freshstop.co.za; safarimotors2@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(273, 273, 255, 'FRESHSTOP ALICE', '381076', NULL, '6b21f2c', 'CB', 'SHAUN MILLER', 'NEIL CRAFFORD', 'EC', '1 BRIDGE STREET', '040 653 0091', '082 885 7056', 'neil@caltexalice.co.za; alice@freshstop.co.za; admin@caltexalice.co.za', 1, '2020-07-30 10:37:04'),
(274, 274, 0, 'FRESHSTOP TASK NISSAN BARBERTON', '332839', NULL, 'bc77228', 'HMcD', 'DESIRé VAN DER HOVEN', 'LOUISA BUITENDACH', 'MPU N', '119 DE VILLIERS STREET', '013 712 2843', '076 440 4454', 'desire@tasknissan.co.za; tasknissan@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(275, 275, 258, 'FRESHSTOP ST LUCIA', '381104', NULL, '63d078f', 'LH', 'ASHLEIGH MOODLEY', 'CELESTE JARDIM', 'KZN N', '76 PELIKAAN STREET', '035 590 1375', '071 081 9357', 'stlucia@freshstop.co.za ; caltexstlucia@gmail.com', 1, '2020-07-30 10:37:04'),
(276, 276, 257, 'FRESHSTOP MOSSELBAAI', '313991', NULL, 'e98ae91', 'RVB', 'CARL GUSE', 'CHARIN GUSE', 'EC', '11 GERICKE STREET', '044 695 1833', '072 957 5896', 'carlguse@icloud.com; charinguse@mweb.co.za; mosselbaai@freshstop.co.za, ', 1, '2020-07-30 10:37:04'),
(277, 277, 259, 'FRESHSTOP CLB GARAGE', '382157', NULL, 'f50ef1e', 'HMcD', 'HERTZOG LANDMAN', 'HER-JOHAN BOONZAAIER', 'MPU', 'FARM LETABADRIFT', '', '082 901 8265', 'hertzog@clbfruits.co.za; herjohan@mweb.co.za; Clbfruits@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(278, 278, 260, 'FRESHSTOP MARLANDS SERVICE STATION ', '314136', NULL, '9100ca4', 'RC', 'MITCHELL DEAN MAGIDSON', 'ALLAN', 'INL', '58 BARBARA ROAD', '011 828 8053', '082 903 3937', 'marlands5858@gmail.com; homesteadsstation@gmail.com; marlands@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(279, 279, 261, 'FRESHSTOP KINGSWAY ', '336335', NULL, '54115b7', 'TM', 'RIAZ DHAI', 'ELAINE SOOBRATHIE', 'KZN S', '2 GUS BROWN ROAD', '031 916 1517', '084 530 5183', 'caltexonkingsway@gmail.com; rdhai@zkca.co.za; kingsway@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(280, 280, 262, 'FRESHSTOP REDHILL SERVICE STATION', '316377', NULL, 'c7bfbd1', 'AA', 'TREVOR VISVANATHAN', 'SELWYN MOODLEY', 'KZN', '1200 CHRIS HANI ROAD', '031 569 4226', '074 680 2793', 'redhill@freshstop.co.za; birdsactuary@freshstop.co.za; trevor@trevisfuel.com', 1, '2020-07-30 10:37:04'),
(281, 281, 267, 'FRESHSTOP TASK NISSAN EXPRESS', '332839', NULL, 'a5b735c', 'HMcD', 'DESIRE VAN DER HOVEN', 'LOUISA BUITENDACH', 'INL', 'C/O CROWN & DE VILLIERS', '013 712 2843', '076 440 4454', 'desire@tasknissan.co.za; tasknissan@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(282, 282, 264, 'FRESHSTOP ELOFFSDAL MOTORS', '313450', NULL, 'c4d125f', 'NB', 'ILLZE COETZER', 'ALFRED KGADIMA', 'PTA', '647 PAUL KRUGER STREET', '012 335 2821', '072 332 2404', 'eloffsdal@freshstop.co.za; icoetzer@absamail.co.za', 1, '2020-07-30 10:37:04'),
(283, 283, 263, 'FRESHSTOP STAR STOP MONTE VISTA NORTH', '315456A', NULL, 'cc088f8', 'DH', 'SIPHELELE SITHOLE', 'NTANDO', 'OFS', '14KM NORTH OF HARRISMITH, N3', '087 997 1263 ', '072 940 6930', 'montevistan@freshstop.co.za; s.sithole@nqatshana.co.za', 1, '2020-07-30 10:37:04'),
(284, 284, 0, 'FRESHSTOP STAR STOP MONTE VISTA SOUTH', '315456B', NULL, 'c7ebfd9', 'DH', 'SIPHELELE SITHOLE', 'NTANDO', 'OFS', '14KM NORTH OF HARRISMITH, N3', '087 997 1263', '072 940 6930', 'montevistas@freshstop.co.za;  s.sithole@nqatshana.co.za; montevistasouth@nqatshana.co.za', 1, '2020-07-30 10:37:04'),
(285, 285, 266, 'FRESHSTOP CHE GUEVARA ROAD SERVICE STATION', '313270', NULL, '14f8cfa', 'TM', 'TREVOR VISVANATHAN', 'PHAKAMA', 'KZN', '71 CHE GUEVARA ROAD', '033 345 8484', '083 637 1170 / 082 080 2087', 'trevor@trevisfuel.co.za; cheguevara@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(286, 286, 265, 'FRESHSTOP PHAMBILI', '315826', NULL, 'aaa18d6', 'SD', 'LESMIE JULIES', 'LESMI JULIES', 'WC', 'C/O LANSDOWNE & HAMMER ROADS', 'N/A', '084 666 0001 / 073 417 3039', 'phambili@freshstop.co.za; lesliejulies@gmail.com; lesmijulies@gmail.com', 1, '2020-07-30 10:37:04'),
(287, 287, 275, 'FRESHSTOP HOMESTEAD', '333405', NULL, 'f24844a', 'RC', 'MITCHELL DEAN MAGIDSON', 'PETER & ARMAND', 'INL', '85 BARBARA ROAD', '011 822 3011', 'N/A', 'homestead@freshstop.co.za; homesteadsstation@gmail.com; radical@mweb.co.za', 1, '2020-07-30 10:37:04'),
(288, 288, 268, 'FRESHSTOP UNIVERSAL FILLING STATION - PRESENTLY CLOSED', '313398', NULL, 'af70250', 'GS', 'SEAPEHI DUMASI', 'PHILLIP APHANE', 'INL', '96 VAN RIEBEECK AVENUE', '011 452 4916', '083 387 8431', 'universal@freshstop.co.za; seapehi.dumasi@gmail.com', 1, '2020-07-30 10:37:04'),
(289, 289, 269, 'FRESHSTOP HEIDELBERG', '380970', NULL, '939fccd', 'RVB', 'JOFFREY KLUE', 'MARSHA PIETERSEN', 'WC', '1 EKSTEEN STREET', '028 722 1224', '074 589 8564 / 073 004 1117', 'joffrey@moovfuel.co.za; heidelberg@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(290, 290, 271, 'FRESHSTOP FLAGSTAFF', '382889', NULL, '34d616c', 'LD', 'SINO MAKONGWANA & NYANA MAKONGWANA', 'SINO MAKONGWANA', 'EC', '23 MAIN STREET', '039 940 5273', '078 693 4173 / 076 887 3367', 'sino.goldenecon@gmail.com; flagstaff@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(291, 291, 270, 'FRESHSTOP THE RANCH ', '333324', NULL, '1554141', 'NB', 'JAN DE BEER', 'WILLEM', 'LIM', '24 KM SOUTH OF POLOKWANE ON THE N1', '015 225 7270 ', '082 807 8174', 'jdebeer@xpress.co.za; ranchstop@gmail.com; theranch@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(292, 292, 274, 'FRESHSTOP BEAUFORT WEST', '333863', NULL, 'f629ccf', 'RVB', 'VICTOR TREU', 'DILLON', 'WC', '156 DONKIN ROAD', '023 414 3211', '063 696 0443 / 074 552 0308', 'beaufortwest@freshstop.co.za ; frans-marx.meyer@bkb.co.za; victor.treu@bkb.co.za; yolanda.colesky@bkb.co.za; christelle.vanderiet@bkb.co.za; riaan.vosloo@bkb.co.za', 1, '2020-07-30 10:37:04'),
(293, 293, 272, 'FRESHSTOP VAN REENEN', '317290', NULL, 'b525c10', 'DH', 'ABEN NAIDOO', 'MONIQUE', 'OFS', 'N3 NATIONAL ROAD', '074 764 0859', '081 597 1802', 'aben.mvhd@gmail.com;  vanreenen@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(294, 294, 273, 'FRESHSTOP BUTTERWORTH', '382113', NULL, 'b99e265', 'LD', 'GCOBANI NTSHANGA', 'TANDIE NTSHANGA', 'EC', 'C/O BELL & MTHATHA STREET', '047 491 0222', '071 001 3152 / 076 315 7390', 'ntshangag@walkermotors.co.za; ntshangan@walkermotors.co.za; butterworth@freshstop.co.za ', 1, '2020-07-30 10:37:04'),
(295, 295, 0, 'FRESHSTOP PINETOWN', '381984', NULL, 'cc5063c', 'AA', 'CLYNT LUND', 'CLYNT LUND', 'KZN', '161 JOSIAH GUMEDE ROAD', '031 332 9791', '082 824 0048', 'clynt.lund@gmail.com;  pinetown@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(296, 296, 276, 'FRESHSTOP MEGA MOTORS', '333183', NULL, 'f8ca92c', 'RC', 'ZUNAID JANSE VAN RESNSBURG', 'CHARLENE', 'MPU S', 'C/O HEINDRICH & WALTER STREET', '017 648 3843', '082 318 9426', 'megamotors@freshstop.co.za; megamotors@vodamail.co.za', 1, '2020-07-30 10:37:04'),
(297, 297, 277, 'FRESHSTOP POSTHOUSE', '312698', NULL, '152a420', 'AG', 'SACHA ANSELL', 'MARK ANSELL', 'INL', 'C/O MAIN ROAD & POSTHOUSE STREET', '011 706 1196', '083 279 9508', 'posthouse@freshstop.co.za; sacha@ansellproperties.co.za; mark@ansellproperties.co.za', 1, '2020-07-30 10:37:04'),
(298, 298, 278, 'FRESHSTOP GOODWILL', '382012', NULL, 'c9cee0c', 'GS', 'M. FEROUZE MOOSA / ASIF MOOSA', 'TASNEEM / NAZEEM', 'INL', '2 HANOVER STREET, C/O MAIN REEF ROAD', '011 837 4006', '083 678 6007 / 082 883 3222', 'goodwill@freshstop.co.za; tasneem@caltexcrown.com', 1, '2020-07-30 10:37:04'),
(299, 299, 280, 'FRESHSTOP PERSEVERANCE ', '376440', NULL, '1d7dce9', 'RP', 'DAVID EDWARDS', 'MATT EDWARDS', 'EC', '9 KURLAND ROAD', '041 463 1110', '072 832 3722 / 072 640 8796', 'perseverance@freshstop.co.za; david.edwards@sasfin.com; pssadmin@wire1.net', 1, '2020-07-30 10:37:04'),
(300, 300, 279, 'FRESHSTOP 45TH CUTTING ', '316750', NULL, '3f60557', 'AA', 'TREVOR VISVANATHAN', 'YUGEN MAISTRY / ALLAN', 'KZN', '928 KING CETSHWAYO HIGH', '031 207 6290', '083 637 1170 / 084 945 5000 / ', '45thstreet@freshstop.co.za; birdsanctuary@freshstop.co.za; trevor@trevisfuel.com', 1, '2020-07-30 10:37:04'),
(301, 301, 281, 'FRESHSTOP PAN AFRICA', '380806', NULL, '1d57f88', 'GS', 'RICHARD ISRAELSOHN', 'MO', 'INL', '722 CHADWICK AVE', '011 786 0280', '071 885 6939', 'caltexpanafrica@gmail.com; panafrica@freshstop.co.za; richard690414@gmail.com', 1, '2020-07-30 10:37:04'),
(302, 302, 282, 'FRESHSTOP BREDASDORP', '382764', NULL, '0455ffd', 'RVB', 'ANDRIES DE VILLIERS', 'CARINE MATTHEE', 'WC', 'C/O LONG AND ALL SAINTS STREET', '028 424 1144', '082 898 5856', 'andriesdv@rolagrp.co.za; bredasdorp@freshstop.co.za; carinem@rolagrp.co.za', 1, '2020-07-30 10:37:04'),
(303, 303, 283, 'FRESHSTOP MOGODI', '', NULL, 'c7b9c71', 'NB', 'FRANS PIETERSE', 'CHRISTO VAN DER MERWE', 'LIM', 'STAND 47, MOGODI GA-MPHAHLELE, CNR R37 & R518', '083 414 3974', '083 414 3974', 'fransjr@bfgroup.co.za; mogodi@bfgroup.co.za; christo@bfgroup.co.za', 1, '2020-07-30 10:37:04'),
(304, 304, 284, 'FRESHSTOP LEMON TREE SERVICE STATION', '382373', NULL, '597dba2', 'MN', 'HENDRIK JANSEN', 'RIKA HOWARD', 'INL', 'C/O LINCOLN & ASCOT STREET', '011 442 410', '082 715 6900 / 082 293 0933', 'hendrik.jansen@bmwdealer.co.za; rika.howard@alberante.co.za; lemontree@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(305, 305, 285, 'FRESHSTOP BOSHOFF', '315873', NULL, '0924dd6', 'AA', 'S P HERBERT', 'NATASHA', 'KZN', '21 BOSHOFF STREET', '033 342 2493', '082 468 2828', 'stephenherbert@mweb.co.za; boshoff@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(306, 306, 287, 'FRESHSTOP NIGEL SERVICE STATION', '333295', NULL, '291ad85', 'RC', 'DAWIE ERNST ', 'OBED MAKHUBO', 'INL', '1 HEIDELBERG ROAD', '011 814 1562', '081 406 0334', 'dawie@ferthal.co.za; obed@nigelss.co.za; nigel@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(307, 307, 286, 'FRESHSTOP RIVERSIDE R40', '382888', NULL, '0c567a1', 'HMcD', 'LLEWELLYN NEL', 'KOBUS KLOPPER', 'INL', 'ERF 916 (R40), RIVERSIDE PARK EXT 24', '013 591 0209', '082 878 0288', 'riverside@ithubalethu.co.za; riverside@freshstop.co.za; groupops@ithubaletu.co.za', 1, '2020-07-30 10:37:04'),
(308, 308, 289, 'FRESHSTOP CAROLINA', '382919', NULL, '722fd1f', 'HMcD', 'PIET KOTZE', 'MARTINIQUE KOTZE', 'INL', '58 VOORTREKKER STREET', '013 591 2847', '079 401 3710', 'pieter.kotze@gmail.com; carolina@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(309, 309, 290, 'FRESHSTOP STAR CIRCLE', '314104', NULL, '7ed1049', 'AG', 'ZAHIR DESAI', 'HAROON', 'INL', 'Cnr BEYERS NAUDE & PETER ROAD', '010 222 0424', '082 567 6403', 'linkcaltex@mweb.co.za; starcircle@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(310, 310, 291, 'FRESHSTOP PORT EDWARD', '316024', NULL, 'ab039c3', 'AA', 'GARY HIGHCOCK ', 'VICTORIA', 'KZN', 'Cnr R61 & OWEN ELLIS DRIVE', '039 311 2230', '082 808 8361', 'portedwardgarage@satcom.co.za; portedward@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(311, 311, 292, 'FRESHSTOP CLAIRWOOD', '382404', NULL, '573784a', 'MC', 'AHMED HOOSEN MOHAMED', 'AHMED HOOSEN MOHAMED', 'KZN', '605 SOUTHCOAST ROAD', '', '072 138 6222', 'kruzin.am@gmail.com; clairwood@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(312, 312, 293, 'FRESHSTOP BULWER PARK', '313250', NULL, '1f71195', 'TM', 'YOUNUS CASSIM', 'YOUNUS CASSIM', 'KZN', '159 BULWER ROAD', '031 201 2923', '083 778 6706', 'admin@bulpark.co.za; younus@coweymotors.co.za; bulwerpark@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(313, 313, 288, 'FRESHSTOP CALA', '383112', NULL, '967851d', 'LD', 'LWANDILE NJOBE', 'LINDA NJOBE', 'EC', 'CNR uMZIMKHULU STR & R410', '073 825 3709', '083 984 8881', 'njobelinda@gmail.com; lwandilenjobe2@gmail.com; cala@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(314, 314, 294, 'FRESHSTOP BLUE LAGOON', '380701', NULL, '5d5eebf', 'TM', 'DEAN PRETORIUS', 'DEAN PRETORIUS', 'KZN', '80 ATHLONE DRIVE', '', '076 730 2661', 'dean@oilgro.co.za; bluelagoon@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(315, 315, 295, 'FRESHSTOP GLJ MOTORS', '382902', NULL, '1672364', 'NB', 'JAAP LEE', 'CHARISKA ROBBERTZE', 'INL', '75 PRETORIUS STREET', '', '082 565 3452', 'jaap@bluepump.co.za; glj@bluepump.co.za; gljmotors@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(316, 316, 297, 'FRESHSTOP GUINEA FOWL', '314892', NULL, '531284c', 'AA', 'ALGENE TIMM', 'BRYDEN TIMM / SHIVON TIMM ', 'KZN', '372 MURCHISON STREET', '036 637 2761', '082 808 6532', 'algenetimm@gmail.com; guineafowl@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(317, 317, 296, 'FRESHSTOP PAROW', '380808', NULL, '60f8761', 'SD', 'ABDUL ALEEM ALI', 'HAMIED SALLIE', 'WC', '142 VOORTREKKER ROAD', '021 286 9053', '082 553 0848', 'mr.a.ali@hotmail.com; parow@freshstop.co.za; hamied99@hotmail.com; ', 1, '2020-07-30 10:37:04'),
(318, 318, 300, 'FRESHSTOP LION VALLEY', '382416', NULL, '9b0ed36', 'HMcD', 'JULIUS MARSH', 'DANIEL SELLO', 'LIM', 'SHOP 8, DIRK WINTERBACH STREET', '013 751 2620', '082 415 5801', 'julius@ithubaletu.co.za; lionvalley@freshstop.co.za; lionvalley@ithubaletu.co.za', 1, '2020-07-30 10:37:04'),
(319, 319, 298, 'FRESHSTOP NGCOBO', '383113', NULL, '26be263', 'LD', 'GCINA MNQANDI', 'DUMISA MBUSELA', 'EC', '134 MTHATHA ROAD', '047 548 1295', '082 494 6259 / 072 187 9229', 'g.mnqandi@yahoo.com; ngcobo@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(320, 320, 299, 'FRESHSTOP SONOP', '312937', NULL, '40a7eae', 'AMA', 'MICKEY KEYSER ', 'DANIE VON WIELLIGH', 'WC', 'N7, CLANWILLIAM ROAD', '078 690 5505', '083 444 9854', 'mickey@paarlfreshstop.co.za; danie@sonopmotors.co.za; sonop@freshstop.co.za; ', 1, '2020-07-30 10:37:04'),
(321, 321, 301, 'FRESHSTOP VREDENBURG', '317461', NULL, 'db43ff6', 'AMA', 'JOFFREY KLUE', 'JOFFREY KLUE', 'WC', '27 MAIN STREET', '', '074 589 8564', 'joffrey@moovfuel.co.za; vredenburg@freshstop.co.za', 1, '2020-07-30 10:37:04'),
(322, 322, 0, 'FRESHSTOP BOABAB PLAZA (BOKMAKIERIE)', '', NULL, '13d19f1', 'NB', 'JOHAN VAN EEDEN SNR', 'JOHAN VAN EEDEN JNR', 'LIM', 'CNR N1 & R525', '', '082 859 5074', 'johan@baobabplaza.co.za;  baobab@freshstop.co.za; amanda@edenoil.co.za; ', 1, '2020-07-30 10:37:04'),
(323, 323, 303, 'FRESHSTOP BOLAND SERVICE STATION ', '', NULL, '9495b9e', 'SD', 'BILKEES MEHMOOD', 'GLYNIS JENNEKER', 'WC', '248 BIRD ST', '021 887 5806', '082 211 2245', 'bmehmood31@gmail.com; boland@freshstop.co.za', 1, '2020-07-30 10:37:04');

-- --------------------------------------------------------

--
-- Table structure for table `sliders_images`
--

CREATE TABLE `sliders_images` (
  `sliders_id` int(11) NOT NULL,
  `sliders_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sliders_url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `carousel_id` int(11) DEFAULT NULL,
  `sliders_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `sliders_group` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sliders_html_text` text COLLATE utf8_unicode_ci NOT NULL,
  `expires_date` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `languages_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders_images`
--

INSERT INTO `sliders_images` (`sliders_id`, `sliders_title`, `sliders_url`, `carousel_id`, `sliders_image`, `sliders_group`, `sliders_html_text`, `expires_date`, `date_added`, `status`, `type`, `date_status_change`, `languages_id`) VALUES
(1, 'Left Slider with Thumbs (770x400)', '', 5, '109', '', '', '2035-11-15 00:00:00', '2020-04-13 14:36:18', 1, 'topseller', '2020-04-13 14:36:18', 1),
(4, 'Full Screen Slider (1600x420)', '', 1, '111', '', '', '2020-07-15 00:00:00', '2020-04-13 14:32:27', 1, 'special', '2020-04-13 14:32:27', 1),
(7, 'Full Page Slider (1170x420)', '', 2, '108', '', '', '2025-11-26 00:00:00', '2020-04-13 14:31:54', 1, 'topseller', '2020-04-13 14:31:54', 1),
(10, 'Right Slider with Thumbs (770x400)', '', 3, '110', '', '', '2025-10-20 00:00:00', '2020-04-13 14:33:23', 1, 'topseller', '2020-04-13 14:33:23', 1),
(13, 'Right Slider with Navigation (770x400)', '', 4, '109', '', '', '2025-07-24 00:00:00', '2020-04-13 14:33:58', 1, 'topseller', '2020-04-13 14:33:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `specials`
--

CREATE TABLE `specials` (
  `specials_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `specials_new_products_price` decimal(15,2) NOT NULL,
  `specials_date_added` int(11) NOT NULL,
  `specials_last_modified` int(11) NOT NULL,
  `expires_date` int(11) NOT NULL,
  `date_status_change` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_class`
--

CREATE TABLE `tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `tax_class_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tax_class_description` text COLLATE utf8_unicode_ci,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_rates`
--

CREATE TABLE `tax_rates` (
  `tax_rates_id` int(11) NOT NULL,
  `tax_zone_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_priority` int(11) DEFAULT '1',
  `tax_rate` decimal(7,2) NOT NULL,
  `tax_description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `top_offers`
--

CREATE TABLE `top_offers` (
  `top_offers_id` int(11) NOT NULL,
  `top_offers_text` text COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `top_offers`
--

INSERT INTO `top_offers` (`top_offers_id`, `top_offers_text`, `language_id`, `created_at`, `updated_at`) VALUES
(1, '<div class=\\\"pro-info\\\">\n                Get<strong> UPTO 40% OFF </strong>on your 1st order\n                <div class=\\\"pro-link-dropdown js-toppanel-link-dropdown\\\">\n                  <a href=\\\"shop-page1.html\\\" class=\\\"pro-dropdown-toggle\\\">\n                    More details\n                  </a>\n                  \n                </div>\n            </div>', 1, '2020-02-04 03:14:16', NULL),
(2, '<div class=\\\"pro-info\\\">\n                Get<strong> UPTO 40% OFF </strong>on your 1st order\n                <div class=\\\"pro-link-dropdown js-toppanel-link-dropdown\\\">\n                  <a href=\\\"shop-page1.html\\\" class=\\\"pro-dropdown-toggle\\\">\n                    More details\n                  </a>\n                  \n                </div>\n            </div>', 1, '2020-02-04 03:14:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `unit_id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units_descriptions`
--

CREATE TABLE `units_descriptions` (
  `units_description_id` int(10) UNSIGNED NOT NULL,
  `units_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `languages_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ups_shipping`
--

CREATE TABLE `ups_shipping` (
  `ups_id` int(11) NOT NULL,
  `pickup_method` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `isDisplayCal` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `serviceType` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `shippingEnvironment` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `access_key` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `person_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_1` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_2` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_package` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `parcel_height` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `parcel_width` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ups_shipping`
--

INSERT INTO `ups_shipping` (`ups_id`, `pickup_method`, `isDisplayCal`, `serviceType`, `shippingEnvironment`, `user_name`, `access_key`, `password`, `person_name`, `company_name`, `phone_number`, `address_line_1`, `address_line_2`, `country`, `state`, `post_code`, `city`, `no_of_package`, `parcel_height`, `parcel_width`, `title`) VALUES
(1, '07', '', 'US_01,US_02,US_03,US_12,US_13,US_14,US_59', '0', 'nyblueprint', 'FCD7C8F94CB5EF46', 'delfia11', '', '', '', 'D Ground', '', 'US', 'NY', '10312', 'New York City', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_address_id` int(11) NOT NULL DEFAULT '0',
  `country_code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `phone_verified` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_id_tiwilo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(33) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `user_name`, `first_name`, `last_name`, `gender`, `default_address_id`, `country_code`, `phone`, `email`, `password`, `avatar`, `status`, `is_seen`, `phone_verified`, `remember_token`, `auth_id_tiwilo`, `dob`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'Admin', '-', NULL, 0, NULL, NULL, 'admin@test.com', '$2y$10$RuANyL4vAANY5/0LZ2/ToedWpvl7ZVL05C0rR5Fpc4yWt7ZtK0lc2', NULL, '1', 0, NULL, NULL, NULL, NULL, '2020-08-13 07:17:02', '2020-08-13 07:17:02'),
(2, 13, 'retailer1_retailer1597837545', 'retailer1', 'retailer', NULL, 0, NULL, '0877004010', 'retailer1@freshstop.co.za', '$2y$10$C.YUUTSHWSki8PReV6g5heVj6Lut0P4r0FWkH2Rq7Wwz6Ff6D3j9K', '', '1', 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_to_address`
--

CREATE TABLE `user_to_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_book_id` int(11) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `user_types_id` int(11) NOT NULL,
  `user_types_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_types_id`, `user_types_name`, `created_at`, `updated_at`, `isActive`) VALUES
(1, 'Super Admin', 1534774230, NULL, 1),
(2, 'Customers', 1534777027, NULL, 1),
(3, 'Vendors', 1538390209, NULL, 1),
(4, 'Delivery Guy', 1542965906, NULL, 1),
(5, 'Test 1', 1542965906, NULL, 1),
(6, 'Test 2', 1542965906, NULL, 1),
(7, 'Test 3', 1542965906, NULL, 1),
(8, 'Test 4', 1542965906, NULL, 1),
(9, 'Test 5', 1542965906, NULL, 1),
(10, 'Test 6', 1542965906, NULL, 1),
(11, 'Admin', 1542965906, NULL, 1),
(12, 'Auditor', 1597759159, NULL, 1),
(13, 'Retailer', 1597759166, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `whos_online`
--

CREATE TABLE `whos_online` (
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `full_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `time_entry` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `time_last_click` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `last_page_url` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `zone_id` int(11) NOT NULL,
  `zone_country_id` int(11) NOT NULL,
  `zone_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES
(1, 223, 'AL', 'Alabama'),
(2, 223, 'AK', 'Alaska'),
(3, 223, 'AS', 'American Samoa'),
(4, 223, 'AZ', 'Arizona'),
(5, 223, 'AR', 'Arkansas'),
(6, 223, 'AF', 'Armed Forces Africa'),
(7, 223, 'AA', 'Armed Forces Americas'),
(8, 223, 'AC', 'Armed Forces Canada'),
(9, 223, 'AE', 'Armed Forces Europe'),
(10, 223, 'AM', 'Armed Forces Middle East'),
(11, 223, 'AP', 'Armed Forces Pacific'),
(12, 223, 'CA', 'California'),
(13, 223, 'CO', 'Colorado'),
(14, 223, 'CT', 'Connecticut'),
(15, 223, 'DE', 'Delaware'),
(16, 223, 'DC', 'District of Columbia'),
(17, 223, 'FM', 'Federated States Of Micronesia'),
(18, 223, 'FL', 'Florida'),
(19, 223, 'GA', 'Georgia'),
(20, 223, 'GU', 'Guam'),
(21, 223, 'HI', 'Hawaii'),
(22, 223, 'ID', 'Idaho'),
(23, 223, 'IL', 'Illinois'),
(24, 223, 'IN', 'Indiana'),
(25, 223, 'IA', 'Iowa'),
(26, 223, 'KS', 'Kansas'),
(27, 223, 'KY', 'Kentucky'),
(28, 223, 'LA', 'Louisiana'),
(29, 223, 'ME', 'Maine'),
(30, 223, 'MH', 'Marshall Islands'),
(31, 223, 'MD', 'Maryland'),
(32, 223, 'MA', 'Massachusetts'),
(33, 223, 'MI', 'Michigan'),
(34, 223, 'MN', 'Minnesota'),
(35, 223, 'MS', 'Mississippi'),
(36, 223, 'MO', 'Missouri'),
(37, 223, 'MT', 'Montana'),
(38, 223, 'NE', 'Nebraska'),
(39, 223, 'NV', 'Nevada'),
(40, 223, 'NH', 'New Hampshire'),
(41, 223, 'NJ', 'New Jersey'),
(42, 223, 'NM', 'New Mexico'),
(43, 223, 'NY', 'New York'),
(44, 223, 'NC', 'North Carolina'),
(45, 223, 'ND', 'North Dakota'),
(46, 223, 'MP', 'Northern Mariana Islands'),
(47, 223, 'OH', 'Ohio'),
(48, 223, 'OK', 'Oklahoma'),
(49, 223, 'OR', 'Oregon'),
(50, 223, 'PW', 'Palau'),
(51, 223, 'PA', 'Pennsylvania'),
(52, 223, 'PR', 'Puerto Rico'),
(53, 223, 'RI', 'Rhode Island'),
(54, 223, 'SC', 'South Carolina'),
(55, 223, 'SD', 'South Dakota'),
(56, 223, 'TN', 'Tennessee'),
(57, 223, 'TX', 'Texas'),
(58, 223, 'UT', 'Utah'),
(59, 223, 'VT', 'Vermont'),
(60, 223, 'VI', 'Virgin Islands'),
(61, 223, 'VA', 'Virginia'),
(62, 223, 'WA', 'Washington'),
(63, 223, 'WV', 'West Virginia'),
(64, 223, 'WI', 'Wisconsin'),
(65, 223, 'WY', 'Wyoming'),
(66, 38, 'AB', 'Alberta'),
(67, 38, 'BC', 'British Columbia'),
(68, 38, 'MB', 'Manitoba'),
(69, 38, 'NF', 'Newfoundland'),
(70, 38, 'NB', 'New Brunswick'),
(71, 38, 'NS', 'Nova Scotia'),
(72, 38, 'NT', 'Northwest Territories'),
(73, 38, 'NU', 'Nunavut'),
(74, 38, 'ON', 'Ontario'),
(75, 38, 'PE', 'Prince Edward Island'),
(76, 38, 'QC', 'Quebec'),
(77, 38, 'SK', 'Saskatchewan'),
(78, 38, 'YT', 'Yukon Territory'),
(79, 81, 'NDS', 'Niedersachsen'),
(80, 81, 'BAW', 'Baden-Württemberg'),
(81, 81, 'BAY', 'Bayern'),
(82, 81, 'BER', 'Berlin'),
(83, 81, 'BRG', 'Brandenburg'),
(84, 81, 'BRE', 'Bremen'),
(85, 81, 'HAM', 'Hamburg'),
(86, 81, 'HES', 'Hessen'),
(87, 81, 'MEC', 'Mecklenburg-Vorpommern'),
(88, 81, 'NRW', 'Nordrhein-Westfalen'),
(89, 81, 'RHE', 'Rheinland-Pfalz'),
(90, 81, 'SAR', 'Saarland'),
(91, 81, 'SAS', 'Sachsen'),
(92, 81, 'SAC', 'Sachsen-Anhalt'),
(93, 81, 'SCN', 'Schleswig-Holstein'),
(94, 81, 'THE', 'Thüringen'),
(95, 14, 'WI', 'Wien'),
(96, 14, 'NO', 'Niederösterreich'),
(97, 14, 'OO', 'Oberösterreich'),
(98, 14, 'SB', 'Salzburg'),
(99, 14, 'KN', 'Kärnten'),
(100, 14, 'ST', 'Steiermark'),
(101, 14, 'TI', 'Tirol'),
(102, 14, 'BL', 'Burgenland'),
(103, 14, 'VB', 'Voralberg'),
(104, 204, 'AG', 'Aargau'),
(105, 204, 'AI', 'Appenzell Innerrhoden'),
(106, 204, 'AR', 'Appenzell Ausserrhoden'),
(107, 204, 'BE', 'Bern'),
(108, 204, 'BL', 'Basel-Landschaft'),
(109, 204, 'BS', 'Basel-Stadt'),
(110, 204, 'FR', 'Freiburg'),
(111, 204, 'GE', 'Genf'),
(112, 204, 'GL', 'Glarus'),
(113, 204, 'JU', 'Graubünden'),
(114, 204, 'JU', 'Jura'),
(115, 204, 'LU', 'Luzern'),
(116, 204, 'NE', 'Neuenburg'),
(117, 204, 'NW', 'Nidwalden'),
(118, 204, 'OW', 'Obwalden'),
(119, 204, 'SG', 'St. Gallen'),
(120, 204, 'SH', 'Schaffhausen'),
(121, 204, 'SO', 'Solothurn'),
(122, 204, 'SZ', 'Schwyz'),
(123, 204, 'TG', 'Thurgau'),
(124, 204, 'TI', 'Tessin'),
(125, 204, 'UR', 'Uri'),
(126, 204, 'VD', 'Waadt'),
(127, 204, 'VS', 'Wallis'),
(128, 204, 'ZG', 'Zug'),
(129, 204, 'ZH', 'Zürich'),
(130, 195, 'A Coruña', 'A Coruña'),
(131, 195, 'Alava', 'Alava'),
(132, 195, 'Albacete', 'Albacete'),
(133, 195, 'Alicante', 'Alicante'),
(134, 195, 'Almeria', 'Almeria'),
(135, 195, 'Asturias', 'Asturias'),
(136, 195, 'Avila', 'Avila'),
(137, 195, 'Badajoz', 'Badajoz'),
(138, 195, 'Baleares', 'Baleares'),
(139, 195, 'Barcelona', 'Barcelona'),
(140, 195, 'Burgos', 'Burgos'),
(141, 195, 'Caceres', 'Caceres'),
(142, 195, 'Cadiz', 'Cadiz'),
(143, 195, 'Cantabria', 'Cantabria'),
(144, 195, 'Castellon', 'Castellon'),
(145, 195, 'Ceuta', 'Ceuta'),
(146, 195, 'Ciudad Real', 'Ciudad Real'),
(147, 195, 'Cordoba', 'Cordoba'),
(148, 195, 'Cuenca', 'Cuenca'),
(149, 195, 'Girona', 'Girona'),
(150, 195, 'Granada', 'Granada'),
(151, 195, 'Guadalajara', 'Guadalajara'),
(152, 195, 'Guipuzcoa', 'Guipuzcoa'),
(153, 195, 'Huelva', 'Huelva'),
(154, 195, 'Huesca', 'Huesca'),
(155, 195, 'Jaen', 'Jaen'),
(156, 195, 'La Rioja', 'La Rioja'),
(157, 195, 'Las Palmas', 'Las Palmas'),
(158, 195, 'Leon', 'Leon'),
(159, 195, 'Lleida', 'Lleida'),
(160, 195, 'Lugo', 'Lugo'),
(161, 195, 'Madrid', 'Madrid'),
(162, 195, 'Malaga', 'Malaga'),
(163, 195, 'Melilla', 'Melilla'),
(164, 195, 'Murcia', 'Murcia'),
(165, 195, 'Navarra', 'Navarra'),
(166, 195, 'Ourense', 'Ourense'),
(167, 195, 'Palencia', 'Palencia'),
(168, 195, 'Pontevedra', 'Pontevedra'),
(169, 195, 'Salamanca', 'Salamanca'),
(170, 195, 'Santa Cruz de Tenerife', 'Santa Cruz de Tenerife'),
(171, 195, 'Segovia', 'Segovia'),
(172, 195, 'Sevilla', 'Sevilla'),
(173, 195, 'Soria', 'Soria'),
(174, 195, 'Tarragona', 'Tarragona'),
(175, 195, 'Teruel', 'Teruel'),
(176, 195, 'Toledo', 'Toledo'),
(177, 195, 'Valencia', 'Valencia'),
(178, 195, 'Valladolid', 'Valladolid'),
(179, 195, 'Vizcaya', 'Vizcaya'),
(180, 195, 'Zamora', 'Zamora'),
(181, 195, 'Zaragoza', 'Zaragoza');

-- --------------------------------------------------------

--
-- Table structure for table `zones_to_geo_zones`
--

CREATE TABLE `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL,
  `zone_country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`address_book_id`),
  ADD KEY `idx_address_book_customers_id` (`user_id`);

--
-- Indexes for table `alert_settings`
--
ALTER TABLE `alert_settings`
  ADD PRIMARY KEY (`alert_id`);

--
-- Indexes for table `api_calls_list`
--
ALTER TABLE `api_calls_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_answers`
--
ALTER TABLE `audit_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_images`
--
ALTER TABLE `audit_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_questions`
--
ALTER TABLE `audit_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_questions2`
--
ALTER TABLE `audit_questions2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_sites`
--
ALTER TABLE `audit_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banners_id`),
  ADD KEY `idx_banners_group` (`banners_group`);

--
-- Indexes for table `banners_history`
--
ALTER TABLE `banners_history`
  ADD PRIMARY KEY (`banners_history_id`),
  ADD KEY `idx_banners_history_banners_id` (`banners_id`);

--
-- Indexes for table `block_ips`
--
ALTER TABLE `block_ips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_categories_parent_id` (`parent_id`);

--
-- Indexes for table `categories_description`
--
ALTER TABLE `categories_description`
  ADD PRIMARY KEY (`categories_description_id`),
  ADD KEY `idx_categories_name` (`categories_name`);

--
-- Indexes for table `categories_organization`
--
ALTER TABLE `categories_organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_role`
--
ALTER TABLE `categories_role`
  ADD PRIMARY KEY (`categories_role_id`);

--
-- Indexes for table `compare`
--
ALTER TABLE `compare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constant_banners`
--
ALTER TABLE `constant_banners`
  ADD PRIMARY KEY (`banners_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`countries_id`),
  ADD KEY `IDX_COUNTRIES_NAME` (`countries_name`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupans_id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_currencies_code` (`code`);

--
-- Indexes for table `currency_record`
--
ALTER TABLE `currency_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `current_theme`
--
ALTER TABLE `current_theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customers_id`);

--
-- Indexes for table `customers_basket`
--
ALTER TABLE `customers_basket`
  ADD PRIMARY KEY (`customers_basket_id`),
  ADD KEY `idx_customers_basket_customers_id` (`customers_id`);

--
-- Indexes for table `customers_basket_attributes`
--
ALTER TABLE `customers_basket_attributes`
  ADD PRIMARY KEY (`customers_basket_attributes_id`),
  ADD KEY `idx_customers_basket_att_customers_id` (`customers_id`);

--
-- Indexes for table `customers_info`
--
ALTER TABLE `customers_info`
  ADD PRIMARY KEY (`customers_info_id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flash_sale`
--
ALTER TABLE `flash_sale`
  ADD PRIMARY KEY (`flash_sale_id`),
  ADD KEY `products_id` (`products_id`);

--
-- Indexes for table `flate_rate`
--
ALTER TABLE `flate_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_end_theme_content`
--
ALTER TABLE `front_end_theme_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `geo_zones`
--
ALTER TABLE `geo_zones`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`home_banners_id`);

--
-- Indexes for table `http_call_record`
--
ALTER TABLE `http_call_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_categories`
--
ALTER TABLE `image_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inventory_ref_id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`label_id`);

--
-- Indexes for table `label_value`
--
ALTER TABLE `label_value`
  ADD PRIMARY KEY (`label_value_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`languages_id`),
  ADD KEY `IDX_LANGUAGES_NAME` (`name`);

--
-- Indexes for table `liked_products`
--
ALTER TABLE `liked_products`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `manage_min_max`
--
ALTER TABLE `manage_min_max`
  ADD PRIMARY KEY (`min_max_id`);

--
-- Indexes for table `manage_role`
--
ALTER TABLE `manage_role`
  ADD PRIMARY KEY (`manage_role_id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`manufacturers_id`);

--
-- Indexes for table `manufacturers_info`
--
ALTER TABLE `manufacturers_info`
  ADD PRIMARY KEY (`manufacturers_id`,`languages_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `idx_products_date_added` (`news_date_added`);

--
-- Indexes for table `news_categories`
--
ALTER TABLE `news_categories`
  ADD PRIMARY KEY (`categories_id`),
  ADD KEY `idx_categories_parent_id` (`parent_id`);

--
-- Indexes for table `news_categories_description`
--
ALTER TABLE `news_categories_description`
  ADD PRIMARY KEY (`categories_description_id`),
  ADD KEY `idx_categories_name` (`categories_name`);

--
-- Indexes for table `news_description`
--
ALTER TABLE `news_description`
  ADD KEY `products_name` (`news_name`);

--
-- Indexes for table `news_to_news_categories`
--
ALTER TABLE `news_to_news_categories`
  ADD PRIMARY KEY (`news_id`,`categories_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orders_id`),
  ADD KEY `idx_orders_customers_id` (`customers_id`);

--
-- Indexes for table `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`orders_products_id`),
  ADD KEY `idx_orders_products_orders_id` (`orders_id`),
  ADD KEY `idx_orders_products_products_id` (`products_id`);

--
-- Indexes for table `orders_products_attributes`
--
ALTER TABLE `orders_products_attributes`
  ADD PRIMARY KEY (`orders_products_attributes_id`),
  ADD KEY `idx_orders_products_att_orders_id` (`orders_id`);

--
-- Indexes for table `orders_status`
--
ALTER TABLE `orders_status`
  ADD PRIMARY KEY (`orders_status_id`);

--
-- Indexes for table `orders_status_description`
--
ALTER TABLE `orders_status_description`
  ADD PRIMARY KEY (`orders_status_description_id`);

--
-- Indexes for table `orders_status_history`
--
ALTER TABLE `orders_status_history`
  ADD PRIMARY KEY (`orders_status_history_id`),
  ADD KEY `idx_orders_status_history_orders_id` (`orders_id`);

--
-- Indexes for table `orders_total`
--
ALTER TABLE `orders_total`
  ADD PRIMARY KEY (`orders_total_id`),
  ADD KEY `idx_orders_total_orders_id` (`orders_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `pages_description`
--
ALTER TABLE `pages_description`
  ADD PRIMARY KEY (`page_description_id`);

--
-- Indexes for table `payment_description`
--
ALTER TABLE `payment_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`payment_methods_id`);

--
-- Indexes for table `payment_methods_detail`
--
ALTER TABLE `payment_methods_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`products_id`),
  ADD KEY `idx_products_model` (`products_model`),
  ADD KEY `idx_products_date_added` (`products_date_added`);

--
-- Indexes for table `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`products_attributes_id`),
  ADD KEY `idx_products_attributes_products_id` (`products_id`);

--
-- Indexes for table `products_description`
--
ALTER TABLE `products_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_name` (`products_name`);

--
-- Indexes for table `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_images_prodid` (`products_id`);

--
-- Indexes for table `products_options`
--
ALTER TABLE `products_options`
  ADD PRIMARY KEY (`products_options_id`);

--
-- Indexes for table `products_options_descriptions`
--
ALTER TABLE `products_options_descriptions`
  ADD PRIMARY KEY (`products_options_descriptions_id`);

--
-- Indexes for table `products_options_values`
--
ALTER TABLE `products_options_values`
  ADD PRIMARY KEY (`products_options_values_id`);

--
-- Indexes for table `products_options_values_descriptions`
--
ALTER TABLE `products_options_values_descriptions`
  ADD PRIMARY KEY (`products_options_values_descriptions_id`);

--
-- Indexes for table `products_shipping_rates`
--
ALTER TABLE `products_shipping_rates`
  ADD PRIMARY KEY (`products_shipping_rates_id`);

--
-- Indexes for table `products_to_categories`
--
ALTER TABLE `products_to_categories`
  ADD PRIMARY KEY (`products_to_categories_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`reviews_id`),
  ADD KEY `idx_reviews_products_id` (`products_id`),
  ADD KEY `idx_reviews_customers_id` (`customers_id`);

--
-- Indexes for table `reviews_description`
--
ALTER TABLE `reviews_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sesskey`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_name_unique` (`name`);

--
-- Indexes for table `shipping_description`
--
ALTER TABLE `shipping_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_methods`
--
ALTER TABLE `shipping_methods`
  ADD PRIMARY KEY (`shipping_methods_id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders_images`
--
ALTER TABLE `sliders_images`
  ADD PRIMARY KEY (`sliders_id`);

--
-- Indexes for table `specials`
--
ALTER TABLE `specials`
  ADD PRIMARY KEY (`specials_id`),
  ADD KEY `idx_specials_products_id` (`products_id`);

--
-- Indexes for table `tax_class`
--
ALTER TABLE `tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `tax_rates`
--
ALTER TABLE `tax_rates`
  ADD PRIMARY KEY (`tax_rates_id`);

--
-- Indexes for table `top_offers`
--
ALTER TABLE `top_offers`
  ADD PRIMARY KEY (`top_offers_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `units_descriptions`
--
ALTER TABLE `units_descriptions`
  ADD PRIMARY KEY (`units_description_id`);

--
-- Indexes for table `ups_shipping`
--
ALTER TABLE `ups_shipping`
  ADD PRIMARY KEY (`ups_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_address`
--
ALTER TABLE `user_to_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`user_types_id`);

--
-- Indexes for table `whos_online`
--
ALTER TABLE `whos_online`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`zone_id`),
  ADD KEY `idx_zones_country_id` (`zone_country_id`);

--
-- Indexes for table `zones_to_geo_zones`
--
ALTER TABLE `zones_to_geo_zones`
  ADD PRIMARY KEY (`association_id`),
  ADD KEY `idx_zones_to_geo_zones_country_id` (`zone_country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address_book`
--
ALTER TABLE `address_book`
  MODIFY `address_book_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alert_settings`
--
ALTER TABLE `alert_settings`
  MODIFY `alert_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `api_calls_list`
--
ALTER TABLE `api_calls_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `audits`
--
ALTER TABLE `audits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `audit_answers`
--
ALTER TABLE `audit_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `audit_images`
--
ALTER TABLE `audit_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `audit_questions`
--
ALTER TABLE `audit_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `audit_questions2`
--
ALTER TABLE `audit_questions2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `audit_sites`
--
ALTER TABLE `audit_sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banners_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners_history`
--
ALTER TABLE `banners_history`
  MODIFY `banners_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `block_ips`
--
ALTER TABLE `block_ips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `categories_description`
--
ALTER TABLE `categories_description`
  MODIFY `categories_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `categories_organization`
--
ALTER TABLE `categories_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories_role`
--
ALTER TABLE `categories_role`
  MODIFY `categories_role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `compare`
--
ALTER TABLE `compare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `constant_banners`
--
ALTER TABLE `constant_banners`
  MODIFY `banners_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `countries_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupans_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currency_record`
--
ALTER TABLE `currency_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `current_theme`
--
ALTER TABLE `current_theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customers_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers_basket`
--
ALTER TABLE `customers_basket`
  MODIFY `customers_basket_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers_basket_attributes`
--
ALTER TABLE `customers_basket_attributes`
  MODIFY `customers_basket_attributes_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `flash_sale`
--
ALTER TABLE `flash_sale`
  MODIFY `flash_sale_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flate_rate`
--
ALTER TABLE `flate_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `front_end_theme_content`
--
ALTER TABLE `front_end_theme_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `geo_zones`
--
ALTER TABLE `geo_zones`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `home_banners_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `http_call_record`
--
ALTER TABLE `http_call_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `image_categories`
--
ALTER TABLE `image_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=336;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `inventory_ref_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `label_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1118;

--
-- AUTO_INCREMENT for table `label_value`
--
ALTER TABLE `label_value`
  MODIFY `label_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1828;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `languages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `liked_products`
--
ALTER TABLE `liked_products`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manage_min_max`
--
ALTER TABLE `manage_min_max`
  MODIFY `min_max_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manage_role`
--
ALTER TABLE `manage_role`
  MODIFY `manage_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `manufacturers_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `menu_translation`
--
ALTER TABLE `menu_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_categories`
--
ALTER TABLE `news_categories`
  MODIFY `categories_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_categories_description`
--
ALTER TABLE `news_categories_description`
  MODIFY `categories_description_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orders_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_products`
--
ALTER TABLE `orders_products`
  MODIFY `orders_products_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_products_attributes`
--
ALTER TABLE `orders_products_attributes`
  MODIFY `orders_products_attributes_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_status`
--
ALTER TABLE `orders_status`
  MODIFY `orders_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders_status_description`
--
ALTER TABLE `orders_status_description`
  MODIFY `orders_status_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders_status_history`
--
ALTER TABLE `orders_status_history`
  MODIFY `orders_status_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_total`
--
ALTER TABLE `orders_total`
  MODIFY `orders_total_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages_description`
--
ALTER TABLE `pages_description`
  MODIFY `page_description_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_description`
--
ALTER TABLE `payment_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `payment_methods_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payment_methods_detail`
--
ALTER TABLE `payment_methods_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `products_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `products_attributes_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_description`
--
ALTER TABLE `products_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_options`
--
ALTER TABLE `products_options`
  MODIFY `products_options_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_options_descriptions`
--
ALTER TABLE `products_options_descriptions`
  MODIFY `products_options_descriptions_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_options_values`
--
ALTER TABLE `products_options_values`
  MODIFY `products_options_values_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_options_values_descriptions`
--
ALTER TABLE `products_options_values_descriptions`
  MODIFY `products_options_values_descriptions_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_shipping_rates`
--
ALTER TABLE `products_shipping_rates`
  MODIFY `products_shipping_rates_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products_to_categories`
--
ALTER TABLE `products_to_categories`
  MODIFY `products_to_categories_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `reviews_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews_description`
--
ALTER TABLE `reviews_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `shipping_description`
--
ALTER TABLE `shipping_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `shipping_methods`
--
ALTER TABLE `shipping_methods`
  MODIFY `shipping_methods_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;

--
-- AUTO_INCREMENT for table `sliders_images`
--
ALTER TABLE `sliders_images`
  MODIFY `sliders_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `specials`
--
ALTER TABLE `specials`
  MODIFY `specials_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_class`
--
ALTER TABLE `tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_rates`
--
ALTER TABLE `tax_rates`
  MODIFY `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `top_offers`
--
ALTER TABLE `top_offers`
  MODIFY `top_offers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `unit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units_descriptions`
--
ALTER TABLE `units_descriptions`
  MODIFY `units_description_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ups_shipping`
--
ALTER TABLE `ups_shipping`
  MODIFY `ups_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_to_address`
--
ALTER TABLE `user_to_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `user_types_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `zones_to_geo_zones`
--
ALTER TABLE `zones_to_geo_zones`
  MODIFY `association_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
